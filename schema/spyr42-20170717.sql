/*
Navicat MySQL Data Transfer

Source Server         : __LOCALHOST
Source Server Version : 50625
Source Host           : 127.0.0.1:3306
Source Database       : spyr42

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2017-07-17 11:35:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for spyr42_addlpermissions
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_addlpermissions`;
CREATE TABLE `spyr42_addlpermissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moudule_id` int(10) unsigned DEFAULT NULL,
  `moudulegroup_id` int(10) unsigned DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route_filters` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `functions` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_addlpermissions
-- ----------------------------

-- ----------------------------
-- Table structure for spyr42_groups
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_groups`;
CREATE TABLE `spyr42_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_groups
-- ----------------------------
INSERT INTO `spyr42_groups` VALUES ('1', '2ed6008f-45fe-4873-9538-29c52fba476a', 'superuser', 'Superuser', '{\"superuser\":1}', 'Yes', '2017-02-07 13:59:00', '1', '2017-02-27 10:41:00', '1', null, null);
INSERT INTO `spyr42_groups` VALUES ('2', 'df4e64b1-89e2-40c8-9007-d19b5e638be2', 'tenant-admin', 'Customer admin', '{\"perm-module-users\":1,\"perm-module-users-view-list\":1,\"perm-module-users-view-details\":1,\"perm-module-users-create\":1,\"perm-module-users-edit\":1,\"perm-module-users-delete\":1,\"perm-module-users-restore\":1,\"perm-module-users-change-logs\":1,\"perm-module-users-files-view-list\":1,\"perm-module-users-files-view-details\":1,\"perm-module-users-files-create\":1,\"perm-module-users-files-edit\":1,\"perm-module-users-files-delete\":1,\"perm-module-users-files-download\":1,\"perm-module-userdetails\":1,\"perm-module-userdetails-view-list\":1,\"perm-module-userdetails-view-details\":1,\"perm-module-userdetails-create\":1,\"perm-module-userdetails-edit\":1,\"perm-module-userdetails-delete\":1,\"perm-module-userdetails-restore\":1,\"perm-module-userdetails-change-logs\":1,\"perm-module-uploads-create\":1,\"perm-module-uploads-edit\":1,\"perm-module-uploads-delete\":1,\"perm-module-uploads-view-details\":1,\"perm-module-uploads-view-list\":1,\"perm-module-uploads\":1,\"perm-module-uploads-restore\":1,\"perm-module-uploads-change-logs\":1}', 'Yes', '2017-02-20 19:37:20', '1', '2017-05-09 07:12:01', '1', null, null);

-- ----------------------------
-- Table structure for spyr42_gsettings
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_gsettings`;
CREATE TABLE `spyr42_gsettings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allow_tenant_override` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_gsettings
-- ----------------------------
INSERT INTO `spyr42_gsettings` VALUES ('1', 'c07553e1-8d3a-47b0-85b1-89c1ab02c541', 'app.name', 'Global Application Name', 'string', '', 'SPYRFRAME', 'No', 'Yes', '1', '1', '2017-02-19 11:16:15', '2017-05-23 09:25:26', null, null);
INSERT INTO `spyr42_gsettings` VALUES ('2', '0d09deec-ccd2-41de-8c1e-facb41516106', 'test.json.array', 'Test Json Array', 'array', '', '{\"a\":1,\"b\":2,\"c\":3,\"d\":4,\"e\":5}', 'No', 'Yes', '1', '1', '2017-02-19 11:31:06', '2017-05-23 10:18:49', null, null);
INSERT INTO `spyr42_gsettings` VALUES ('3', '085b0e48-d833-4ed3-b964-909c3d2813a6', 'test.another.setting', 'Test another setting', 'string', 'description goes here', 'lorel ipsum dolor', 'No', 'Yes', '1', '1', '2017-02-25 14:45:05', '2017-02-25 14:45:05', null, null);
INSERT INTO `spyr42_gsettings` VALUES ('4', 'b0dbfaec-21ae-4acd-a714-e20fbfcc9099', 'asdfsdaf', 'asdf', 'string', 'asdf', 'asdf', 'No', 'No', '1', '1', '2017-02-25 14:52:36', '2017-02-25 14:52:36', null, null);
INSERT INTO `spyr42_gsettings` VALUES ('5', 'e13439cd-f698-46e4-9135-ab02c5a59c65', 'asfsdfsdfsdf', 'sfsadfsd', 'string', '', 'sadfsdf', 'No', 'No', '1', '1', '2017-02-25 14:53:16', '2017-02-25 14:53:16', null, null);
INSERT INTO `spyr42_gsettings` VALUES ('6', '9dea25e4-3e26-4608-8118-32a47f233c76', 'another setting', 'asdfsdf', 'string', 'sadfsdf', 'sadfasdf', 'Yes', 'No', '1', '1', '2017-02-25 14:53:55', '2017-03-01 06:05:36', null, null);

-- ----------------------------
-- Table structure for spyr42_migrations
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_migrations`;
CREATE TABLE `spyr42_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_migrations
-- ----------------------------
INSERT INTO `spyr42_migrations` VALUES ('2012_12_06_225921_migration_cartalyst_sentry_install_users', '1');
INSERT INTO `spyr42_migrations` VALUES ('2012_12_06_225929_migration_cartalyst_sentry_install_groups', '1');
INSERT INTO `spyr42_migrations` VALUES ('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', '1');
INSERT INTO `spyr42_migrations` VALUES ('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', '1');
INSERT INTO `spyr42_migrations` VALUES ('2017_01_31_063821_spyrfiy_sentry_user_table', '2');
INSERT INTO `spyr42_migrations` VALUES ('2017_01_31_070750_create_tenants_table', '3');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_04_043813_add_user_id_in_tenants', '4');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_05_104909_cleanup_users_table', '5');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_05_105135_create_userdetails_table', '6');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_05_112500_add_user_id_in_userdetails', '7');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_07_121544_add_back_name_in_users_table', '8');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_07_123942_spyrfy_sentry_groups_table', '9');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_08_063428_create_modules_table', '10');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_08_180427_populate_modules_table', '11');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_15_070556_create_modulegroups_table', '12');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_15_072631_change_parent_module_id_to_modulegroup_id', '13');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_15_091903_create_permissions_table', '14');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_15_102824_create_permissioncategories_table', '15');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_15_110028_add_uploads_messages_flag_in_modules', '15');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_15_112618_update_permissioncategories_table', '16');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_16_062143_change_name_of_parent_fields', '17');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_19_104322_create_gsettings_table', '18');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_20_192658_udpate_groups_table', '19');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_24_054339_rename_permissions_table_to_addlpermissions', '20');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_24_055053_drop_permissioncategories_table', '21');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_24_055215_redefine_permissioncategories_in_addlpermissions', '22');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_27_073005_change_group_fields_in_users', '23');
INSERT INTO `spyr42_migrations` VALUES ('2013_04_09_062329_create_revisions_table', '24');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_27_104629_create_uploads_table', '25');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_27_123018_change_uploads_element_uuid_to_string', '26');
INSERT INTO `spyr42_migrations` VALUES ('2017_02_27_123705_change_upoloads_element_id_uuid', '27');
INSERT INTO `spyr42_migrations` VALUES ('2017_03_05_110719_remove_revisions_table', '28');
INSERT INTO `spyr42_migrations` VALUES ('2017_03_05_111302_create_revisions_table', '29');
INSERT INTO `spyr42_migrations` VALUES ('2017_03_05_141116_add_changeset_in_revisions', '30');
INSERT INTO `spyr42_migrations` VALUES ('2017_04_30_081149_add_is_system_user_in_users', '31');
INSERT INTO `spyr42_migrations` VALUES ('2017_05_04_065726_add_has_uplaods_messages_from_modules_table', '32');
INSERT INTO `spyr42_migrations` VALUES ('2017_05_07_064000_change_to_is_editable_by_tenant', '33');
INSERT INTO `spyr42_migrations` VALUES ('2017_05_08_064757_add_ext_in_uploads', '34');

-- ----------------------------
-- Table structure for spyr42_modulegroups
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_modulegroups`;
CREATE TABLE `spyr42_modulegroups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned DEFAULT NULL,
  `order` int(10) unsigned DEFAULT NULL,
  `color_css` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon_css` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_modulegroups
-- ----------------------------
INSERT INTO `spyr42_modulegroups` VALUES ('1', '572b3509-9378-4d9f-8c6e-1b69a0faf78c', 'app-settings', 'App settings', 'lorel ipsum dolor sit amet', '0', '0', '99999', '', 'fa fa-dashboard', '', 'Yes', '1', '1', '2017-02-16 07:32:54', '2017-04-19 11:45:15', null, null);
INSERT INTO `spyr42_modulegroups` VALUES ('2', 'b273bf91-3b5a-4046-8c4d-5ccb0a9bc086', 'permission-settings', 'Permission', 'lorel ipsum dolor sit amet', '1', '0', '0', '', 'fa fa-dashboard', '', 'Yes', '1', '1', '2017-02-16 07:40:05', '2017-02-16 07:40:05', null, null);
INSERT INTO `spyr42_modulegroups` VALUES ('3', 'b9ad3e05-e199-4964-b825-0ea6a63efabf', 'module-configurations', 'Module configurations', 'lorel ipsum dolor sit amet', '1', '0', '0', '', 'fa fa-dashboard', '', 'Yes', '1', '1', '2017-02-16 10:21:45', '2017-02-16 10:21:45', null, null);
INSERT INTO `spyr42_modulegroups` VALUES ('4', '3cbdf876-dfaa-4cea-b9ec-2809f3293a3d', 'subscriptions', 'Subscriptions', 'lorel ipsum dolor sit amet', '0', '0', '0', '', 'fa fa-dashboard', '', 'No', '1', '1', '2017-02-16 10:24:25', '2017-02-27 07:18:21', null, null);

-- ----------------------------
-- Table structure for spyr42_modules
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_modules`;
CREATE TABLE `spyr42_modules` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `modulegroup_id` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned DEFAULT NULL,
  `order` int(10) unsigned DEFAULT NULL,
  `color_css` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon_css` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `has_uploads` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `has_messages` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_modules
-- ----------------------------
INSERT INTO `spyr42_modules` VALUES ('1', '5d3231e2-92d4-48f5-ba7e-f2a2b07dd79e', 'modules', 'Modules', 'lorel ipsum dolor sit amet', '0', '3', '0', '0', 'aqua', 'fa fa-plus', '', null, null, 'Yes', '1', '1', '2017-02-12 10:28:58', '2017-02-16 11:55:46', null, null);
INSERT INTO `spyr42_modules` VALUES ('2', '85fc8af0-d9ca-49e2-8abc-9019835b2ae7', 'users', 'User', 'lorel ipsum dolor sit amet', '0', '0', '0', '0', 'aqua', 'fa fa-plus', '', 'Yes', null, 'Yes', '1', '1', '2017-02-12 10:28:58', '2017-02-27 10:12:48', null, null);
INSERT INTO `spyr42_modules` VALUES ('3', '78aaaf7f-3303-4d43-a005-d23e0e4848cb', 'userdetails', 'User details', 'lorel ipsum dolor sit amet<br />\r\nlorel ipsum dolor sit amet', '2', '0', '0', '0', 'aqua', 'fa fa-plus', '', 'Yes', 'Yes', 'Yes', '1', '1', '2017-02-12 10:28:58', '2017-05-04 12:03:12', null, null);
INSERT INTO `spyr42_modules` VALUES ('4', '608b35f7-afd0-44bb-95b1-b7f06adf5d22', 'groups', 'Group', 'lorel ipsum dolor sit amet', '0', '0', '0', '0', 'aqua', 'fa fa-plus', '', null, null, 'Yes', '1', '1', '2017-02-12 10:28:59', '2017-02-15 03:33:43', null, null);
INSERT INTO `spyr42_modules` VALUES ('5', 'bb86a20a-eda8-474d-b756-a6711e120314', 'tenants', 'Tenant', 'lorel ipsum dolor sit amet', '0', '4', '0', '0', 'aqua', 'fa fa-plus', '', null, null, 'Yes', '1', '1', '2017-02-12 10:28:59', '2017-02-25 12:02:42', null, null);
INSERT INTO `spyr42_modules` VALUES ('7', '50249bd6-5b1b-486c-ae45-bdfc3ac1f44f', 'addlpermissions', 'Additional permissions', 'lorel ipsum dolor sit amet', '2', '0', '0', '0', 'aqua', 'fa fa-plus', '', null, null, 'Yes', '1', '1', '2017-02-14 14:09:11', '2017-03-05 07:41:39', null, null);
INSERT INTO `spyr42_modules` VALUES ('8', 'de3cdd8e-f2a6-4f28-8950-fc98f53c8c87', 'modulegroups', 'Module group', 'lorel ipsum dolor sit amet', '0', '3', '0', '0', 'aqua', 'fa fa-plus', 'modulegroups.index', null, null, 'Yes', '1', '1', '2017-02-15 07:13:08', '2017-02-16 11:55:54', null, null);
INSERT INTO `spyr42_modules` VALUES ('10', '2b5fad54-31c3-4c4f-9f28-7a1e86b31ac3', 'permissioncategories', 'Permission category', 'lorel ipsum dolor sit amet', '0', '2', '0', '0', 'aqua', 'fa fa-plus', 'permissioncategories.index', null, null, 'Yes', '1', '1', '2017-02-15 11:05:54', '2017-02-16 07:47:03', null, null);
INSERT INTO `spyr42_modules` VALUES ('11', 'ee7893a2-72ff-4925-828d-c712249ea987', 'gsettings', 'Global settings', 'lorel ipsum dolor sit amet', '0', '0', '0', '0', 'aqua', 'fa fa-plus', 'gsettings.index', null, null, 'Yes', '1', '1', '2017-02-19 10:47:39', '2017-02-19 10:47:39', null, null);
INSERT INTO `spyr42_modules` VALUES ('12', 'd6f8db2b-1f2f-403a-b068-fc9f0517b5b9', 'uploads', 'Upload', 'lorel&nbsp;', '0', '3', '0', '0', 'aqua', 'fa fa-plus', 'uploads.index', null, null, 'Yes', '1', '1', '2017-02-27 11:03:51', '2017-02-28 12:09:31', null, null);
INSERT INTO `spyr42_modules` VALUES ('13', '0d046744-e724-479e-ad14-3c4d70139e58', 'revisions', 'Revision', 'lorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlorel ipsumlo', '0', '0', '0', '0', 'aqua', 'fa fa-plus', 'revisions.index', 'Yes', 'Yes', 'Yes', '1', '1', '2017-03-05 12:16:55', '2017-05-04 13:43:15', null, null);
INSERT INTO `spyr42_modules` VALUES ('14', 'c3c26079-146a-4694-b458-514a73581667', 'testmodules', 'Test module', 'testmodulesS8GRMT0R', '0', '0', '0', '0', '', '', '', 'Yes', 'Yes', 'No', '1', '1', '2017-05-04 12:35:44', '2017-06-07 10:34:28', null, null);

-- ----------------------------
-- Table structure for spyr42_permissioncategories
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_permissioncategories`;
CREATE TABLE `spyr42_permissioncategories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_permissioncategories
-- ----------------------------

-- ----------------------------
-- Table structure for spyr42_permissions
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_permissions`;
CREATE TABLE `spyr42_permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `permissioncategory_id` int(10) unsigned DEFAULT NULL,
  `moudule_id` int(10) unsigned DEFAULT NULL,
  `moudulegroup_id` int(10) unsigned DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route_filters` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `functions` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_permissions
-- ----------------------------
INSERT INTO `spyr42_permissions` VALUES ('1', '883b02ca-5092-4198-a244-8d7dd2ece17b', 'perm-module-modules-access', 'Modules access', '', '0', '1', '1', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('2', '5cc909ff-69f8-47ee-8f9f-1535829a0fc9', 'perm-module-modules-view-list', 'Modules view grid', '', '0', '1', '1', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('3', '45dda522-f8fb-4ad6-84fc-216bb621b73c', 'perm-module-modules-view-details', 'Modules view details', '', '0', '1', '1', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('4', '54c2c65a-7a91-4408-bd49-ee085194104d', 'perm-module-modules-create', 'Modules create', '', '0', '1', '1', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('5', 'c1ee49fd-e727-4fa2-98df-5c9f2ae9fa64', 'perm-module-modules-edit', 'Modules edit', '', '0', '1', '1', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('6', '71663512-677a-476c-8ca2-3d3bcb2a8bd2', 'perm-module-modules-delete', 'Modules delete', '', '0', '1', '1', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('7', '4a9128f0-b9d1-4b17-8587-a2573e3413bc', 'perm-module-modules-restore', 'Modules restore', '', '0', '1', '1', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('8', 'b387648e-258a-4f5a-bf32-caf15f652aef', 'perm-module-modules-change-logs', 'Modules change logs', '', '0', '1', '1', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('9', 'ef9fcf79-6ad4-4df2-91d6-bd78986562ea', 'perm-module-users-access', 'User access', '', '0', '1', '2', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('10', 'b2204b7c-9196-42c4-a492-6506621320ca', 'perm-module-users-view-list', 'User view grid', '', '0', '1', '2', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('11', 'da341fa8-22fc-4f16-92f9-bd107366e707', 'perm-module-users-view-details', 'User view details', '', '0', '1', '2', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('12', '1f018dbb-e28c-4200-b2b3-038f5a034d4f', 'perm-module-users-create', 'User create', '', '0', '1', '2', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('13', '659015d5-0613-454f-8b7f-bd85afa469b5', 'perm-module-users-edit', 'User edit', '', '0', '1', '2', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('14', '084281bf-a491-44e4-96c9-1566d3b55426', 'perm-module-users-delete', 'User delete', '', '0', '1', '2', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('15', '34326eb3-479c-4603-bde6-4a1fd849256d', 'perm-module-users-restore', 'User restore', '', '0', '1', '2', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:54', '2017-02-15 11:14:54', null, null);
INSERT INTO `spyr42_permissions` VALUES ('16', '0ed12b3f-b200-4663-80d7-89adde687d24', 'perm-module-users-change-logs', 'User change logs', '', '0', '1', '2', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('17', '3be75d1d-c1c8-4d3d-a3cc-c8b691eedd0d', 'perm-module-userdetails-access', 'User details access', '', '0', '1', '3', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('18', 'deeede6e-25d3-4188-a7ab-10d8ce805d05', 'perm-module-userdetails-view-list', 'User details view grid', '', '0', '1', '3', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('19', 'e8d14ad4-ff7b-4957-abd2-0771855678c3', 'perm-module-userdetails-view-details', 'User details view details', '', '0', '1', '3', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('20', 'd8dc28a3-1adf-4149-8072-eddc1e51b4ca', 'perm-module-userdetails-create', 'User details create', '', '0', '1', '3', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('21', '4e8637c5-4e5b-437c-9149-2978de63c665', 'perm-module-userdetails-edit', 'User details edit', '', '0', '1', '3', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('22', 'c1c937c0-8bd8-4920-a05d-44daf3b4e8f7', 'perm-module-userdetails-delete', 'User details delete', '', '0', '1', '3', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('23', 'fbd2540d-d664-4944-953c-ad0e7c173197', 'perm-module-userdetails-restore', 'User details restore', '', '0', '1', '3', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('24', '75fd0e5a-780c-4cac-ab38-7ca5b19a7073', 'perm-module-userdetails-change-logs', 'User details change logs', '', '0', '1', '3', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('25', 'b32d4dee-1b68-4b99-91ae-ed32b609f813', 'perm-module-groups-access', 'Group access', '', '0', '1', '4', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('26', 'd4a9211d-d451-4839-aff1-5c4dfd28a019', 'perm-module-groups-view-list', 'Group view grid', '', '0', '1', '4', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('27', '86c5b978-fcd4-429a-8c52-cea326c2fad3', 'perm-module-groups-view-details', 'Group view details', '', '0', '1', '4', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('28', '271e14e1-6ab3-4fd0-b951-46bdd31507fe', 'perm-module-groups-create', 'Group create', '', '0', '1', '4', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('29', '7d3a8319-5e27-4a48-ae4a-d952d1a18320', 'perm-module-groups-edit', 'Group edit', '', '0', '1', '4', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('30', 'f1dbf96c-b790-4099-a5a1-219148eb87eb', 'perm-module-groups-delete', 'Group delete', '', '0', '1', '4', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('31', 'd0fd5b98-2df5-47d7-8083-ffba1bf65485', 'perm-module-groups-restore', 'Group restore', '', '0', '1', '4', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:55', '2017-02-15 11:14:55', null, null);
INSERT INTO `spyr42_permissions` VALUES ('32', '9b3505d2-6ad5-49ad-a0d3-eec02a6b4c21', 'perm-module-groups-change-logs', 'Group change logs', '', '0', '1', '4', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('33', '241dd659-7d13-4cad-ae0a-c381f6fc66dd', 'perm-module-tenants-access', 'Tenant access', '', '0', '1', '5', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('34', '1bfeb381-eb93-4803-97a3-aec5a4c4d6c7', 'perm-module-tenants-view-list', 'Tenant view grid', '', '0', '1', '5', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('35', '8ad2f0b6-8cae-41f8-a9df-34beb16fd8af', 'perm-module-tenants-view-details', 'Tenant view details', '', '0', '1', '5', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('36', '90dd34b7-fd7e-4010-9ea0-1c66c39dc01c', 'perm-module-tenants-create', 'Tenant create', '', '0', '1', '5', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('37', '67c65c49-6058-485a-a961-170e704cd965', 'perm-module-tenants-edit', 'Tenant edit', '', '0', '1', '5', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('38', '15d6d0e7-aa54-4777-ad8f-ddb7f0c7d5ff', 'perm-module-tenants-delete', 'Tenant delete', '', '0', '1', '5', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('39', '206a65f3-82b3-4068-a954-2ab5651c318b', 'perm-module-tenants-restore', 'Tenant restore', '', '0', '1', '5', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('40', 'd3e6c5ad-48e7-4e5b-aeb7-6b86a5c338a6', 'perm-module-tenants-change-logs', 'Tenant change logs', '', '0', '1', '5', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('41', '0ac29299-6f97-41c3-9cab-9a3433a6995d', 'perm-module-modulegroups-access', 'Module group access', '', '0', '1', '8', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('42', 'e2bcaabc-550a-4fa6-87ed-cf031ac4adc4', 'perm-module-modulegroups-view-list', 'Module group view grid', '', '0', '1', '8', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('43', 'c54a6b74-7985-4236-806c-a5ecdfc37cba', 'perm-module-modulegroups-view-details', 'Module group view details', '', '0', '1', '8', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('44', '8a68569e-db4c-4045-87a2-4a20e603eab1', 'perm-module-modulegroups-create', 'Module group create', '', '0', '1', '8', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('45', '65ded2c5-6252-4d68-b08c-7730513aa311', 'perm-module-modulegroups-edit', 'Module group edit', '', '0', '1', '8', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('46', '35bfdc86-24b2-4065-aea3-2a4cae6ef1de', 'perm-module-modulegroups-delete', 'Module group delete', '', '0', '1', '8', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('47', '65656281-70d1-45c6-8816-4709be4ffe9b', 'perm-module-modulegroups-restore', 'Module group restore', '', '0', '1', '8', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:56', '2017-02-15 11:14:56', null, null);
INSERT INTO `spyr42_permissions` VALUES ('48', '86603f53-82c0-47bd-aa23-65fbacc226ed', 'perm-module-modulegroups-change-logs', 'Module group change logs', '', '0', '1', '8', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('49', '371c036c-c41f-4880-b035-4d43c1047c08', 'perm-module-permissions-access', 'Permission access', '', '0', '1', '9', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('50', '1f19b12f-48aa-4138-aabb-4b968d77578e', 'perm-module-permissions-view-list', 'Permission view grid', '', '0', '1', '9', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('51', '8b4e0f77-6b57-4fca-9801-aa9cc4c2d028', 'perm-module-permissions-view-details', 'Permission view details', '', '0', '1', '9', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('52', 'fc84510f-b4c4-4e1a-b0b7-7a878e138341', 'perm-module-permissions-create', 'Permission create', '', '0', '1', '9', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('53', '2e6588b2-c655-44e3-aef9-bdeda65359ff', 'perm-module-permissions-edit', 'Permission edit', '', '0', '1', '9', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('54', '60c119ed-cb6f-4fc5-a6aa-d651eaf03944', 'perm-module-permissions-delete', 'Permission delete', '', '0', '1', '9', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('55', '700477fe-f677-421d-baba-d2d27c8b4c56', 'perm-module-permissions-restore', 'Permission restore', '', '0', '1', '9', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('56', 'c5294d0a-a9c6-46c8-bafd-6ee88ef69f1b', 'perm-module-permissions-change-logs', 'Permission change logs', '', '0', '1', '9', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('57', 'baa8b138-2467-4a4a-aaab-eb7217bbba24', 'perm-module-permissioncategories-access', 'Permissioncategory access', '', '0', '1', '10', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('58', '17da7a12-7b1b-4324-8f51-cacef4d90467', 'perm-module-permissioncategories-view-list', 'Permissioncategory view grid', '', '0', '1', '10', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('59', '961f5cb2-11d3-462d-a2d1-f5378a193000', 'perm-module-permissioncategories-view-details', 'Permissioncategory view details', '', '0', '1', '10', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('60', 'af422e95-f4b8-4d12-8548-a0c329fa7eae', 'perm-module-permissioncategories-create', 'Permissioncategory create', '', '0', '1', '10', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('61', '2111cbe3-1ef7-4862-8ee3-caa390f1c74c', 'perm-module-permissioncategories-edit', 'Permissioncategory edit', '', '0', '1', '10', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('62', '9b9cce02-9021-4030-abbf-48a329ef5a7d', 'perm-module-permissioncategories-delete', 'Permissioncategory delete', '', '0', '1', '10', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('63', '0513fe75-d6e5-4657-ad6d-1658933f660f', 'perm-module-permissioncategories-restore', 'Permissioncategory restore', '', '0', '1', '10', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);
INSERT INTO `spyr42_permissions` VALUES ('64', 'd43f3966-4499-4baa-b562-a2d542c24c4a', 'perm-module-permissioncategories-change-logs', 'Permissioncategory change logs', '', '0', '1', '10', null, null, null, null, null, 'Yes', '1', '1', '2017-02-15 11:14:57', '2017-02-15 11:14:57', null, null);

-- ----------------------------
-- Table structure for spyr42_revisions
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_revisions`;
CREATE TABLE `spyr42_revisions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(10) unsigned DEFAULT NULL,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `changeset` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_id` int(10) unsigned DEFAULT NULL,
  `module_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `element_id` int(10) unsigned DEFAULT NULL,
  `element_uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old` text COLLATE utf8_unicode_ci,
  `new` text COLLATE utf8_unicode_ci,
  `desc` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=316 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_revisions
-- ----------------------------
INSERT INTO `spyr42_revisions` VALUES ('1', null, 'c14b2834-f85a-41d3-9c53-f2c8374ec0a9', null, null, null, null, null, null, null, null, null, null, null, '1', '1', '2017-03-05 14:38:17', '2017-03-05 14:38:17', null, null);
INSERT INTO `spyr42_revisions` VALUES ('2', null, '7f1fc193-c895-4403-b3bc-81f0906c1bbe', null, null, null, null, null, null, null, null, null, null, null, '1', '1', '2017-03-05 14:38:17', '2017-03-05 14:38:17', null, null);
INSERT INTO `spyr42_revisions` VALUES ('3', null, '85064076-ee85-452e-844d-6a45bf581cfe', null, 'update', '2', null, null, null, 'last_login', '2017-03-05 14:38:17', '2017-03-05 14:39:13', '', null, '1', '1', '2017-03-05 14:39:13', '2017-03-05 14:39:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('4', null, '4fd7c184-7fe3-43d9-92a1-113e4e7611fd', null, 'update', '2', null, null, null, 'updated_at', '2017-03-05 14:38:17', '2017-03-05 14:39:13', '', null, '1', '1', '2017-03-05 14:39:13', '2017-03-05 14:39:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('5', null, 'c8604f20-9fbb-4f05-b196-74c0d9603f44', null, '', '2', null, null, null, 'group_ids_csv', null, ',2,', '', null, '1', '1', '2017-03-05 14:40:10', '2017-03-05 14:40:10', null, null);
INSERT INTO `spyr42_revisions` VALUES ('6', null, 'afb7d419-aae3-4e14-98f0-20a13a1aff70', null, '', '2', null, null, null, 'group_titles_csv', null, ',tenant-admin,', '', null, '1', '1', '2017-03-05 14:40:10', '2017-03-05 14:40:10', null, null);
INSERT INTO `spyr42_revisions` VALUES ('7', null, 'a9b311dc-2350-41f2-b4df-958d3fb43052', null, '', '2', null, null, null, 'updated_at', '2017-02-28 09:57:56', '2017-03-05 14:40:10', '', null, '1', '1', '2017-03-05 14:40:10', '2017-03-05 14:40:10', null, null);
INSERT INTO `spyr42_revisions` VALUES ('8', null, '0df33234-692d-44de-b5ad-4fbaaeaf66f4', 'FfdMtwet', '', '2', null, null, null, 'group_ids_csv', ',1,', ',2,', '', null, '1', '1', '2017-03-05 14:42:12', '2017-03-05 14:42:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('9', null, 'e00bdc5b-a4e0-40e1-bf72-92b6e20bc3b8', 'FfdMtwet', '', '2', null, null, null, 'group_titles_csv', ',superuser,', ',tenant-admin,', '', null, '1', '1', '2017-03-05 14:42:12', '2017-03-05 14:42:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('10', null, '375ea586-1854-4fbe-8946-f9240d5b31e0', 'FfdMtwet', '', '2', null, null, null, 'updated_at', '2017-03-05 14:41:23', '2017-03-05 14:42:12', '', null, '1', '1', '2017-03-05 14:42:13', '2017-03-05 14:42:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('11', null, 'e58434a1-8a0b-4c21-80f3-f4739d3d48e4', 'tZ6AuLAm', '', '2', 'users', null, null, 'group_ids_csv', ',2,', ',1,', '', null, '1', '1', '2017-03-05 14:43:09', '2017-03-05 14:43:09', null, null);
INSERT INTO `spyr42_revisions` VALUES ('12', null, '9c133cb6-5f43-4187-8257-1e5bd5ce13e1', 'tZ6AuLAm', '', '2', 'users', null, null, 'group_titles_csv', ',tenant-admin,', ',superuser,', '', null, '1', '1', '2017-03-05 14:43:09', '2017-03-05 14:43:09', null, null);
INSERT INTO `spyr42_revisions` VALUES ('13', null, '0a5ead66-7917-49af-a98f-8524b4e651a5', 'tZ6AuLAm', '', '2', 'users', null, null, 'updated_at', '2017-03-05 14:42:12', '2017-03-05 14:43:09', '', null, '1', '1', '2017-03-05 14:43:09', '2017-03-05 14:43:09', null, null);
INSERT INTO `spyr42_revisions` VALUES ('14', null, 'bdb35953-a94b-4d99-a44c-eb62d8235421', '9Pms0tda', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'group_ids_csv', ',1,', ',2,', '', null, '1', '1', '2017-03-05 14:44:01', '2017-03-05 14:44:01', null, null);
INSERT INTO `spyr42_revisions` VALUES ('15', null, 'c1de36fa-5337-42eb-bb3a-481155ea76fd', '9Pms0tda', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'group_titles_csv', ',superuser,', ',tenant-admin,', '', null, '1', '1', '2017-03-05 14:44:01', '2017-03-05 14:44:01', null, null);
INSERT INTO `spyr42_revisions` VALUES ('16', null, '807af1a6-5dd1-46f1-a2f8-952527c6900a', '9Pms0tda', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'updated_at', '2017-03-05 14:43:09', '2017-03-05 14:44:01', '', null, '1', '1', '2017-03-05 14:44:02', '2017-03-05 14:44:02', null, null);
INSERT INTO `spyr42_revisions` VALUES ('17', null, 'e6a4d844-77ae-4430-a34d-6b78f4d59c11', 'wBmFzLTb', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'updated_at', '2017-03-05 14:44:01', '2017-03-05 14:46:24', '', 'Yes', '1', '1', '2017-03-05 14:46:24', '2017-03-05 14:46:24', null, null);
INSERT INTO `spyr42_revisions` VALUES ('18', null, '5e30a83f-cf77-445a-aee6-68822af7089e', '8IjKwXmn', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'group_ids_csv', ',2,', ',1,', '', 'Yes', '1', '1', '2017-03-05 14:51:11', '2017-03-05 14:51:11', null, null);
INSERT INTO `spyr42_revisions` VALUES ('19', null, '66c08dba-a2f7-493e-a563-c6c3291ce1fc', '8IjKwXmn', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'group_titles_csv', ',tenant-admin,', ',superuser,', '', 'Yes', '1', '1', '2017-03-05 14:51:11', '2017-03-05 14:51:11', null, null);
INSERT INTO `spyr42_revisions` VALUES ('20', null, 'ef6c7499-c01d-4c7d-89d2-6b2cd19373dc', '8IjKwXmn', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'updated_at', '2017-03-05 14:46:24', '2017-03-05 14:51:11', '', 'Yes', '1', '1', '2017-03-05 14:51:11', '2017-03-05 14:51:11', null, null);
INSERT INTO `spyr42_revisions` VALUES ('21', null, '6fabf6c5-3e3e-4c3e-977b-c751fe996cf0', 'XzcI7i8z', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'updated_at', '2017-03-05 14:51:11', '2017-03-05 14:51:19', '', 'Yes', '1', '1', '2017-03-05 14:51:19', '2017-03-05 14:51:19', null, null);
INSERT INTO `spyr42_revisions` VALUES ('22', null, '8696019a-4db1-480b-a0f5-5c9e1b429d79', 'YwneFwlR', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'updated_at', '2017-03-05 14:51:19', '2017-03-05 14:52:49', '', 'Yes', '1', '1', '2017-03-05 14:52:49', '2017-03-05 14:52:49', null, null);
INSERT INTO `spyr42_revisions` VALUES ('23', null, 'f76a50a5-d002-4403-90a4-a15640985cb1', '6Dm4m1vq', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'updated_at', '2017-03-05 14:52:49', '2017-03-05 14:52:51', '', 'Yes', '1', '1', '2017-03-05 14:52:51', '2017-03-05 14:52:51', null, null);
INSERT INTO `spyr42_revisions` VALUES ('24', null, '78663a84-fb30-4113-a3a4-c4480ff6eeb0', 'XG2qB7Hq', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'group_ids_csv', ',1,', ',2,', '', 'Yes', '1', '1', '2017-03-05 14:54:34', '2017-03-05 14:54:34', null, null);
INSERT INTO `spyr42_revisions` VALUES ('25', null, '17d43ccb-67b8-438d-b253-694762eb6b86', 'XG2qB7Hq', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'group_titles_csv', ',superuser,', ',tenant-admin,', '', 'Yes', '1', '1', '2017-03-05 14:54:34', '2017-03-05 14:54:34', null, null);
INSERT INTO `spyr42_revisions` VALUES ('26', null, '42ed3135-0bdb-480a-9531-d8a805172d7e', 'jH9ByH2q', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-03-05 14:39:13', '2017-03-06 09:08:24', '', 'Yes', '1', '1', '2017-03-06 09:08:24', '2017-03-06 09:08:24', null, null);
INSERT INTO `spyr42_revisions` VALUES ('27', null, '018acda0-4d29-4cc6-81ce-586506d81c79', 'RZxfG9Pl', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-03-06 09:08:24', '2017-03-06 09:09:08', '', 'Yes', '1', '1', '2017-03-06 09:09:08', '2017-03-06 09:09:08', null, null);
INSERT INTO `spyr42_revisions` VALUES ('28', null, '44a48e19-ce17-480b-bdfa-dbbc400e0c64', 'm2leVO21', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-03-06 09:09:08', '2017-03-06 09:09:24', '', 'Yes', '1', '1', '2017-03-06 09:09:24', '2017-03-06 09:09:24', null, null);
INSERT INTO `spyr42_revisions` VALUES ('29', null, '97728e4e-ffc8-4508-ad9f-8b7b4b81f223', 'LjDu0Pt2', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-03-06 09:09:24', '2017-03-06 09:09:47', '', 'Yes', '1', '1', '2017-03-06 09:09:47', '2017-03-06 09:09:47', null, null);
INSERT INTO `spyr42_revisions` VALUES ('30', null, '0dce583b-4817-4d67-8ed0-a30b5632555b', 'UX4HpI6n', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-03-06 09:09:47', '2017-03-06 09:10:43', '', 'Yes', '1', '1', '2017-03-06 09:10:43', '2017-03-06 09:10:43', null, null);
INSERT INTO `spyr42_revisions` VALUES ('31', null, '815cd5bf-2fd7-4c6c-a1a0-8295dc40a04d', 'y5Jtfjjz', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-03-06 09:10:43', '2017-03-06 09:11:03', '', 'Yes', '1', '1', '2017-03-06 09:11:03', '2017-03-06 09:11:03', null, null);
INSERT INTO `spyr42_revisions` VALUES ('32', null, 'a8614447-b623-4870-80c6-43c2a0c57915', '1ATaEMq3', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-03-06 09:11:03', '2017-03-06 09:13:15', '', 'Yes', '1', '1', '2017-03-06 09:13:15', '2017-03-06 09:13:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('33', null, 'aa52835b-0d9c-4a85-adca-79d67754dcc5', 'ScOYCGDn', '', '2', 'users', '4', '06b122e9-7b28-4c3e-a38f-4301159d7632', 'tenant_id', '1', '', '', 'Yes', '1', '1', '2017-03-06 09:13:37', '2017-03-06 09:13:37', null, null);
INSERT INTO `spyr42_revisions` VALUES ('34', null, '77568032-f974-4fa8-a2f2-b949fbf2081c', 'ScOYCGDn', '', '2', 'users', '4', '06b122e9-7b28-4c3e-a38f-4301159d7632', 'group_ids_csv', ',2,', ',1,', '', 'Yes', '1', '1', '2017-03-06 09:13:37', '2017-03-06 09:13:37', null, null);
INSERT INTO `spyr42_revisions` VALUES ('35', null, '6e6b5737-ab38-4253-a0e0-e71a8940a6d4', 'ScOYCGDn', '', '2', 'users', '4', '06b122e9-7b28-4c3e-a38f-4301159d7632', 'group_titles_csv', ',tenant-admin,', ',superuser,', '', 'Yes', '1', '1', '2017-03-06 09:13:37', '2017-03-06 09:13:37', null, null);
INSERT INTO `spyr42_revisions` VALUES ('36', null, '90940dbf-7c05-4d7b-8866-cfbde330bce6', '2wY6atzs', '', '2', 'users', '8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', 'name', 'testchangelog', 'testssdfchangelog', '', 'Yes', '1', '1', '2017-03-06 10:05:53', '2017-03-06 10:05:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('37', null, '35910a43-0c3b-4ff0-a9fb-3df5565671ed', '5iniVerd', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-03-06 09:13:15', '2017-04-05 12:09:10', '', 'Yes', '1', '1', '2017-04-05 12:09:10', '2017-04-05 12:09:10', null, null);
INSERT INTO `spyr42_revisions` VALUES ('38', null, '95b51300-2095-4088-bdc9-e7098bda2a47', '4T9zbpIX', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-04-05 12:09:10', '2017-04-05 12:20:34', '', 'Yes', '1', '1', '2017-04-05 12:20:34', '2017-04-05 12:20:34', null, null);
INSERT INTO `spyr42_revisions` VALUES ('39', null, '2320511f-7368-468b-8d61-67d1b4d8072a', 'lXjFbezA', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-04-05 12:20:34', '2017-04-19 10:03:36', '', 'Yes', '1', '1', '2017-04-19 10:03:36', '2017-04-19 10:03:36', null, null);
INSERT INTO `spyr42_revisions` VALUES ('40', null, '1a101e61-870b-499a-9946-645abe75c6cf', 'SoSnrhpu', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-04-19 10:03:36', '2017-04-30 05:04:18', '', 'Yes', '1', '1', '2017-04-30 05:04:19', '2017-04-30 05:04:19', null, null);
INSERT INTO `spyr42_revisions` VALUES ('41', null, '3064146f-f003-4300-8e65-dd8539ca35f8', 'ZWz4dNGy', '', '2', 'users', '10', '3aaebac7-295b-4a3f-aad3-c6b41938a145', 'activation_code', null, 'ZpGSsJbdYpPFkeg55CLbQ88pbYasKHSnxBtbsh6ObI', '', 'Yes', '1', '1', '2017-04-30 05:44:35', '2017-04-30 05:44:35', null, null);
INSERT INTO `spyr42_revisions` VALUES ('42', null, 'f99ebea5-738e-4d85-85ed-d68bff152c11', 'iq5vIras', '', '2', 'users', '12', '3235a063-97cb-4311-a04a-133872026af7', 'activation_code', null, '4ao13jAq9dIO0ngYs1oQpMcnYUp6NXyJqtUAG7SGtU', '', 'Yes', '1', '1', '2017-04-30 06:16:48', '2017-04-30 06:16:48', null, null);
INSERT INTO `spyr42_revisions` VALUES ('43', null, '70c872f7-67a9-42b7-a04e-0c1ec1d439bc', 'bZzAJbvL', '', '2', 'users', '14', '9650eed5-96fc-4e6b-b852-8e0daed8cdf4', 'activation_code', null, 'xE3af7H1XPD6Hv8fzzOXQ63Ml4b33kce0NWS6cuz4I', '', 'Yes', '1', '1', '2017-04-30 06:30:55', '2017-04-30 06:30:55', null, null);
INSERT INTO `spyr42_revisions` VALUES ('44', null, '1a4ab5cb-f239-434b-a05f-23dc99a5e350', 'gs0K1QEv', '', '2', 'users', '16', '9b6d767b-6faa-40c4-a7ba-6ffbd94fc168', 'activation_code', null, 'Y1qYmqov9FJTAkbqjbxZpgEiEhqowMYfh1daYdkMWK', '', 'Yes', '1', '1', '2017-04-30 06:31:34', '2017-04-30 06:31:34', null, null);
INSERT INTO `spyr42_revisions` VALUES ('45', null, '32d2a21b-44a3-48c9-83aa-71273cd962e7', 'tgK6QgPE', '', '2', 'users', '18', '0e2728c8-0956-4c68-ab2c-bd3d4af335a8', 'activation_code', null, 'NNvewzwvrxWpCUn8FfBJvJCLKeV8sbeJFNI8B9upf2', '', 'Yes', '1', '1', '2017-04-30 06:32:15', '2017-04-30 06:32:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('46', '7', 'd61a6537-460c-4789-905c-30399dfe8775', 'lh3asVXL', '', '2', 'users', '18', '0e2728c8-0956-4c68-ab2c-bd3d4af335a8', 'persist_code', null, 'EbH5MvzVQVeCOhbkkJPtfnqUGQf731nYYvvN9f7yJV', '', 'Yes', '1', '1', '2017-04-30 06:36:46', '2017-04-30 06:36:46', null, null);
INSERT INTO `spyr42_revisions` VALUES ('47', '7', 'f629293c-6aaa-4fce-a4c2-00a0942609ff', 'bB1bYGVW', '', '2', 'users', '18', '0e2728c8-0956-4c68-ab2c-bd3d4af335a8', 'last_login', null, '2017-04-30 06:36:46', '', 'Yes', '1', '1', '2017-04-30 06:36:46', '2017-04-30 06:36:46', null, null);
INSERT INTO `spyr42_revisions` VALUES ('48', '7', 'ae14b8d3-a1a3-4bb4-8511-95b0f309f844', 'LyxHg5JS', '', '2', 'users', '2', 'f03ee964-4343-40e2-be55-13321a1a4721', 'group_titles_csv', ',tenant-admin,', ',Customeradmin,', '', 'Yes', '1', '1', '2017-04-30 06:42:39', '2017-04-30 06:42:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('49', '7', 'd6d71708-968e-46ca-ad15-de53cf94439f', 'kkBc9s9s', '', '2', 'users', '2', 'f03ee964-4343-40e2-be55-13321a1a4721', 'group_titles_csv', ',Customeradmin,', ',Customer admin,', '', 'Yes', '1', '1', '2017-04-30 06:45:36', '2017-04-30 06:45:36', null, null);
INSERT INTO `spyr42_revisions` VALUES ('50', null, '63a78a66-2836-4985-bce8-497bc9e32c29', 'NYTGWhw3', '', '2', 'users', '20', 'b5381cc9-39ac-4ad8-aadc-0f8e1d818c9c', 'activation_code', null, 'el52X3gp0vhKW1oFTYVBhwhUE2brVrsLGuPgvurLsx', '', 'Yes', '1', '1', '2017-04-30 06:46:54', '2017-04-30 06:46:54', null, null);
INSERT INTO `spyr42_revisions` VALUES ('51', null, '1a802390-ffa1-4a06-91bc-f34cf1c80548', 'DpAihmgG', '', '2', 'users', '22', '53b44b53-b6e0-4c26-960c-ed21d278851b', 'activation_code', null, 'RY7SgttvTWPAncG0DLIfpODG8PtWzErXGtHR7wS9Sg', '', 'Yes', '1', '1', '2017-04-30 06:50:26', '2017-04-30 06:50:26', null, null);
INSERT INTO `spyr42_revisions` VALUES ('52', null, '13f28bcf-3810-4ccb-b42f-a93c52841624', 'iBLanZlQ', '', '2', 'users', '24', '15f6571e-3aa2-4543-9ecf-932fd56e9174', 'activation_code', null, 'BlrOTexmf4jUE31vhb5bzTeCwLC9Cm5yRRuSB80QyI', '', 'Yes', '1', '1', '2017-04-30 06:52:12', '2017-04-30 06:52:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('53', null, '3f03ece2-c3fd-4a0d-b178-09cb179a1d17', 'MK5JnCtX', '', '2', 'users', '26', 'a214220a-1f60-4345-a789-fd0dce2d451b', 'activation_code', null, 'FsGWehNSnrdOwgJEyZutQRzzdMiPQReOPyYNR5O0Vm', '', 'Yes', '1', '1', '2017-04-30 07:26:33', '2017-04-30 07:26:33', null, null);
INSERT INTO `spyr42_revisions` VALUES ('54', '10', '1b3fb51b-4571-4f44-834b-788d4eeaae3c', '5UTkDGYL', '', '2', 'users', '24', '15f6571e-3aa2-4543-9ecf-932fd56e9174', 'group_ids_csv', ',2,', null, '', 'Yes', '1', '1', '2017-04-30 07:30:07', '2017-04-30 07:30:07', null, null);
INSERT INTO `spyr42_revisions` VALUES ('55', '10', 'db5a6f66-aa95-4517-a8a8-78c4cf3407ea', '5UTkDGYL', '', '2', 'users', '24', '15f6571e-3aa2-4543-9ecf-932fd56e9174', 'group_titles_csv', ',Customer admin,', null, '', 'Yes', '1', '1', '2017-04-30 07:30:07', '2017-04-30 07:30:07', null, null);
INSERT INTO `spyr42_revisions` VALUES ('56', '10', 'e376eac7-76d3-4e57-9964-d3c2f8b9a7fa', '5UTkDGYL', '', '2', 'users', '24', '15f6571e-3aa2-4543-9ecf-932fd56e9174', 'persist_code', null, 'haIhdldoF8cJGQhCRkyTFCNTVPv4G7YMJ4nRcnYbLw', '', 'Yes', '1', '1', '2017-04-30 07:30:07', '2017-04-30 07:30:07', null, null);
INSERT INTO `spyr42_revisions` VALUES ('57', '10', '083970aa-5b6d-45b6-95de-09503c870318', 'A75spgUe', '', '2', 'users', '24', '15f6571e-3aa2-4543-9ecf-932fd56e9174', 'last_login', null, '2017-04-30 07:30:07', '', 'Yes', '1', '1', '2017-04-30 07:30:07', '2017-04-30 07:30:07', null, null);
INSERT INTO `spyr42_revisions` VALUES ('58', null, 'bd000d70-6cb1-487b-b520-fc6d4ce5aac3', 'R0crYzaV', '', '2', 'users', '24', '15f6571e-3aa2-4543-9ecf-932fd56e9174', 'group_ids_csv', null, ',2,', '', 'Yes', '1', '1', '2017-04-30 08:09:55', '2017-04-30 08:09:55', null, null);
INSERT INTO `spyr42_revisions` VALUES ('59', null, '624f976a-f257-4a10-ae94-bea9412bfdd4', 'R0crYzaV', '', '2', 'users', '24', '15f6571e-3aa2-4543-9ecf-932fd56e9174', 'group_titles_csv', null, ',Customer admin,', '', 'Yes', '1', '1', '2017-04-30 08:09:55', '2017-04-30 08:09:55', null, null);
INSERT INTO `spyr42_revisions` VALUES ('60', null, '168fbe71-9851-4445-b861-6b6a2de15f78', 'Ptt70vBA', '', '2', 'users', '23', '526fbb10-634b-4a90-9e3e-5939f5b5531e', 'is_system_user', null, 'Yes', '', 'Yes', '1', '1', '2017-04-30 08:21:24', '2017-04-30 08:21:24', null, null);
INSERT INTO `spyr42_revisions` VALUES ('61', null, 'ef7f2d49-d57e-4ea6-aa64-1e5c46d24a97', 'JfkmTvYK', '', '2', 'users', '28', 'f67068d4-427b-466b-8ac9-80a4701de12e', 'activation_code', null, 'hpyJJYBOnCGo3Dibr5QLz9X1jxdmyp8vtVSrikaEyp', '', 'Yes', '1', '1', '2017-04-30 08:22:11', '2017-04-30 08:22:11', null, null);
INSERT INTO `spyr42_revisions` VALUES ('62', null, 'cfe72675-1ceb-4534-9797-008b14e81928', '9Ti8SOrB', '', '2', 'users', '30', '15d62e9a-869f-4ea7-88ff-41044e8b4120', 'activation_code', null, 'q3OiQTW4ZOT5ur62eeQM54mko2tQg1em10fCRwjMkR', '', 'Yes', '1', '1', '2017-04-30 08:24:43', '2017-04-30 08:24:43', null, null);
INSERT INTO `spyr42_revisions` VALUES ('63', null, 'bb766b4e-b809-4550-bdb2-bae5d4023476', 'ok2riuq0', '', '2', 'users', '33', 'dae69a36-7828-4df6-8142-ccde37e8f486', 'group_ids_csv', '2', null, '', 'Yes', '1', '1', '2017-04-30 08:31:22', '2017-04-30 08:31:22', null, null);
INSERT INTO `spyr42_revisions` VALUES ('64', null, 'd1389731-ad04-419b-9df4-72f38c477de5', 'ok2riuq0', '', '2', 'users', '33', 'dae69a36-7828-4df6-8142-ccde37e8f486', 'group_titles_csv', 'Customer admin', null, '', 'Yes', '1', '1', '2017-04-30 08:31:22', '2017-04-30 08:31:22', null, null);
INSERT INTO `spyr42_revisions` VALUES ('65', null, 'cdac6190-b75e-410c-9fa6-af768ea6e6b6', 'ok2riuq0', '', '2', 'users', '33', 'dae69a36-7828-4df6-8142-ccde37e8f486', 'is_active', 'Yes', 'No', '', 'Yes', '1', '1', '2017-04-30 08:31:22', '2017-04-30 08:31:22', null, null);
INSERT INTO `spyr42_revisions` VALUES ('66', null, '149f85a0-fdcb-4832-93e1-7f617801253b', '6QwQPoJx', '', '2', 'users', '34', '37e70521-7af4-4e65-8e30-ad9d419f72e0', 'activation_code', null, 'JsqSFwkQzfH9k9yuHgOWv6kKvzrylLkQEM7tGkNKyf', '', 'Yes', '1', '1', '2017-04-30 08:37:13', '2017-04-30 08:37:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('67', null, '13bf89c8-5393-4c06-9c26-c17c1e8daabf', 'kJ1zhQLE', '', '2', 'users', '36', '6c22c25c-6514-441f-9469-5552d7f027ca', 'activation_code', null, 'EpIEilWvEQNO8mZaTsOtgl32lxo9qkBlSCNIlSFcis', '', 'Yes', '1', '1', '2017-04-30 08:40:00', '2017-04-30 08:40:00', null, null);
INSERT INTO `spyr42_revisions` VALUES ('68', null, 'd009218b-8eff-4281-856f-4da977dedcfa', 'mmkEZPW1', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'activation_code', null, 'pEe4UbYfS4zG3lqK28FlcqwsbTv9EcQ2rPAzuhuKNK', '', 'Yes', '1', '1', '2017-04-30 08:41:25', '2017-04-30 08:41:25', null, null);
INSERT INTO `spyr42_revisions` VALUES ('69', '18', '384835f4-ab57-4940-b63d-58bb8ac1370d', 'i9lJ4jEU', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', '2', null, '', 'Yes', '1', '1', '2017-04-30 09:58:26', '2017-04-30 09:58:26', null, null);
INSERT INTO `spyr42_revisions` VALUES ('70', '18', 'c6e370c3-9d19-454d-a054-1c325c21d629', 'i9lJ4jEU', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', 'Customer admin', null, '', 'Yes', '1', '1', '2017-04-30 09:58:26', '2017-04-30 09:58:26', null, null);
INSERT INTO `spyr42_revisions` VALUES ('71', '18', 'be4db614-f841-4612-98cc-4c6c9562bc17', 'i9lJ4jEU', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'persist_code', null, 'zNIhJs9RwAb71VCLUZZCfK5ZW6p0i6B1afpgk4TSRN', '', 'Yes', '1', '1', '2017-04-30 09:58:26', '2017-04-30 09:58:26', null, null);
INSERT INTO `spyr42_revisions` VALUES ('72', '18', '930d96f1-3466-47fb-a59c-01bedd3aca1b', 'zj0ThKsE', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', null, '2017-04-30 09:58:26', '', 'Yes', '1', '1', '2017-04-30 09:58:27', '2017-04-30 09:58:27', null, null);
INSERT INTO `spyr42_revisions` VALUES ('73', null, '16b5b89f-c301-4815-8553-c498ee0f083d', 'OECuTXKH', '', '2', 'users', '6', 'cab23d68-e0a3-42f8-a3dd-6f2e78c7f3a9', 'tenant_id', null, '18', '', 'Yes', '1', '1', '2017-04-30 10:13:10', '2017-04-30 10:13:10', null, null);
INSERT INTO `spyr42_revisions` VALUES ('74', null, 'fc5b1fee-e760-483c-bfed-334ab03b857f', 'OECuTXKH', '', '2', 'users', '6', 'cab23d68-e0a3-42f8-a3dd-6f2e78c7f3a9', 'group_titles_csv', ',tenant-admin,', ',Customer admin,', '', 'Yes', '1', '1', '2017-04-30 10:13:10', '2017-04-30 10:13:10', null, null);
INSERT INTO `spyr42_revisions` VALUES ('75', null, 'c0a4b07f-0a5a-4899-a987-bf0a0366bdc9', 'OECuTXKH', '', '2', 'users', '6', 'cab23d68-e0a3-42f8-a3dd-6f2e78c7f3a9', 'is_system_user', null, 'No', '', 'Yes', '1', '1', '2017-04-30 10:13:10', '2017-04-30 10:13:10', null, null);
INSERT INTO `spyr42_revisions` VALUES ('76', null, '1994e47f-95fd-4dcb-949f-3cb1d3121a26', '2cQtu94D', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', null, ',2,', '', 'Yes', '1', '1', '2017-04-30 10:14:29', '2017-04-30 10:14:29', null, null);
INSERT INTO `spyr42_revisions` VALUES ('77', null, 'fd4433ec-ab27-453b-813d-88e42eb60b41', '2cQtu94D', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', null, ',Customer admin,', '', 'Yes', '1', '1', '2017-04-30 10:14:29', '2017-04-30 10:14:29', null, null);
INSERT INTO `spyr42_revisions` VALUES ('78', null, '84e8e408-dd96-454c-b9c1-43bfbfb3a881', '2cQtu94D', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'is_system_user', null, 'No', '', 'Yes', '1', '1', '2017-04-30 10:14:29', '2017-04-30 10:14:29', null, null);
INSERT INTO `spyr42_revisions` VALUES ('79', null, 'c99c96c3-3697-4b50-b582-dbadf86d0dc5', 'CDjRQTbr', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-04-30 05:04:18', '2017-05-02 06:49:14', '', 'Yes', '1', '1', '2017-05-02 06:49:14', '2017-05-02 06:49:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('80', '18', '237e29fb-2351-4fb6-aa83-62ee8fcbf5cf', 'YExWdO6T', '', '2', 'users', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'group_ids_csv', ',2,', null, '', 'Yes', '38', '38', '2017-05-02 06:49:35', '2017-05-02 06:49:35', null, null);
INSERT INTO `spyr42_revisions` VALUES ('81', '18', '2dabaaad-941d-4676-a0df-8aaca3e5c425', 'YExWdO6T', '', '2', 'users', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'group_titles_csv', ',Customer admin,', null, '', 'Yes', '38', '38', '2017-05-02 06:49:35', '2017-05-02 06:49:35', null, null);
INSERT INTO `spyr42_revisions` VALUES ('82', '18', '12e6a17f-4f92-4169-adeb-42100e879433', 'YExWdO6T', '', '2', 'users', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'persist_code', null, 'TRkcb1xrMA2T1OzVnXOwiQ73Q1uWzFFaKltOICaeTY', '', 'Yes', '38', '38', '2017-05-02 06:49:35', '2017-05-02 06:49:35', null, null);
INSERT INTO `spyr42_revisions` VALUES ('83', '18', '076dd79e-f489-4222-bb7b-6ede7aca30e9', 'iLoYpNox', '', '2', 'users', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'last_login', null, '2017-05-02 06:49:36', '', 'Yes', '38', '38', '2017-05-02 06:49:36', '2017-05-02 06:49:36', null, null);
INSERT INTO `spyr42_revisions` VALUES ('84', null, 'b29e249e-3ed4-492f-b324-56a3f3cc7a46', 'dKI3UtOM', '', '2', 'users', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'group_ids_csv', null, ',2,', '', 'Yes', '38', '38', '2017-05-02 06:59:31', '2017-05-02 06:59:31', null, null);
INSERT INTO `spyr42_revisions` VALUES ('85', null, 'd0d034a4-f63d-4917-83b9-bd2b67519077', 'dKI3UtOM', '', '2', 'users', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'group_titles_csv', null, ',Customer admin,', '', 'Yes', '38', '38', '2017-05-02 06:59:31', '2017-05-02 06:59:31', null, null);
INSERT INTO `spyr42_revisions` VALUES ('86', null, '5b7f8651-cc1e-4547-b805-a59e6874b6ac', 'd2Hk8jOM', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-02 06:49:14', '2017-05-02 10:54:17', '', 'Yes', '1', '1', '2017-05-02 10:54:18', '2017-05-02 10:54:18', null, null);
INSERT INTO `spyr42_revisions` VALUES ('87', '18', 'db4e8840-79d3-4dc8-86f6-7f94f2fa283e', 'VlZ8g34Z', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', ',2,', null, '', 'Yes', '1', '1', '2017-05-03 11:09:34', '2017-05-03 11:09:34', null, null);
INSERT INTO `spyr42_revisions` VALUES ('88', '18', '71e0fe64-2565-41ba-a99f-94602a4b3f3b', 'VlZ8g34Z', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', ',Customer admin,', null, '', 'Yes', '1', '1', '2017-05-03 11:09:34', '2017-05-03 11:09:34', null, null);
INSERT INTO `spyr42_revisions` VALUES ('89', '18', '7dedb552-a089-45b8-93c3-66b0eb0cb076', 'VlZ8g34Z', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-04-30 09:58:26', '2017-05-03 11:09:33', '', 'Yes', '1', '1', '2017-05-03 11:09:34', '2017-05-03 11:09:34', null, null);
INSERT INTO `spyr42_revisions` VALUES ('90', null, '444f72fa-a7a2-4c7e-bc98-62188201d1e9', 'kNnfQPiv', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-02 10:54:17', '2017-05-03 11:09:45', '', 'Yes', '1', '1', '2017-05-03 11:09:45', '2017-05-03 11:09:45', null, null);
INSERT INTO `spyr42_revisions` VALUES ('91', null, '79478e93-a764-412d-a9c8-b4efb8bf8510', 'MPPeDabn', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', null, ',2,', '', 'Yes', '1', '1', '2017-05-03 11:10:06', '2017-05-03 11:10:06', null, null);
INSERT INTO `spyr42_revisions` VALUES ('92', null, '04ecd97f-66e9-4f92-bbc0-0091d8f93df3', 'MPPeDabn', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', null, ',Customer admin,', '', 'Yes', '1', '1', '2017-05-03 11:10:06', '2017-05-03 11:10:06', null, null);
INSERT INTO `spyr42_revisions` VALUES ('93', null, '7b6a14a3-5784-48ad-896a-2f442aa8c59e', 'pGGN5kfU', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-03 11:09:45', '2017-05-04 06:51:27', '', 'Yes', '1', '1', '2017-05-04 06:51:29', '2017-05-04 06:51:29', null, null);
INSERT INTO `spyr42_revisions` VALUES ('94', '18', 'c4b43a3c-926b-4772-bbbd-17c4d321416d', 'xWhgR79j', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', ',2,', null, '', 'Yes', '1', '1', '2017-05-04 07:05:46', '2017-05-04 07:05:46', null, null);
INSERT INTO `spyr42_revisions` VALUES ('95', '18', 'cbb697a9-2f4b-4d6d-ace1-25aaa8143eaa', 'xWhgR79j', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', ',Customer admin,', null, '', 'Yes', '1', '1', '2017-05-04 07:05:46', '2017-05-04 07:05:46', null, null);
INSERT INTO `spyr42_revisions` VALUES ('96', '18', '55b5516f-306b-4805-b75c-d8441175c919', 'xWhgR79j', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-03 11:09:33', '2017-05-04 07:05:46', '', 'Yes', '1', '1', '2017-05-04 07:05:46', '2017-05-04 07:05:46', null, null);
INSERT INTO `spyr42_revisions` VALUES ('97', null, 'a84c91c8-a2f5-44c9-98b6-653f9580de98', 'ygUky1wY', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', null, ',2,', '', 'Yes', '1', '1', '2017-05-04 07:06:59', '2017-05-04 07:06:59', null, null);
INSERT INTO `spyr42_revisions` VALUES ('98', null, 'ea25c6cf-995a-4f4a-a0fa-54969a22fcb1', 'ygUky1wY', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', null, ',Customer admin,', '', 'Yes', '1', '1', '2017-05-04 07:06:59', '2017-05-04 07:06:59', null, null);
INSERT INTO `spyr42_revisions` VALUES ('99', null, 'abf20051-cd12-45e8-b6ea-f17c4e6fd615', 'gfjAGUzh', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-04 06:51:27', '2017-05-04 11:42:35', '', 'Yes', '1', '1', '2017-05-04 11:42:36', '2017-05-04 11:42:36', null, null);
INSERT INTO `spyr42_revisions` VALUES ('100', null, '938fc73c-3b52-466b-b779-309635e4f246', 'riXYvvxG', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-04 11:42:35', '2017-05-04 12:15:09', '', 'Yes', '1', '1', '2017-05-04 12:15:09', '2017-05-04 12:15:09', null, null);
INSERT INTO `spyr42_revisions` VALUES ('101', null, '22cbed20-75fe-4c04-8c96-1af37ca1bade', 'MAiErwFm', '', '1', 'modules', '13', '0d046744-e724-479e-ad14-3c4d70139e58', 'desc', 'lorel&nbsp;', 'lorel ipsum', '', null, '1', '1', '2017-05-04 13:43:16', '2017-05-04 13:43:16', null, null);
INSERT INTO `spyr42_revisions` VALUES ('102', null, '1f722e50-16a0-4475-9d92-1240752e320f', 'MAiErwFm', '', '1', 'modules', '13', '0d046744-e724-479e-ad14-3c4d70139e58', 'has_uploads', null, 'Yes', '', null, '1', '1', '2017-05-04 13:43:16', '2017-05-04 13:43:16', null, null);
INSERT INTO `spyr42_revisions` VALUES ('103', null, '1edcaad6-5e92-418e-be12-887ef345168b', 'MAiErwFm', '', '1', 'modules', '13', '0d046744-e724-479e-ad14-3c4d70139e58', 'has_messages', null, 'Yes', '', null, '1', '1', '2017-05-04 13:43:16', '2017-05-04 13:43:16', null, null);
INSERT INTO `spyr42_revisions` VALUES ('104', '18', '05ba6145-e292-4094-a0c7-0d89f50c6698', 'z0lWXfbS', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', ',2,', null, '', null, '1', '1', '2017-05-07 05:49:04', '2017-05-07 05:49:04', null, null);
INSERT INTO `spyr42_revisions` VALUES ('105', '18', '57e038b3-fa77-4f7b-909a-95c6de04bafe', 'z0lWXfbS', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', ',Customer admin,', null, '', null, '1', '1', '2017-05-07 05:49:05', '2017-05-07 05:49:05', null, null);
INSERT INTO `spyr42_revisions` VALUES ('106', '18', 'd38a146b-1437-48a7-992a-1cd30ad401b6', 'z0lWXfbS', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-04 07:05:46', '2017-05-07 05:49:04', '', null, '1', '1', '2017-05-07 05:49:05', '2017-05-07 05:49:05', null, null);
INSERT INTO `spyr42_revisions` VALUES ('107', null, '964efdf2-4b84-4f25-a734-70658c88279d', 'YopC6jbK', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-04 12:15:09', '2017-05-07 05:50:35', '', null, '1', '1', '2017-05-07 05:50:35', '2017-05-07 05:50:35', null, null);
INSERT INTO `spyr42_revisions` VALUES ('108', null, '292b7576-55ea-4611-8778-96204f30c9de', 'ycSjnvSO', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', null, ',2,', '', null, '1', '1', '2017-05-07 06:02:59', '2017-05-07 06:02:59', null, null);
INSERT INTO `spyr42_revisions` VALUES ('109', null, 'd38859ed-48e4-413f-8fa3-f989da38606c', 'ycSjnvSO', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', null, ',Customer admin,', '', null, '1', '1', '2017-05-07 06:02:59', '2017-05-07 06:02:59', null, null);
INSERT INTO `spyr42_revisions` VALUES ('110', '18', '8eaeded6-2e78-4754-8bb8-8ccc08e2e23d', 'An0QMUIG', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', ',2,', null, '', null, '1', '1', '2017-05-07 06:03:30', '2017-05-07 06:03:30', null, null);
INSERT INTO `spyr42_revisions` VALUES ('111', '18', 'a6a32c69-38cb-4929-a81f-d6767021b8d7', 'An0QMUIG', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', ',Customer admin,', null, '', null, '1', '1', '2017-05-07 06:03:30', '2017-05-07 06:03:30', null, null);
INSERT INTO `spyr42_revisions` VALUES ('112', '18', 'f4ce413e-36f9-49f4-8bfb-dac00a643a39', 'An0QMUIG', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-07 05:49:04', '2017-05-07 06:03:30', '', null, '1', '1', '2017-05-07 06:03:30', '2017-05-07 06:03:30', null, null);
INSERT INTO `spyr42_revisions` VALUES ('113', null, 'f89927fe-06be-4830-ace9-5993b2b192ef', 'TEE9WIRU', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', null, ',2,', '', null, '1', '1', '2017-05-07 06:04:09', '2017-05-07 06:04:09', null, null);
INSERT INTO `spyr42_revisions` VALUES ('114', null, '86e24f3e-9874-4cfb-818d-d3821c596d8f', 'TEE9WIRU', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', null, ',Customer admin,', '', null, '1', '1', '2017-05-07 06:04:09', '2017-05-07 06:04:09', null, null);
INSERT INTO `spyr42_revisions` VALUES ('115', '18', '9e75349a-1065-4b11-bb8e-53cf0b36b5d0', 'IwCZ9fux', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', ',2,', null, '', null, '1', '1', '2017-05-07 06:04:39', '2017-05-07 06:04:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('116', '18', '6a6d35d7-6a29-483c-91e2-75fdd823fb76', 'IwCZ9fux', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', ',Customer admin,', null, '', null, '1', '1', '2017-05-07 06:04:39', '2017-05-07 06:04:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('117', '18', '13614a58-efc7-4121-88c8-833a41b7ab4a', 'IwCZ9fux', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-07 06:03:30', '2017-05-07 06:04:39', '', null, '1', '1', '2017-05-07 06:04:39', '2017-05-07 06:04:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('118', null, '1b9bc82d-b126-4f64-aa59-acee6936576f', 'gQIvfESe', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_ids_csv', null, ',2,', '', null, '1', '1', '2017-05-07 06:05:12', '2017-05-07 06:05:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('119', null, '65347979-a16a-45c4-a34e-5b97a5795e12', 'gQIvfESe', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'group_titles_csv', null, ',Customer admin,', '', null, '1', '1', '2017-05-07 06:05:12', '2017-05-07 06:05:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('120', '18', 'fc5e432c-5927-4319-85ba-d0ebddeb5476', 'Xe7FGQf9', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-07 06:04:39', '2017-05-07 06:08:39', '', null, '1', '1', '2017-05-07 06:08:39', '2017-05-07 06:08:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('121', '18', 'a41b682e-958a-4971-8d7b-b59b80efae15', 'R8hbXWtD', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-07 06:08:39', '2017-05-07 06:11:19', '', null, '1', '1', '2017-05-07 06:11:19', '2017-05-07 06:11:19', null, null);
INSERT INTO `spyr42_revisions` VALUES ('122', null, '986a817c-11e7-4f6d-a7db-dcdaa2459a6c', '38ydSsPw', '', '2', 'users', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'is_editable_by_tenant', 'No', 'Yes', '', null, '38', '38', '2017-05-07 07:38:15', '2017-05-07 07:38:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('123', '18', 'd8757257-a3ee-4768-935e-7b0989964bac', 'UuE14jnn', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-07 06:11:19', '2017-05-07 10:09:39', '', null, '1', '1', '2017-05-07 10:09:39', '2017-05-07 10:09:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('124', null, '618711ba-4bef-4a85-9c33-318ae1d665ee', 'rkuq70xH', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-07 05:50:35', '2017-05-07 10:20:06', '', null, '1', '1', '2017-05-07 10:20:06', '2017-05-07 10:20:06', null, null);
INSERT INTO `spyr42_revisions` VALUES ('125', '18', '3e7b7eca-fb19-4c34-ab7d-d0fa9be8e8b5', 'QzvJuq8r', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-07 10:09:39', '2017-05-09 06:03:57', '', null, '1', '1', '2017-05-09 06:03:58', '2017-05-09 06:03:58', null, null);
INSERT INTO `spyr42_revisions` VALUES ('126', null, 'f91424a5-6dda-4489-898e-26ebbe0f2117', 'dK6llZSc', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-07 10:20:06', '2017-05-09 06:57:50', '', null, '1', '1', '2017-05-09 06:57:51', '2017-05-09 06:57:51', null, null);
INSERT INTO `spyr42_revisions` VALUES ('127', '18', '78e8abbd-955f-48d5-999e-011943f713d3', 'xPDx7Rv0', '', '12', 'uploads', '28', 'be78984b-2731-4be6-908e-3b94d05f86a0', 'desc', null, 'lorel ipsum', '', null, '38', '38', '2017-05-09 07:42:38', '2017-05-09 07:42:38', null, null);
INSERT INTO `spyr42_revisions` VALUES ('128', null, '2dd64139-5c60-4008-b432-468d2b73548b', 'IwjhCRdQ', '', '12', 'uploads', '1', 'b5cd6339-3915-4828-a051-0d80c842a48a', 'ext', null, 'png', '', null, '2', '2', '2017-05-09 08:31:13', '2017-05-09 08:31:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('129', null, '673f0e5c-8a5e-40bd-a30d-5a20dcac4d81', 'HAdly6Fn', '', '12', 'uploads', '2', 'ba662af7-a01a-4ed2-b1f0-b993e49b3764', 'ext', null, 'png', '', null, '2', '2', '2017-05-09 08:31:13', '2017-05-09 08:31:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('130', null, 'e9fc8585-e2f6-4b99-a2fe-1263b110309b', 'iR8cJtiT', '', '12', 'uploads', '3', '1930ebcd-f816-4bfa-a6c2-b8a8fe1698d9', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:13', '2017-05-09 08:31:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('131', null, '9decb14c-ff76-42fe-bb4d-35c37b4d137f', 'UZm8ZKh9', '', '12', 'uploads', '4', '6dd981b7-1c78-420d-89a7-562b842ee778', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:13', '2017-05-09 08:31:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('132', null, '3dbd9c3b-426f-4af8-8c61-6087c777013d', 'MmDkDYq9', '', '12', 'uploads', '5', 'cf55038b-0f47-4787-a039-1ce35f1f6bd9', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:14', '2017-05-09 08:31:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('133', null, '8bba9f78-1c24-4a0f-a482-852d884fced1', 'gwjvIudo', '', '12', 'uploads', '6', '5a740f73-4a63-4e75-8a18-094ae373e066', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:31:14', '2017-05-09 08:31:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('134', null, 'cd188051-3f87-4dba-8315-3bfac7c8c9ac', 'm1nSrXUL', '', '12', 'uploads', '7', '01f8423d-a501-4f25-9682-a9566f7c7cb7', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:14', '2017-05-09 08:31:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('135', null, '0f69e881-54e5-4557-ab15-61e71b18eecb', 'YOLFbjAZ', '', '12', 'uploads', '8', 'af05bb54-caf4-4a70-8ee7-860749d00c42', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:14', '2017-05-09 08:31:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('136', null, '962a9a48-1aaa-44a7-b4cf-b436219d6a1d', '58NCc0Ds', '', '12', 'uploads', '9', '3f021bdf-0240-4b3e-bba4-ec04d1183824', 'ext', null, 'xls', '', null, '1', '1', '2017-05-09 08:31:14', '2017-05-09 08:31:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('137', null, 'a266f691-876a-4b12-a4f5-d374b8b56f84', 'Lgkpbo5s', '', '12', 'uploads', '10', '551682e7-fc19-4e8a-9ee7-8a7b086e3b3e', 'ext', null, 'pdf', '', null, '1', '1', '2017-05-09 08:31:14', '2017-05-09 08:31:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('138', null, 'a1e8b122-3a38-49ab-9268-3f2cc498d343', 'DqixfR9k', '', '12', 'uploads', '11', 'f32c3cae-b11a-495c-8905-5fb8ae1b0a8d', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:14', '2017-05-09 08:31:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('139', null, 'b9d3146d-544e-4495-b685-788693d23b3e', 'gNSXrWXg', '', '12', 'uploads', '12', 'ce186425-1b4f-4cc9-b5d3-fc25dfaa4efe', 'ext', null, 'jpg', '', null, '1', '1', '2017-05-09 08:31:15', '2017-05-09 08:31:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('140', null, '3787419d-b0b8-46df-b1db-ff258c0185db', 'CaDmsIXM', '', '12', 'uploads', '13', '2c848ee8-b8bf-4ddf-86c6-cb67ee619479', 'ext', null, 'docx', '', null, '1', '1', '2017-05-09 08:31:15', '2017-05-09 08:31:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('141', null, '893d8613-51c5-4367-9048-0c3a7b15255e', 'UbvNwLMl', '', '12', 'uploads', '14', '0ce6ff2a-6251-4fc5-98da-47c4fb277f44', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:15', '2017-05-09 08:31:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('142', null, 'f7c93a6f-9f74-4a4f-ba34-3a7716e0597d', 'GOjbmitM', '', '12', 'uploads', '15', '22db8a51-ac37-47f8-98f6-9aa26afbb626', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:15', '2017-05-09 08:31:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('143', null, 'd4a3afba-ffbc-428c-8299-5c492959eaa1', 'wKaGTVSC', '', '12', 'uploads', '16', '6015e283-3d22-4312-97fd-6d97b30dac05', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:15', '2017-05-09 08:31:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('144', null, '5704995f-08f4-4beb-b2d0-7583d1f704b3', 'U7lIZt90', '', '12', 'uploads', '17', 'c95a020e-79a3-4113-9747-e3f3e32498ef', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:15', '2017-05-09 08:31:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('145', null, 'ca410fa3-87d4-4276-8327-452eda11d24c', 'o3D0BlcM', '', '12', 'uploads', '18', '7066a7c2-f002-40a8-990e-2c73c691e1f4', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:15', '2017-05-09 08:31:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('146', null, '98fca37d-b3f0-4801-a5de-7d8c3845e503', 'Hzi5M58f', '', '12', 'uploads', '19', '93e92624-222b-4c59-8221-77130a3d3f97', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:15', '2017-05-09 08:31:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('147', null, 'a4ab85cc-526f-4e94-8f2e-ae5dd53c61af', 'BUGXtGQx', '', '12', 'uploads', '20', '2a579465-7b16-4a74-a9f8-725fd38c22ed', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:15', '2017-05-09 08:31:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('148', null, '06b09580-9c98-4b58-b5cb-f1e3527f54e3', 'fHOwja5S', '', '12', 'uploads', '21', 'f5052f64-869c-4e0d-8ce2-8fa04e423c58', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:16', '2017-05-09 08:31:16', null, null);
INSERT INTO `spyr42_revisions` VALUES ('149', null, '3c1c1dfa-dc68-4d92-a7ea-1437d297c0d8', '2HbYKuzh', '', '12', 'uploads', '22', '3658fd83-92a4-4cd6-a539-8879a2cb9cde', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:16', '2017-05-09 08:31:16', null, null);
INSERT INTO `spyr42_revisions` VALUES ('150', null, '3d6c1641-f522-4511-890e-58b12fef6f2a', 'Y9zhndv2', '', '12', 'uploads', '23', 'c7f61745-7cfd-4090-bf60-f107cb5a691e', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:16', '2017-05-09 08:31:16', null, null);
INSERT INTO `spyr42_revisions` VALUES ('151', null, '9f375958-8a8a-4348-8dd6-329f5eb4f3f7', 'VAPoql2F', '', '12', 'uploads', '24', '7eb660ab-54db-435e-b080-bfc97bb8394c', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:16', '2017-05-09 08:31:16', null, null);
INSERT INTO `spyr42_revisions` VALUES ('152', null, '8f6b0b67-a713-4580-9560-f06be818ec1a', 'HIIYFdD0', '', '12', 'uploads', '25', '8762f1bf-4cd6-403e-bb77-90aa79f5d3a8', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:16', '2017-05-09 08:31:16', null, null);
INSERT INTO `spyr42_revisions` VALUES ('153', null, '42645250-f9e9-4df3-86c0-110977790bdf', '2aYrN2OL', '', '12', 'uploads', '26', 'cf8cf28e-81c7-453f-aced-d1e5840b9a44', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:16', '2017-05-09 08:31:16', null, null);
INSERT INTO `spyr42_revisions` VALUES ('154', null, '58c18bee-954e-403e-a4f2-b9be2888d475', 'aoCpfvLL', '', '12', 'uploads', '27', '4543ab72-68a5-4284-8095-cd0ab27dd6eb', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:31:16', '2017-05-09 08:31:16', null, null);
INSERT INTO `spyr42_revisions` VALUES ('155', null, '690b20e8-587d-4420-a5dd-0d8e3706b8d6', 'RVBJDfFp', '', '12', 'uploads', '28', 'be78984b-2731-4be6-908e-3b94d05f86a0', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:31:17', '2017-05-09 08:31:17', null, null);
INSERT INTO `spyr42_revisions` VALUES ('156', null, 'f04cc9be-abaf-403f-8ed9-5caf7537fdd3', 'hVEjbpsX', '', '12', 'uploads', '29', '85ed30aa-a314-4298-85ca-3cc83803a63e', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:31:17', '2017-05-09 08:31:17', null, null);
INSERT INTO `spyr42_revisions` VALUES ('157', null, '13db009c-0b8a-45a6-87d0-3ae0361b84cc', 'Nbiu4yAr', '', '12', 'uploads', '30', '042b9fe9-4512-4168-89de-dad75e7d442f', 'ext', null, 'jpg', '', null, '38', '38', '2017-05-09 08:31:17', '2017-05-09 08:31:17', null, null);
INSERT INTO `spyr42_revisions` VALUES ('158', null, '5f07ab45-710b-481c-81f0-0b7e4587be7e', 'Nbiu4yAr', '', '12', 'uploads', '30', '042b9fe9-4512-4168-89de-dad75e7d442f', 'is_active', null, 'Yes', '', null, '38', '38', '2017-05-09 08:31:17', '2017-05-09 08:31:17', null, null);
INSERT INTO `spyr42_revisions` VALUES ('159', null, '90817e2c-63c8-417e-ad75-a93685790c13', '7CjZ9qbH', '', '12', 'uploads', '31', 'd0bf3951-f3ea-4e22-8b4b-d9823c77cc68', 'ext', null, 'jpg', '', null, '38', '38', '2017-05-09 08:31:17', '2017-05-09 08:31:17', null, null);
INSERT INTO `spyr42_revisions` VALUES ('160', null, '48273934-3741-4da2-b625-7ec056ab2771', '7CjZ9qbH', '', '12', 'uploads', '31', 'd0bf3951-f3ea-4e22-8b4b-d9823c77cc68', 'is_active', null, 'Yes', '', null, '38', '38', '2017-05-09 08:31:17', '2017-05-09 08:31:17', null, null);
INSERT INTO `spyr42_revisions` VALUES ('161', null, 'dd438bf0-24e3-4de6-8c05-c9a8fd04e0e2', 'Kve4THew', '', '12', 'uploads', '1', 'b5cd6339-3915-4828-a051-0d80c842a48a', 'ext', null, 'png', '', null, '2', '2', '2017-05-09 08:31:52', '2017-05-09 08:31:52', null, null);
INSERT INTO `spyr42_revisions` VALUES ('162', null, '408eb12f-b024-4db9-9726-82369821adee', 'UhZCP0LA', '', '12', 'uploads', '2', 'ba662af7-a01a-4ed2-b1f0-b993e49b3764', 'ext', null, 'png', '', null, '2', '2', '2017-05-09 08:31:52', '2017-05-09 08:31:52', null, null);
INSERT INTO `spyr42_revisions` VALUES ('163', null, '7f7d032f-6d15-4b3e-b16b-5748923daf0f', 'UWOHxwbq', '', '12', 'uploads', '3', '1930ebcd-f816-4bfa-a6c2-b8a8fe1698d9', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:52', '2017-05-09 08:31:52', null, null);
INSERT INTO `spyr42_revisions` VALUES ('164', null, '524c2b3c-3fb1-4e5c-b532-eec0b56b5bd9', 'l66ct0Mg', '', '12', 'uploads', '4', '6dd981b7-1c78-420d-89a7-562b842ee778', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:52', '2017-05-09 08:31:52', null, null);
INSERT INTO `spyr42_revisions` VALUES ('165', null, '9035ea11-a739-4181-a961-08d636717039', '7g4kkyJ0', '', '12', 'uploads', '5', 'cf55038b-0f47-4787-a039-1ce35f1f6bd9', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:53', '2017-05-09 08:31:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('166', null, '3b339f09-2542-4432-b335-44479b501418', 'UsT99q9G', '', '12', 'uploads', '6', '5a740f73-4a63-4e75-8a18-094ae373e066', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:31:53', '2017-05-09 08:31:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('167', null, 'bab1a661-90e3-4a75-a1f8-8a67342254f9', 'C1uMd5XE', '', '12', 'uploads', '7', '01f8423d-a501-4f25-9682-a9566f7c7cb7', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:53', '2017-05-09 08:31:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('168', null, '5ccc4fdc-59ea-4b81-b268-ffbfdec52579', 'n8UyejtP', '', '12', 'uploads', '8', 'af05bb54-caf4-4a70-8ee7-860749d00c42', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:53', '2017-05-09 08:31:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('169', null, '941ec37b-6d83-4c0a-a94c-1d081d4704bd', 'sXEr6A4A', '', '12', 'uploads', '9', '3f021bdf-0240-4b3e-bba4-ec04d1183824', 'ext', null, 'xls', '', null, '1', '1', '2017-05-09 08:31:53', '2017-05-09 08:31:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('170', null, '506e9f9c-9fb4-4e3a-8faf-584cf65d73ff', 'b6VuSyRW', '', '12', 'uploads', '10', '551682e7-fc19-4e8a-9ee7-8a7b086e3b3e', 'ext', null, 'pdf', '', null, '1', '1', '2017-05-09 08:31:53', '2017-05-09 08:31:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('171', null, '3e9fc5ee-f8b5-4ad8-85fb-96cab1a0b26f', 'NbOWlx2p', '', '12', 'uploads', '11', 'f32c3cae-b11a-495c-8905-5fb8ae1b0a8d', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:53', '2017-05-09 08:31:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('172', null, 'd57690aa-ffe5-4c05-8c23-a7a445850b30', '1coXuufk', '', '12', 'uploads', '12', 'ce186425-1b4f-4cc9-b5d3-fc25dfaa4efe', 'ext', null, 'jpg', '', null, '1', '1', '2017-05-09 08:31:53', '2017-05-09 08:31:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('173', null, '0cd3ba3f-5516-4c0c-a8b5-6e8bc15db04b', 'NLeRWxhc', '', '12', 'uploads', '13', '2c848ee8-b8bf-4ddf-86c6-cb67ee619479', 'ext', null, 'docx', '', null, '1', '1', '2017-05-09 08:31:54', '2017-05-09 08:31:54', null, null);
INSERT INTO `spyr42_revisions` VALUES ('174', null, 'f586dd24-3b4c-4475-9730-729f28a191e0', '67iyhKWo', '', '12', 'uploads', '14', '0ce6ff2a-6251-4fc5-98da-47c4fb277f44', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:54', '2017-05-09 08:31:54', null, null);
INSERT INTO `spyr42_revisions` VALUES ('175', null, 'e5883290-7f31-44c1-adf4-33f1175f9fd7', 'kZkN9P8j', '', '12', 'uploads', '15', '22db8a51-ac37-47f8-98f6-9aa26afbb626', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:54', '2017-05-09 08:31:54', null, null);
INSERT INTO `spyr42_revisions` VALUES ('176', null, '8a5b2c9c-0cae-4635-a363-37ee82b6248f', 'vmIEbypi', '', '12', 'uploads', '16', '6015e283-3d22-4312-97fd-6d97b30dac05', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:54', '2017-05-09 08:31:54', null, null);
INSERT INTO `spyr42_revisions` VALUES ('177', null, '21a2163b-0db4-4bbe-8bbf-a365299fa4da', 'P30GiYoU', '', '12', 'uploads', '17', 'c95a020e-79a3-4113-9747-e3f3e32498ef', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:54', '2017-05-09 08:31:54', null, null);
INSERT INTO `spyr42_revisions` VALUES ('178', null, 'db514aa3-69e8-42e5-8ca1-7420d4a05d7c', 'EoYhrtmx', '', '12', 'uploads', '18', '7066a7c2-f002-40a8-990e-2c73c691e1f4', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:54', '2017-05-09 08:31:54', null, null);
INSERT INTO `spyr42_revisions` VALUES ('179', null, '3802aa70-cde6-464c-a0ec-f13839a95e87', 'SChcOwBK', '', '12', 'uploads', '19', '93e92624-222b-4c59-8221-77130a3d3f97', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:54', '2017-05-09 08:31:54', null, null);
INSERT INTO `spyr42_revisions` VALUES ('180', null, 'efe35d57-1e9b-4e26-93b8-99c08c7e0502', 'IIGszNv5', '', '12', 'uploads', '20', '2a579465-7b16-4a74-a9f8-725fd38c22ed', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:54', '2017-05-09 08:31:54', null, null);
INSERT INTO `spyr42_revisions` VALUES ('181', null, 'bf3df329-690a-44a0-a07f-a1624d048ed3', 'Jb3yodp7', '', '12', 'uploads', '21', 'f5052f64-869c-4e0d-8ce2-8fa04e423c58', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:55', '2017-05-09 08:31:55', null, null);
INSERT INTO `spyr42_revisions` VALUES ('182', null, '7d05634c-7be7-4ed4-b158-29724da4fbe8', 'lHe5d42R', '', '12', 'uploads', '22', '3658fd83-92a4-4cd6-a539-8879a2cb9cde', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:55', '2017-05-09 08:31:55', null, null);
INSERT INTO `spyr42_revisions` VALUES ('183', null, 'c2abbad6-f6a5-436f-ad73-76f63f4a128b', 'Id1Q6QSH', '', '12', 'uploads', '23', 'c7f61745-7cfd-4090-bf60-f107cb5a691e', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:55', '2017-05-09 08:31:55', null, null);
INSERT INTO `spyr42_revisions` VALUES ('184', null, 'fe5693ff-ae79-40a3-bc4a-59c5d3516aa6', 'EhWjzKdb', '', '12', 'uploads', '24', '7eb660ab-54db-435e-b080-bfc97bb8394c', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:55', '2017-05-09 08:31:55', null, null);
INSERT INTO `spyr42_revisions` VALUES ('185', null, 'ab8ae03e-2e0e-48d8-8c40-72862f74408e', 'CGZbGxMB', '', '12', 'uploads', '25', '8762f1bf-4cd6-403e-bb77-90aa79f5d3a8', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:55', '2017-05-09 08:31:55', null, null);
INSERT INTO `spyr42_revisions` VALUES ('186', null, 'a1cfd2bf-3638-46b0-b096-ee99f71db6c7', 'XV1QsxIF', '', '12', 'uploads', '26', 'cf8cf28e-81c7-453f-aced-d1e5840b9a44', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:31:55', '2017-05-09 08:31:55', null, null);
INSERT INTO `spyr42_revisions` VALUES ('187', null, 'b84a2ca5-dcd4-484b-8d0b-890e02d8d0f9', 'BAFjVqvd', '', '12', 'uploads', '27', '4543ab72-68a5-4284-8095-cd0ab27dd6eb', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:31:56', '2017-05-09 08:31:56', null, null);
INSERT INTO `spyr42_revisions` VALUES ('188', null, 'fc01800d-95e5-4668-888e-ec63508a743e', 'HHt2aGX1', '', '12', 'uploads', '28', 'be78984b-2731-4be6-908e-3b94d05f86a0', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:31:56', '2017-05-09 08:31:56', null, null);
INSERT INTO `spyr42_revisions` VALUES ('189', null, 'f7266503-76ab-4737-8964-8249080a97e3', 'Qhp1vekP', '', '12', 'uploads', '29', '85ed30aa-a314-4298-85ca-3cc83803a63e', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:31:56', '2017-05-09 08:31:56', null, null);
INSERT INTO `spyr42_revisions` VALUES ('190', null, '291cf60f-f9a4-4435-83a4-50475394cc54', '1x1UOjod', '', '12', 'uploads', '30', '042b9fe9-4512-4168-89de-dad75e7d442f', 'ext', null, 'jpg', '', null, '38', '38', '2017-05-09 08:31:56', '2017-05-09 08:31:56', null, null);
INSERT INTO `spyr42_revisions` VALUES ('191', null, 'af306402-d5aa-4d4b-8f0c-e11443888f89', '0nT473fU', '', '12', 'uploads', '31', 'd0bf3951-f3ea-4e22-8b4b-d9823c77cc68', 'ext', null, 'jpg', '', null, '38', '38', '2017-05-09 08:31:56', '2017-05-09 08:31:56', null, null);
INSERT INTO `spyr42_revisions` VALUES ('192', null, 'e6cb56b6-14ef-4d4a-bb3a-3985687064d9', 'Q4wY9Tyw', '', '12', 'uploads', '1', 'b5cd6339-3915-4828-a051-0d80c842a48a', 'ext', null, 'png', '', null, '2', '2', '2017-05-09 08:33:11', '2017-05-09 08:33:11', null, null);
INSERT INTO `spyr42_revisions` VALUES ('193', null, '62bfb686-a253-4cf4-9c85-92a97303ff1c', 'M6u34Zan', '', '12', 'uploads', '2', 'ba662af7-a01a-4ed2-b1f0-b993e49b3764', 'ext', null, 'png', '', null, '2', '2', '2017-05-09 08:33:11', '2017-05-09 08:33:11', null, null);
INSERT INTO `spyr42_revisions` VALUES ('194', null, 'fad34ac3-7f26-472d-8440-a1ccc0debc19', 'iND6HRao', '', '12', 'uploads', '3', '1930ebcd-f816-4bfa-a6c2-b8a8fe1698d9', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:11', '2017-05-09 08:33:11', null, null);
INSERT INTO `spyr42_revisions` VALUES ('195', null, '293d6e21-f6f2-4de8-a744-0e0fdd5a7e8d', 'M7YiGKaG', '', '12', 'uploads', '4', '6dd981b7-1c78-420d-89a7-562b842ee778', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:12', '2017-05-09 08:33:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('196', null, '57743636-162d-49b6-a3b1-9576ee80479f', 'qm4JDfLH', '', '12', 'uploads', '5', 'cf55038b-0f47-4787-a039-1ce35f1f6bd9', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:12', '2017-05-09 08:33:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('197', null, '00696340-682f-4f97-bf67-97bb01d21147', '8YXIjXE3', '', '12', 'uploads', '6', '5a740f73-4a63-4e75-8a18-094ae373e066', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:33:12', '2017-05-09 08:33:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('198', null, '5ea5e174-bf8d-4ce7-bddd-6fbc6c35f532', 'YoUSthps', '', '12', 'uploads', '7', '01f8423d-a501-4f25-9682-a9566f7c7cb7', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:12', '2017-05-09 08:33:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('199', null, '0175060f-50bc-4fd3-a75c-05e53e874d66', 'x0Yq9Hpl', '', '12', 'uploads', '8', 'af05bb54-caf4-4a70-8ee7-860749d00c42', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:12', '2017-05-09 08:33:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('200', null, '2326df2b-03f1-409a-8569-dbeae9bbeaac', 'qK6EanNH', '', '12', 'uploads', '9', '3f021bdf-0240-4b3e-bba4-ec04d1183824', 'ext', null, 'xls', '', null, '1', '1', '2017-05-09 08:33:12', '2017-05-09 08:33:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('201', null, '6433d09a-410b-4d08-998f-b76fe929000f', 'JcIdZLOp', '', '12', 'uploads', '10', '551682e7-fc19-4e8a-9ee7-8a7b086e3b3e', 'ext', null, 'pdf', '', null, '1', '1', '2017-05-09 08:33:12', '2017-05-09 08:33:12', null, null);
INSERT INTO `spyr42_revisions` VALUES ('202', null, 'd2209e1a-dbb3-47d1-bb71-2ce930769c28', 'LyxsZzWZ', '', '12', 'uploads', '11', 'f32c3cae-b11a-495c-8905-5fb8ae1b0a8d', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:13', '2017-05-09 08:33:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('203', null, 'a7228ae2-b906-4fd2-9949-fcdb7a6fd635', 'TI5vgW0n', '', '12', 'uploads', '12', 'ce186425-1b4f-4cc9-b5d3-fc25dfaa4efe', 'ext', null, 'jpg', '', null, '1', '1', '2017-05-09 08:33:13', '2017-05-09 08:33:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('204', null, '06078381-1b52-43fb-8a48-c14ea07ad69d', 'vledB3oq', '', '12', 'uploads', '13', '2c848ee8-b8bf-4ddf-86c6-cb67ee619479', 'ext', null, 'docx', '', null, '1', '1', '2017-05-09 08:33:13', '2017-05-09 08:33:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('205', null, 'a742c5fc-5c07-4981-8b65-46d66abbdc30', 'VgmSNR4T', '', '12', 'uploads', '14', '0ce6ff2a-6251-4fc5-98da-47c4fb277f44', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:13', '2017-05-09 08:33:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('206', null, '7819b6ae-58c5-4820-8283-0441fb800e68', 'OznLdi2r', '', '12', 'uploads', '15', '22db8a51-ac37-47f8-98f6-9aa26afbb626', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:13', '2017-05-09 08:33:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('207', null, '39e280ed-126a-417a-92c3-0fd7f773cc8e', 't0lYAR3X', '', '12', 'uploads', '16', '6015e283-3d22-4312-97fd-6d97b30dac05', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:13', '2017-05-09 08:33:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('208', null, 'a7efe5a4-1d00-45df-bceb-547801d28704', 'epgmX83h', '', '12', 'uploads', '17', 'c95a020e-79a3-4113-9747-e3f3e32498ef', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:13', '2017-05-09 08:33:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('209', null, 'a7c6d3cf-3bcd-4811-9651-5878b99b85f6', 'L6aSuo7n', '', '12', 'uploads', '18', '7066a7c2-f002-40a8-990e-2c73c691e1f4', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:13', '2017-05-09 08:33:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('210', null, '880d13be-4534-4e78-a54d-bfd424c19f20', 'Wyyvgqez', '', '12', 'uploads', '19', '93e92624-222b-4c59-8221-77130a3d3f97', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:14', '2017-05-09 08:33:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('211', null, 'fa507710-401b-40ca-87cd-b6685d08854d', 'gNDp5UtM', '', '12', 'uploads', '20', '2a579465-7b16-4a74-a9f8-725fd38c22ed', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:14', '2017-05-09 08:33:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('212', null, 'cda29963-477e-4f18-b896-d074962878b0', '2TN7Ol8f', '', '12', 'uploads', '21', 'f5052f64-869c-4e0d-8ce2-8fa04e423c58', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:14', '2017-05-09 08:33:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('213', null, '76f6f50e-0f7e-4e2e-afdb-36d33912a4e4', 'YZ6PTZnY', '', '12', 'uploads', '22', '3658fd83-92a4-4cd6-a539-8879a2cb9cde', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:14', '2017-05-09 08:33:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('214', null, 'd1d46a43-b6c6-4cb7-a8d1-299e556d61f9', 'ctDIHZoE', '', '12', 'uploads', '23', 'c7f61745-7cfd-4090-bf60-f107cb5a691e', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:14', '2017-05-09 08:33:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('215', null, '4e5aab48-9b29-44fa-8ea8-b132f10725b4', 'LyEzG9r0', '', '12', 'uploads', '24', '7eb660ab-54db-435e-b080-bfc97bb8394c', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:14', '2017-05-09 08:33:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('216', null, '3c7ceb42-b698-4fd0-a681-5e7ab172738a', 'F0VFyRba', '', '12', 'uploads', '25', '8762f1bf-4cd6-403e-bb77-90aa79f5d3a8', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:15', '2017-05-09 08:33:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('217', null, '6c3363bd-b53a-4ff1-bf55-a47b057c8698', 'j9WQpUDz', '', '12', 'uploads', '26', 'cf8cf28e-81c7-453f-aced-d1e5840b9a44', 'ext', null, 'png', '', null, '1', '1', '2017-05-09 08:33:15', '2017-05-09 08:33:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('218', null, 'eee2ab26-4734-4273-a1c5-5daa8c0efd60', 'WWDFKTV5', '', '12', 'uploads', '27', '4543ab72-68a5-4284-8095-cd0ab27dd6eb', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:33:15', '2017-05-09 08:33:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('219', null, '4f91ec6f-5aba-4e7a-bedc-e5e3a1d91eb9', 'VzH4hF6R', '', '12', 'uploads', '28', 'be78984b-2731-4be6-908e-3b94d05f86a0', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:33:15', '2017-05-09 08:33:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('220', null, 'fbe711e4-8709-4c9e-9ee4-2f409114beac', 'GanoRzap', '', '12', 'uploads', '29', '85ed30aa-a314-4298-85ca-3cc83803a63e', 'ext', null, 'png', '', null, '38', '38', '2017-05-09 08:33:15', '2017-05-09 08:33:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('221', null, '49f9e6b5-78eb-4094-b0bb-04a5b531b186', 'Q4InrJps', '', '12', 'uploads', '30', '042b9fe9-4512-4168-89de-dad75e7d442f', 'ext', null, 'jpg', '', null, '38', '38', '2017-05-09 08:33:15', '2017-05-09 08:33:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('222', null, '559e8c2c-8411-4bb8-8b7a-231bba0842a3', 'ysTlYMM4', '', '12', 'uploads', '31', 'd0bf3951-f3ea-4e22-8b4b-d9823c77cc68', 'ext', null, 'jpg', '', null, '38', '38', '2017-05-09 08:33:15', '2017-05-09 08:33:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('223', null, 'b7930cf3-e98e-42ef-a641-4ce57f6083ab', 'dCEIoJIR', '', '1', 'modules', '14', 'c3c26079-146a-4694-b458-514a73581667', 'desc', 'asdfasdf', 'testmodulesH9E9NUBN', '', null, '1', '1', '2017-05-09 08:36:39', '2017-05-09 08:36:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('224', null, '2d74a256-b34d-4347-88c3-dae787e33dea', 'TLt8bL4y', '', '12', 'uploads', '31', 'd0bf3951-f3ea-4e22-8b4b-d9823c77cc68', 'desc', '', 'lorel ipsum dolor sit amet', '', 'Yes', '38', '38', '2017-05-09 10:56:18', '2017-05-09 10:56:18', null, null);
INSERT INTO `spyr42_revisions` VALUES ('225', null, '1b60a427-b4b0-42ee-97fe-0163b556b5bb', 'wx8KAemy', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-09 06:57:50', '2017-05-16 05:21:44', '', 'Yes', '1', '1', '2017-05-16 05:21:45', '2017-05-16 05:21:45', null, null);
INSERT INTO `spyr42_revisions` VALUES ('226', '18', 'fafb733e-f859-4ba7-b351-e2514e75d1ff', '8aAOcUdY', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-09 06:03:57', '2017-05-16 05:21:57', '', 'Yes', '1', '1', '2017-05-16 05:21:57', '2017-05-16 05:21:57', null, null);
INSERT INTO `spyr42_revisions` VALUES ('227', '18', 'b636a13b-cf82-4018-a076-aecec10fb290', 'an61vhbn', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-16 05:21:57', '2017-05-18 05:17:57', '', 'Yes', '1', '1', '2017-05-18 05:17:57', '2017-05-18 05:17:57', null, null);
INSERT INTO `spyr42_revisions` VALUES ('228', null, '33ce40d3-da42-4201-a2cc-5394aa091769', 'N0anOht2', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-16 05:21:44', '2017-05-18 05:17:59', '', 'Yes', '1', '1', '2017-05-18 05:17:59', '2017-05-18 05:17:59', null, null);
INSERT INTO `spyr42_revisions` VALUES ('229', null, '81b54f53-20fd-4e98-b64b-b718eba766e6', 'PTTcx7hz', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-18 05:17:59', '2017-05-23 09:23:19', '', 'Yes', '1', '1', '2017-05-23 09:23:19', '2017-05-23 09:23:19', null, null);
INSERT INTO `spyr42_revisions` VALUES ('230', '18', 'f9072a20-d8ca-4dea-8559-f407523f7dc9', 'sfvmIj5B', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-18 05:17:57', '2017-05-23 09:23:26', '', 'Yes', '1', '1', '2017-05-23 09:23:26', '2017-05-23 09:23:26', null, null);
INSERT INTO `spyr42_revisions` VALUES ('231', null, '99ff9091-7cb2-462f-a238-ea1a64be0bb0', '8c2o5HpF', '', '11', 'gsettings', '1', 'c07553e1-8d3a-47b0-85b1-89c1ab02c541', 'value', '<b>SPYR</b>FRAME', 'SPYRFRAME', '', 'Yes', '1', '1', '2017-05-23 09:25:26', '2017-05-23 09:25:26', null, null);
INSERT INTO `spyr42_revisions` VALUES ('232', null, '8b4e10bc-6015-4b51-8c03-d1e4954c0ef4', '8c2o5HpF', '', '11', 'gsettings', '1', 'c07553e1-8d3a-47b0-85b1-89c1ab02c541', 'allow_tenant_override', null, 'No', '', 'Yes', '1', '1', '2017-05-23 09:25:26', '2017-05-23 09:25:26', null, null);
INSERT INTO `spyr42_revisions` VALUES ('233', null, '7a4a8cc2-86f2-4fe2-b5f2-dabbf8c439ba', 'JeTBKNZJ', '', '11', 'gsettings', '2', '0d09deec-ccd2-41de-8c1e-facb41516106', 'type', 'array', 'te', '', 'Yes', '1', '1', '2017-05-23 09:57:23', '2017-05-23 09:57:23', null, null);
INSERT INTO `spyr42_revisions` VALUES ('234', null, '92c67628-d841-4207-9bad-b3c7d6f8305e', 'ErXntmPo', '', '11', 'gsettings', '2', '0d09deec-ccd2-41de-8c1e-facb41516106', 'type', 'te', 'array', '', 'Yes', '1', '1', '2017-05-23 09:58:04', '2017-05-23 09:58:04', null, null);
INSERT INTO `spyr42_revisions` VALUES ('235', null, 'ad485f06-6c04-4a7a-b656-bf5e148aa1e4', 'QbNQmN64', '', '11', 'gsettings', '2', '0d09deec-ccd2-41de-8c1e-facb41516106', 'is_active', 'Yes', 'No', '', 'Yes', '1', '1', '2017-05-23 10:18:45', '2017-05-23 10:18:45', null, null);
INSERT INTO `spyr42_revisions` VALUES ('236', null, 'ec7dd528-cbdb-4753-b043-98707e4495f1', 'n6eZfUGp', '', '11', 'gsettings', '2', '0d09deec-ccd2-41de-8c1e-facb41516106', 'is_active', 'No', 'Yes', '', 'Yes', '1', '1', '2017-05-23 10:18:49', '2017-05-23 10:18:49', null, null);
INSERT INTO `spyr42_revisions` VALUES ('237', '18', 'cb1a5e25-122b-46b3-9ba0-2ad341aab2a1', 'fqf1Xupq', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-23 09:23:26', '2017-05-24 06:31:19', '', 'Yes', '1', '1', '2017-05-24 06:31:19', '2017-05-24 06:31:19', null, null);
INSERT INTO `spyr42_revisions` VALUES ('238', null, '10671f14-2d26-42c8-8641-306cccd0377a', 'MM68bcx7', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-05-23 09:23:19', '2017-06-05 06:44:29', '', 'Yes', '1', '1', '2017-06-05 06:44:29', '2017-06-05 06:44:29', null, null);
INSERT INTO `spyr42_revisions` VALUES ('239', '18', '16ddb5ac-a4d7-41f9-850f-57688e30f8ef', 'bBF5nTtd', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-05-24 06:31:19', '2017-06-05 06:44:55', '', 'Yes', '1', '1', '2017-06-05 06:44:55', '2017-06-05 06:44:55', null, null);
INSERT INTO `spyr42_revisions` VALUES ('240', '18', 'ad676a2f-cb17-4568-99d2-8d61d635bce6', 'YHCYP2e9', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-05 06:44:55', '2017-06-06 09:55:43', '', 'Yes', '1', '1', '2017-06-06 09:55:43', '2017-06-06 09:55:43', null, null);
INSERT INTO `spyr42_revisions` VALUES ('241', '18', 'ac4c69f8-24ef-4be4-8f07-8eed22bf26ee', 'zOI0SfHk', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 09:55:43', '2017-06-06 09:58:53', '', 'Yes', '1', '1', '2017-06-06 09:58:53', '2017-06-06 09:58:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('242', '18', 'ea0b7d3c-b2d1-45ae-81d0-b0fc8c5cf5ac', 'bV1aiO3S', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 09:58:53', '2017-06-06 10:13:56', '', 'Yes', '1', '1', '2017-06-06 10:13:56', '2017-06-06 10:13:56', null, null);
INSERT INTO `spyr42_revisions` VALUES ('243', '18', '2e39b691-f374-4793-a815-b7916899d996', 'bphRk0DM', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:13:56', '2017-06-06 10:14:10', '', 'Yes', '1', '1', '2017-06-06 10:14:10', '2017-06-06 10:14:10', null, null);
INSERT INTO `spyr42_revisions` VALUES ('244', '18', '30919331-f1ac-4521-817d-298f3b7db5a1', 'MELa4jC1', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:14:10', '2017-06-06 10:14:38', '', 'Yes', '1', '1', '2017-06-06 10:14:38', '2017-06-06 10:14:38', null, null);
INSERT INTO `spyr42_revisions` VALUES ('245', '18', '9ebe71a7-85ad-4bbe-b31b-a7df93caae69', 'zB5bmeW6', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:14:38', '2017-06-06 10:15:31', '', 'Yes', '1', '1', '2017-06-06 10:15:31', '2017-06-06 10:15:31', null, null);
INSERT INTO `spyr42_revisions` VALUES ('246', '18', '4cc3ffb6-b6c8-4331-a415-1fc9be848528', 'oAGsttSu', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:15:31', '2017-06-06 10:15:47', '', 'Yes', '1', '1', '2017-06-06 10:15:47', '2017-06-06 10:15:47', null, null);
INSERT INTO `spyr42_revisions` VALUES ('247', '18', '4c1755e3-0f4f-445b-879c-49e7bdeb40eb', 'TFRJfXz6', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:15:47', '2017-06-06 10:16:13', '', 'Yes', '1', '1', '2017-06-06 10:16:13', '2017-06-06 10:16:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('248', '18', 'd79566b4-431a-460c-87d2-27c3b383d405', 'dIZLC6As', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:16:13', '2017-06-06 10:17:14', '', 'Yes', '1', '1', '2017-06-06 10:17:14', '2017-06-06 10:17:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('249', '18', 'b2523643-22a0-4112-9acd-00c4f7f760ec', 'D5YVugbs', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:17:14', '2017-06-06 10:17:34', '', 'Yes', '1', '1', '2017-06-06 10:17:34', '2017-06-06 10:17:34', null, null);
INSERT INTO `spyr42_revisions` VALUES ('250', '18', '8b84bc5a-41aa-405f-802a-c6a6f324c853', 'qsV5QhXs', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:17:34', '2017-06-06 10:17:39', '', 'Yes', '1', '1', '2017-06-06 10:17:39', '2017-06-06 10:17:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('251', '18', '966d2f06-5a1a-43d9-ac73-ad67a05b96ee', 'bokgMHZD', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:17:39', '2017-06-06 10:17:44', '', 'Yes', '1', '1', '2017-06-06 10:17:44', '2017-06-06 10:17:44', null, null);
INSERT INTO `spyr42_revisions` VALUES ('252', '18', 'b4b631b5-2773-4d26-981e-7559653dae4e', 'OwPYLWTY', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:17:44', '2017-06-06 10:41:02', '', 'Yes', '1', '1', '2017-06-06 10:41:02', '2017-06-06 10:41:02', null, null);
INSERT INTO `spyr42_revisions` VALUES ('253', '18', '22ff6124-30ae-4f3d-aedc-49f80634ce47', 'Gu5BQmhs', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:41:02', '2017-06-06 10:41:40', '', 'Yes', '1', '1', '2017-06-06 10:41:40', '2017-06-06 10:41:40', null, null);
INSERT INTO `spyr42_revisions` VALUES ('254', '18', '15eda6a5-3953-4abb-9530-d191d290b4fe', 'BJEmoZpO', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:41:40', '2017-06-06 10:41:55', '', 'Yes', '1', '1', '2017-06-06 10:41:55', '2017-06-06 10:41:55', null, null);
INSERT INTO `spyr42_revisions` VALUES ('255', '18', '1281401f-1737-4509-931e-5bf153c439fb', 'm2wFf2zn', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:41:55', '2017-06-06 10:45:14', '', 'Yes', '1', '1', '2017-06-06 10:45:14', '2017-06-06 10:45:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('256', '18', '8db06441-b343-4c28-a1a6-33b4284dfdbd', 'FvGtUioz', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:45:14', '2017-06-06 10:49:13', '', 'Yes', '1', '1', '2017-06-06 10:49:13', '2017-06-06 10:49:13', null, null);
INSERT INTO `spyr42_revisions` VALUES ('257', '18', '10681ad6-60c8-4b9c-b589-6d8c6b656074', 'iAGtVXwp', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:49:13', '2017-06-06 10:50:11', '', 'Yes', '1', '1', '2017-06-06 10:50:11', '2017-06-06 10:50:11', null, null);
INSERT INTO `spyr42_revisions` VALUES ('258', '18', 'c823a116-dbb3-47de-a5c8-55f745905636', 'nc6IhbAB', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:50:11', '2017-06-06 10:50:23', '', 'Yes', '1', '1', '2017-06-06 10:50:23', '2017-06-06 10:50:23', null, null);
INSERT INTO `spyr42_revisions` VALUES ('259', '18', '64430b41-8543-4dc4-80e4-b1126547a130', 'thqz7amn', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:50:23', '2017-06-06 10:50:57', '', 'Yes', '1', '1', '2017-06-06 10:50:57', '2017-06-06 10:50:57', null, null);
INSERT INTO `spyr42_revisions` VALUES ('260', '18', '41079eb3-bad7-42c4-b81b-0003fef1d89e', 'wKJmmiYm', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:50:57', '2017-06-06 10:51:18', '', 'Yes', '1', '1', '2017-06-06 10:51:18', '2017-06-06 10:51:18', null, null);
INSERT INTO `spyr42_revisions` VALUES ('261', '18', 'e7b38496-3939-44ad-bbee-9411ace6d70a', 'cTwdST3n', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:51:18', '2017-06-06 10:51:37', '', 'Yes', '1', '1', '2017-06-06 10:51:37', '2017-06-06 10:51:37', null, null);
INSERT INTO `spyr42_revisions` VALUES ('262', '18', 'f2813793-9513-41f9-9155-d766076d1e82', 'odrakLtS', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:51:37', '2017-06-06 10:52:45', '', 'Yes', '1', '1', '2017-06-06 10:52:45', '2017-06-06 10:52:45', null, null);
INSERT INTO `spyr42_revisions` VALUES ('263', '18', '9b803cdd-a685-4cea-9bdf-59e242ebc5a4', 'Dw17cAFF', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:52:45', '2017-06-06 10:53:15', '', 'Yes', '1', '1', '2017-06-06 10:53:15', '2017-06-06 10:53:15', null, null);
INSERT INTO `spyr42_revisions` VALUES ('264', '18', '5259fbe6-658d-4cbf-a5f7-4a684e1acbe2', 'x1g30Zqs', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:53:15', '2017-06-06 10:53:40', '', 'Yes', '1', '1', '2017-06-06 10:53:40', '2017-06-06 10:53:40', null, null);
INSERT INTO `spyr42_revisions` VALUES ('265', '18', '8db0051e-c725-4c75-9e19-cf67cc6ea229', 'gsffHWSZ', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:53:40', '2017-06-06 10:54:02', '', 'Yes', '1', '1', '2017-06-06 10:54:02', '2017-06-06 10:54:02', null, null);
INSERT INTO `spyr42_revisions` VALUES ('266', '18', '6f8d23a8-bb67-4419-8fd5-1c0c48dd54c1', 'yF2OvSw8', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:54:02', '2017-06-06 10:54:20', '', 'Yes', '1', '1', '2017-06-06 10:54:20', '2017-06-06 10:54:20', null, null);
INSERT INTO `spyr42_revisions` VALUES ('267', '18', 'b96c6a33-3664-400d-80bf-866f31276021', 'ewxa6Owu', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:54:20', '2017-06-06 10:55:18', '', 'Yes', '1', '1', '2017-06-06 10:55:18', '2017-06-06 10:55:18', null, null);
INSERT INTO `spyr42_revisions` VALUES ('268', '18', '2c9db716-b2f3-455f-8fd0-ea87efbc5a81', '7Uv62tkE', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:55:18', '2017-06-06 10:55:43', '', 'Yes', '1', '1', '2017-06-06 10:55:43', '2017-06-06 10:55:43', null, null);
INSERT INTO `spyr42_revisions` VALUES ('269', '18', '88b0970c-2002-4bdb-abfd-682be03e9c46', 'VkZ1a6Cr', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:55:43', '2017-06-06 10:55:58', '', 'Yes', '1', '1', '2017-06-06 10:55:58', '2017-06-06 10:55:58', null, null);
INSERT INTO `spyr42_revisions` VALUES ('270', '18', '85ff7f94-b0a6-4bca-a2c9-b8cf39c8f82c', 'C5gr9LwD', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:55:58', '2017-06-06 10:56:34', '', 'Yes', '1', '1', '2017-06-06 10:56:34', '2017-06-06 10:56:34', null, null);
INSERT INTO `spyr42_revisions` VALUES ('271', '18', '96c0d9e7-2bbe-4939-91a8-b666a5dc1a29', 'DnBXhxOj', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:56:34', '2017-06-06 10:56:48', '', 'Yes', '1', '1', '2017-06-06 10:56:48', '2017-06-06 10:56:48', null, null);
INSERT INTO `spyr42_revisions` VALUES ('272', '18', 'e4db757a-23b5-47ba-9ffe-d779032284ea', 'v9zMIoxA', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:56:48', '2017-06-06 10:57:18', '', 'Yes', '1', '1', '2017-06-06 10:57:18', '2017-06-06 10:57:18', null, null);
INSERT INTO `spyr42_revisions` VALUES ('273', '18', '8fbc9c61-882b-4678-ab88-eb43e94aadf7', 'mDpx6Ggd', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:57:18', '2017-06-06 10:57:32', '', 'Yes', '1', '1', '2017-06-06 10:57:32', '2017-06-06 10:57:32', null, null);
INSERT INTO `spyr42_revisions` VALUES ('274', '18', '3e365695-8f53-4ee6-99b9-390626219e3d', 'LiGwl3Jq', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:57:32', '2017-06-06 10:58:37', '', 'Yes', '1', '1', '2017-06-06 10:58:37', '2017-06-06 10:58:37', null, null);
INSERT INTO `spyr42_revisions` VALUES ('275', '18', 'a739bc7a-2dca-454b-8cfe-b51b9f1951a8', 't0mnfT0f', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:58:37', '2017-06-06 10:59:03', '', 'Yes', '1', '1', '2017-06-06 10:59:03', '2017-06-06 10:59:03', null, null);
INSERT INTO `spyr42_revisions` VALUES ('276', '18', '7e1cb937-d6ae-4ebf-9cec-2143eb59376c', 'NIzNjv2N', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:59:03', '2017-06-06 10:59:29', '', 'Yes', '1', '1', '2017-06-06 10:59:29', '2017-06-06 10:59:29', null, null);
INSERT INTO `spyr42_revisions` VALUES ('277', '18', '092b8536-8368-4730-8a52-d76d8a5dc0ee', '20Xu2iUD', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:59:29', '2017-06-06 10:59:54', '', 'Yes', '1', '1', '2017-06-06 10:59:54', '2017-06-06 10:59:54', null, null);
INSERT INTO `spyr42_revisions` VALUES ('278', '18', 'd581f2fb-dd83-4152-8fac-0c311bbee565', 'xhld2BRj', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 10:59:54', '2017-06-06 11:00:14', '', 'Yes', '1', '1', '2017-06-06 11:00:14', '2017-06-06 11:00:14', null, null);
INSERT INTO `spyr42_revisions` VALUES ('279', '18', '6e2e9328-0708-47a7-ae53-950df25fab64', 'cL6mXd5S', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:00:14', '2017-06-06 11:00:24', '', 'Yes', '1', '1', '2017-06-06 11:00:24', '2017-06-06 11:00:24', null, null);
INSERT INTO `spyr42_revisions` VALUES ('280', '18', '9ce6f606-5fb3-4f25-b0c8-7bfd69d5b1ba', 'PN5aKizW', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:00:24', '2017-06-06 11:00:39', '', 'Yes', '1', '1', '2017-06-06 11:00:39', '2017-06-06 11:00:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('281', '18', '17077892-a570-4429-9236-bad829245b40', 'NmXXGhoa', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:00:39', '2017-06-06 11:00:56', '', 'Yes', '1', '1', '2017-06-06 11:00:56', '2017-06-06 11:00:56', null, null);
INSERT INTO `spyr42_revisions` VALUES ('282', '18', 'b2548188-a32f-4ad4-8eef-d12ff72314fd', 'OQWkRBsv', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:00:56', '2017-06-06 11:01:04', '', 'Yes', '1', '1', '2017-06-06 11:01:04', '2017-06-06 11:01:04', null, null);
INSERT INTO `spyr42_revisions` VALUES ('283', '18', 'a8b3497c-010a-4f52-83a5-4c1819f42b41', '8zKAkINt', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:01:04', '2017-06-06 11:01:35', '', 'Yes', '1', '1', '2017-06-06 11:01:35', '2017-06-06 11:01:35', null, null);
INSERT INTO `spyr42_revisions` VALUES ('284', '18', 'a44bf687-c8db-4135-9955-aa903b95645d', 'UaQ7TiJu', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:01:35', '2017-06-06 11:08:36', '', 'Yes', '1', '1', '2017-06-06 11:08:36', '2017-06-06 11:08:36', null, null);
INSERT INTO `spyr42_revisions` VALUES ('285', '18', '8089f425-9374-45b5-a008-81f7c4dfb34f', 'I7lcGnMR', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:08:36', '2017-06-06 11:08:56', '', 'Yes', '1', '1', '2017-06-06 11:08:56', '2017-06-06 11:08:56', null, null);
INSERT INTO `spyr42_revisions` VALUES ('286', '18', '1ad0e166-dad0-451a-91ee-e4279e59e58a', 'kyFxOZTc', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:08:56', '2017-06-06 11:09:16', '', 'Yes', '1', '1', '2017-06-06 11:09:16', '2017-06-06 11:09:16', null, null);
INSERT INTO `spyr42_revisions` VALUES ('287', '18', '0b1ad483-b8a3-4fc5-9829-6d9b2afd57f3', 'LC23dkT5', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:09:16', '2017-06-06 11:09:33', '', 'Yes', '1', '1', '2017-06-06 11:09:33', '2017-06-06 11:09:33', null, null);
INSERT INTO `spyr42_revisions` VALUES ('288', '18', '3763bdef-6ff5-49a9-ac2c-0fcf01bc810f', '5ADthSov', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:09:33', '2017-06-06 11:09:53', '', 'Yes', '1', '1', '2017-06-06 11:09:53', '2017-06-06 11:09:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('289', '18', '3651a330-751a-4974-9c82-eb3b1f148bee', 's1SBgC5G', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:09:53', '2017-06-06 11:10:04', '', 'Yes', '1', '1', '2017-06-06 11:10:04', '2017-06-06 11:10:04', null, null);
INSERT INTO `spyr42_revisions` VALUES ('290', '18', '2f104d95-3b76-4143-8066-56ef1a95582c', 'Wr6tIabz', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:10:04', '2017-06-06 11:10:39', '', 'Yes', '1', '1', '2017-06-06 11:10:39', '2017-06-06 11:10:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('291', '18', 'bec89c64-33c4-4636-8c7e-c909b9655134', 'KJKW8vfm', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:10:39', '2017-06-06 11:10:59', '', 'Yes', '1', '1', '2017-06-06 11:10:59', '2017-06-06 11:10:59', null, null);
INSERT INTO `spyr42_revisions` VALUES ('292', '18', '3ca11dbb-9799-45b0-8cc7-f2fca5c0e23a', 'JhQ4Chv9', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:10:59', '2017-06-06 11:12:21', '', 'Yes', '1', '1', '2017-06-06 11:12:21', '2017-06-06 11:12:21', null, null);
INSERT INTO `spyr42_revisions` VALUES ('293', '18', '7aa57d43-35e2-48f6-8211-f0b1db9018c8', 'BHy5UOuj', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:12:21', '2017-06-06 11:12:58', '', 'Yes', '1', '1', '2017-06-06 11:12:58', '2017-06-06 11:12:58', null, null);
INSERT INTO `spyr42_revisions` VALUES ('294', '18', '93f077b8-5168-45d8-9a3d-e9fd1ac762c8', 'c7NXK3Su', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:12:58', '2017-06-06 11:13:53', '', 'Yes', '1', '1', '2017-06-06 11:13:53', '2017-06-06 11:13:53', null, null);
INSERT INTO `spyr42_revisions` VALUES ('295', '18', '4715dc9d-623c-4c56-9861-bb44f417838a', '3tPGDLig', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:13:53', '2017-06-06 11:14:27', '', 'Yes', '1', '1', '2017-06-06 11:14:27', '2017-06-06 11:14:27', null, null);
INSERT INTO `spyr42_revisions` VALUES ('296', '18', '5f97e151-cb72-4e76-b8dd-1aaea93b8252', 'R3jQ7HWO', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:14:27', '2017-06-06 11:14:35', '', 'Yes', '1', '1', '2017-06-06 11:14:35', '2017-06-06 11:14:35', null, null);
INSERT INTO `spyr42_revisions` VALUES ('297', '18', 'f7871b7e-2ff5-493c-bc6b-98e7ca20819b', 'irLbRVnK', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:14:35', '2017-06-06 11:16:08', '', 'Yes', '1', '1', '2017-06-06 11:16:08', '2017-06-06 11:16:08', null, null);
INSERT INTO `spyr42_revisions` VALUES ('298', '18', '0140bdb0-9293-46ec-b670-946dd7999e89', 'ouhMFUh8', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:16:08', '2017-06-06 11:19:32', '', 'Yes', '1', '1', '2017-06-06 11:19:32', '2017-06-06 11:19:32', null, null);
INSERT INTO `spyr42_revisions` VALUES ('299', '18', '70d65790-948c-4cf7-8e39-f3a4c5040e64', 'fYyL1bYx', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:19:32', '2017-06-06 11:20:02', '', 'Yes', '1', '1', '2017-06-06 11:20:02', '2017-06-06 11:20:02', null, null);
INSERT INTO `spyr42_revisions` VALUES ('300', '18', 'a07d2559-76fc-428f-a260-d19ab32a284b', 'QVR9yThT', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:20:02', '2017-06-06 11:20:26', '', 'Yes', '1', '1', '2017-06-06 11:20:26', '2017-06-06 11:20:26', null, null);
INSERT INTO `spyr42_revisions` VALUES ('301', '18', '6af5c5fd-a9c2-4812-bec7-341bed3e4889', 'RuGpfLIP', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:20:26', '2017-06-06 11:20:39', '', 'Yes', '1', '1', '2017-06-06 11:20:39', '2017-06-06 11:20:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('302', '18', '9bfe6069-0233-44cb-a743-ac978bcc3fdd', 'ZsFVSBcj', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:20:39', '2017-06-06 11:21:31', '', 'Yes', '1', '1', '2017-06-06 11:21:31', '2017-06-06 11:21:31', null, null);
INSERT INTO `spyr42_revisions` VALUES ('303', '18', 'da3ecdee-75b0-4a31-b07b-aa52bc5b09a4', 'IUTjuETK', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:21:31', '2017-06-06 11:21:46', '', 'Yes', '1', '1', '2017-06-06 11:21:46', '2017-06-06 11:21:46', null, null);
INSERT INTO `spyr42_revisions` VALUES ('304', '18', '7385919e-fd70-469c-afc9-8f8284431bd4', '4exWt4k6', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:21:46', '2017-06-06 11:22:23', '', 'Yes', '1', '1', '2017-06-06 11:22:23', '2017-06-06 11:22:23', null, null);
INSERT INTO `spyr42_revisions` VALUES ('305', '18', '117e7c3b-0d8f-4000-9b1f-7cdb46eb29f7', 'FNlK2LmJ', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:22:23', '2017-06-06 11:22:38', '', 'Yes', '1', '1', '2017-06-06 11:22:38', '2017-06-06 11:22:38', null, null);
INSERT INTO `spyr42_revisions` VALUES ('306', '18', 'bbcc2cef-e553-407e-bbc6-966dfa04f8c7', 'g3rOkHdc', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:22:38', '2017-06-06 11:22:56', '', 'Yes', '1', '1', '2017-06-06 11:22:56', '2017-06-06 11:22:56', null, null);
INSERT INTO `spyr42_revisions` VALUES ('307', '18', 'b36105fd-87d4-450f-9633-d0ffeab6d7cb', 'mjbSgx6I', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:22:56', '2017-06-06 11:23:30', '', 'Yes', '1', '1', '2017-06-06 11:23:31', '2017-06-06 11:23:31', null, null);
INSERT INTO `spyr42_revisions` VALUES ('308', '18', '202cb65d-7d57-494d-9cf8-0a222bdfdd14', 'wmkqBjw6', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:23:30', '2017-06-06 11:24:57', '', 'Yes', '1', '1', '2017-06-06 11:24:57', '2017-06-06 11:24:57', null, null);
INSERT INTO `spyr42_revisions` VALUES ('309', '18', '58505f97-bf08-4c7f-a1d4-00752002d936', 'Wairf68D', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-06 11:24:57', '2017-06-07 06:09:47', '', 'Yes', '1', '1', '2017-06-07 06:09:47', '2017-06-07 06:09:47', null, null);
INSERT INTO `spyr42_revisions` VALUES ('310', '18', '84f549f5-fb89-402f-84da-9a3d1865da6f', 'd5wPuabo', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-07 06:09:47', '2017-06-07 07:02:19', '', 'Yes', '1', '1', '2017-06-07 07:02:19', '2017-06-07 07:02:19', null, null);
INSERT INTO `spyr42_revisions` VALUES ('311', null, '25300148-682c-44ed-ac4d-4be6c4ea9989', 'dYIJzyEl', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-06-05 06:44:29', '2017-06-07 08:18:01', '', 'Yes', '1', '1', '2017-06-07 08:18:01', '2017-06-07 08:18:01', null, null);
INSERT INTO `spyr42_revisions` VALUES ('312', null, 'b27b4ee4-1fea-45ad-be14-9ba99c658df1', '0r6XZ28u', '', '1', 'modules', '14', 'c3c26079-146a-4694-b458-514a73581667', 'desc', 'testmodulesH9E9NUBN', 'testmodulesS8GRMT0R', '', 'Yes', '1', '1', '2017-06-07 10:34:28', '2017-06-07 10:34:28', null, null);
INSERT INTO `spyr42_revisions` VALUES ('313', '18', '42e6dcc9-13ca-4e18-a932-bb0f44397625', '3Qp5wgiF', '', '2', 'users', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'last_login', '2017-06-07 07:02:19', '2017-06-11 11:29:39', '', 'Yes', '1', '1', '2017-06-11 11:29:39', '2017-06-11 11:29:39', null, null);
INSERT INTO `spyr42_revisions` VALUES ('314', null, 'd00502fc-7b9c-4040-a64b-cd065bc32707', '2jaCD2XV', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-06-07 08:18:01', '2017-07-12 05:55:37', '', 'Yes', '1', '1', '2017-07-12 05:55:37', '2017-07-12 05:55:37', null, null);
INSERT INTO `spyr42_revisions` VALUES ('315', null, '9fd26a64-2cfb-4d51-b21c-4023c39dcbc9', '2MolXg9p', '', '2', 'users', '1', '8311d707-9dd3-4b38-aebd-e3675f079805', 'last_login', '2017-07-12 05:55:37', '2017-07-13 09:22:27', '', 'Yes', '1', '1', '2017-07-13 09:22:27', '2017-07-13 09:22:27', null, null);

-- ----------------------------
-- Table structure for spyr42_tenants
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_tenants`;
CREATE TABLE `spyr42_tenants` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_tenants
-- ----------------------------
INSERT INTO `spyr42_tenants` VALUES ('1', '4f3692c3-9867-459a-894a-d3349ea735a8', 'First tenant', '2', 'Yes', '1', '1', '2017-02-27 06:58:16', '2017-02-27 07:00:25', null, null);
INSERT INTO `spyr42_tenants` VALUES ('9', '99a99216-af51-4bfc-a8c7-9781e98b4ce5', 'tenant2', '22', 'Yes', '1', '1', '2017-04-30 06:50:25', '2017-04-30 06:50:25', null, null);
INSERT INTO `spyr42_tenants` VALUES ('10', 'e537675a-524a-4277-8773-e9f0e789b3b1', 'tenant3', '24', 'Yes', '1', '1', '2017-04-30 06:52:12', '2017-04-30 06:52:12', null, null);
INSERT INTO `spyr42_tenants` VALUES ('18', 'f3a315a2-d2bf-49f0-b288-862a48710af2', 'tenant6', '38', 'Yes', '1', '1', '2017-04-30 08:41:25', '2017-04-30 08:41:25', null, null);

-- ----------------------------
-- Table structure for spyr42_throttle
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_throttle`;
CREATE TABLE `spyr42_throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_throttle
-- ----------------------------
INSERT INTO `spyr42_throttle` VALUES ('2', '1', '::1', '0', '0', '0', null, null, null);

-- ----------------------------
-- Table structure for spyr42_uploads
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_uploads`;
CREATE TABLE `spyr42_uploads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(10) unsigned DEFAULT NULL,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_id` int(10) unsigned DEFAULT NULL,
  `element_id` int(10) unsigned DEFAULT NULL,
  `element_uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_uploads
-- ----------------------------
INSERT INTO `spyr42_uploads` VALUES ('1', '1', 'b5cd6339-3915-4828-a051-0d80c842a48a', '2017-02-27 12_19_13-HRIS - MoHFW.png', '/files/1/cUfC6j_2017-02-27-12_19_13-HRIS---MoHFW.png', 'png', null, null, '2', '3', 'f7051333-2018-4cb8-a369-0e7e7c0f2c10', 'Yes', '2', '2', '2017-02-28 09:59:25', '2017-05-09 10:02:51', null, null);
INSERT INTO `spyr42_uploads` VALUES ('2', '1', 'ba662af7-a01a-4ed2-b1f0-b993e49b3764', '2017-02-27 11_23_20-Seed Dashboard.png', '/files/1/nrUCvD_2017-02-27-11_23_20-Seed-Dashboard.png', 'png', null, null, '2', '3', 'f7051333-2018-4cb8-a369-0e7e7c0f2c10', 'Yes', '2', '2', '2017-02-28 09:59:25', '2017-05-09 10:02:51', null, null);
INSERT INTO `spyr42_uploads` VALUES ('3', null, '1930ebcd-f816-4bfa-a6c2-b8a8fe1698d9', '2017-02-27 11_23_20-Seed Dashboard.png', '/files/0YAbGE_2017-02-27-11_23_20-Seed-Dashboard.png', 'png', null, null, '1', '0', '5eb2b071-057b-4127-9181-aaa683e0e946', 'Yes', '1', '1', '2017-03-01 05:40:42', '2017-05-09 10:02:51', null, null);
INSERT INTO `spyr42_uploads` VALUES ('4', null, '6dd981b7-1c78-420d-89a7-562b842ee778', '2017-02-27 11_15_50-Greenshot.png', '/files/fgikH4_2017-02-27-11_15_50-Greenshot.png', 'png', null, null, '1', '0', '5eb2b071-057b-4127-9181-aaa683e0e946', 'Yes', '1', '1', '2017-03-01 05:41:05', '2017-05-09 10:02:51', null, null);
INSERT INTO `spyr42_uploads` VALUES ('5', null, 'cf55038b-0f47-4787-a039-1ce35f1f6bd9', '2017-04-27 16_00_33-HRIS - MoHFW.png', '/files/d7Ofv4_2017-04-27-16_00_33-HRIS---MoHFW.png', 'png', null, null, '2', '39', '5b01e62a-ba2b-4c5c-b2ed-de2af59888a0', 'Yes', '1', '1', '2017-04-30 09:56:47', '2017-05-09 10:02:52', null, null);
INSERT INTO `spyr42_uploads` VALUES ('6', '18', '5a740f73-4a63-4e75-8a18-094ae373e066', '2017-04-27 16_00_33-HRIS - MoHFW.png', '/files/18/1G3IJY_2017-04-27-16_00_33-HRIS---MoHFW.png', 'png', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '38', '38', '2017-04-30 11:27:50', '2017-05-09 10:02:52', null, null);
INSERT INTO `spyr42_uploads` VALUES ('7', null, '01f8423d-a501-4f25-9682-a9566f7c7cb7', '2017-04-10 13_18_35-HRIS - MoHFW.png', '/files/JXhKAo_2017-04-10-13_18_35-HRIS---MoHFW.png', 'png', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 09:18:27', '2017-05-09 10:02:52', '2017-05-02 09:49:27', null);
INSERT INTO `spyr42_uploads` VALUES ('8', null, 'af05bb54-caf4-4a70-8ee7-860749d00c42', '2017-03-29 12_03_13-Casablanca e-Shop - Photos.png', '/files/2GpyCe_2017-03-29-12_03_13-Casablanca-e-Shop---Photos.png', 'png', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 09:19:11', '2017-05-09 10:02:52', null, null);
INSERT INTO `spyr42_uploads` VALUES ('9', null, '3f021bdf-0240-4b3e-bba4-ec04d1183824', 'accountstatment.xls', '/files/rBH9ck_accountstatment.xls', 'xls', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 09:19:56', '2017-05-09 10:02:52', null, null);
INSERT INTO `spyr42_uploads` VALUES ('10', null, '551682e7-fc19-4e8a-9ee7-8a7b086e3b3e', 'Central-HRIS-Future-plan-Implementation-strategy-1.3.pdf', '/files/fzZ9Y4_Central-HRIS-Future-plan-Implementation-strategy-1.3.pdf', 'pdf', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 09:25:13', '2017-05-09 10:02:52', null, null);
INSERT INTO `spyr42_uploads` VALUES ('11', null, 'f32c3cae-b11a-495c-8905-5fb8ae1b0a8d', '1G3IJY_2017-04-27-16_00_33-HRIS---MoHFW.png', '/files/WFewMl_1G3IJY_2017-04-27-16_00_33-HRIS---MoHFW.png', 'png', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 09:52:48', '2017-05-09 10:02:52', '2017-05-02 10:31:21', null);
INSERT INTO `spyr42_uploads` VALUES ('12', null, 'ce186425-1b4f-4cc9-b5d3-fc25dfaa4efe', '512287198.jpg', '/files/979Rd0_512287198.jpg', 'jpg', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 09:52:48', '2017-05-09 10:02:52', null, null);
INSERT INTO `spyr42_uploads` VALUES ('13', null, '2c848ee8-b8bf-4ddf-86c6-cb67ee619479', 'D4D-Topic-Activation-HRIS.docx.docx', '/files/AtrwMO_D4D-Topic-Activation-HRIS.docx.docx', 'docx', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 09:52:49', '2017-05-09 10:02:53', '2017-05-02 10:31:16', null);
INSERT INTO `spyr42_uploads` VALUES ('14', null, '0ce6ff2a-6251-4fc5-98da-47c4fb277f44', '2017-04-23 14_41_23-HRIS - MoHFW.png', '/files/XtbQti_2017-04-23-14_41_23-HRIS---MoHFW.png', 'png', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 10:54:41', '2017-05-09 10:02:53', null, null);
INSERT INTO `spyr42_uploads` VALUES ('15', null, '22db8a51-ac37-47f8-98f6-9aa26afbb626', '2017-04-19 18_51_30-কঠিন গবেষণা!.png', '/files/pm7beD_2017-04-19-18_51_30-কঠিন-গবেষণা!.png', 'png', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 10:56:46', '2017-05-09 10:02:53', null, null);
INSERT INTO `spyr42_uploads` VALUES ('16', null, '6015e283-3d22-4312-97fd-6d97b30dac05', '2017-04-23 14_41_23-HRIS - MoHFW.png', '/files/FtIcYr_2017-04-23-14_41_23-HRIS---MoHFW.png', 'png', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 11:01:25', '2017-05-09 10:02:53', null, null);
INSERT INTO `spyr42_uploads` VALUES ('17', null, 'c95a020e-79a3-4113-9747-e3f3e32498ef', '2017-03-27 17_10_45-HRIS - MoHFW.png', '/files/xyOMxs_2017-03-27-17_10_45-HRIS---MoHFW.png', 'png', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 11:03:00', '2017-05-09 10:02:53', null, null);
INSERT INTO `spyr42_uploads` VALUES ('18', null, '7066a7c2-f002-40a8-990e-2c73c691e1f4', '2017-01-09 15_42_50-HRIS - MoHFW.png', '/files/owPcTU_2017-01-09-15_42_50-HRIS---MoHFW.png', 'png', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '1', '1', '2017-05-02 11:48:08', '2017-05-09 10:02:53', null, null);
INSERT INTO `spyr42_uploads` VALUES ('19', null, '93e92624-222b-4c59-8221-77130a3d3f97', '2017-01-09 15_42_50-HRIS - MoHFW.png', '/files/E6ELfe_2017-01-09-15_42_50-HRIS---MoHFW.png', 'png', null, null, null, null, null, 'Yes', '1', '1', '2017-05-02 11:48:20', '2017-05-09 10:02:53', null, null);
INSERT INTO `spyr42_uploads` VALUES ('20', null, '2a579465-7b16-4a74-a9f8-725fd38c22ed', '2017-04-27 16_00_33-HRIS - MoHFW.png', '/files/39BPNl_2017-04-27-16_00_33-HRIS---MoHFW.png', 'png', null, null, null, null, null, 'Yes', '1', '1', '2017-05-02 14:15:08', '2017-05-09 10:02:53', null, null);
INSERT INTO `spyr42_uploads` VALUES ('21', null, 'f5052f64-869c-4e0d-8ce2-8fa04e423c58', '2017-05-03 17_38_55-dghshrml4_providerstatuses @dghshrml4 (__LOCALHOST) - Table - Navicat Premium.png', '/files/4RqzpI_2017-05-03-17_38_55-dghshrml4_providerstatuses-@dghshrml4-(__LOCALHOST)---Table---Navicat-Premium.png', 'png', null, null, null, null, null, 'Yes', '1', '1', '2017-05-03 11:47:17', '2017-05-09 10:02:53', null, null);
INSERT INTO `spyr42_uploads` VALUES ('22', null, '3658fd83-92a4-4cd6-a539-8879a2cb9cde', '2017-05-03 17_38_55-dghshrml4_providerstatuses @dghshrml4 (__LOCALHOST) - Table - Navicat Premium.png', '/files/FdeeUe_2017-05-03-17_38_55-dghshrml4_providerstatuses-@dghshrml4-(__LOCALHOST)---Table---Navicat-Premium.png', 'png', null, null, null, null, null, 'Yes', '1', '1', '2017-05-03 11:47:55', '2017-05-09 10:02:53', null, null);
INSERT INTO `spyr42_uploads` VALUES ('23', null, 'c7f61745-7cfd-4090-bf60-f107cb5a691e', '2017-05-03 17_38_55-dghshrml4_providerstatuses @dghshrml4 (__LOCALHOST) - Table - Navicat Premium.png', '/files/MExwub_2017-05-03-17_38_55-dghshrml4_providerstatuses-@dghshrml4-(__LOCALHOST)---Table---Navicat-Premium.png', 'png', null, null, null, null, null, 'Yes', '1', '1', '2017-05-03 11:48:25', '2017-05-09 10:02:54', null, null);
INSERT INTO `spyr42_uploads` VALUES ('24', null, '7eb660ab-54db-435e-b080-bfc97bb8394c', '2017-05-03 17_38_55-dghshrml4_providerstatuses @dghshrml4 (__LOCALHOST) - Table - Navicat Premium.png', '/files/EkXCHq_2017-05-03-17_38_55-dghshrml4_providerstatuses-@dghshrml4-(__LOCALHOST)---Table---Navicat-Premium.png', 'png', null, null, null, null, null, 'Yes', '1', '1', '2017-05-03 11:54:08', '2017-05-09 10:02:54', null, null);
INSERT INTO `spyr42_uploads` VALUES ('25', null, '8762f1bf-4cd6-403e-bb77-90aa79f5d3a8', '2017-05-03 17_38_55-dghshrml4_providerstatuses @dghshrml4 (__LOCALHOST) - Table - Navicat Premium.png', '/files/au1p21_2017-05-03-17_38_55-dghshrml4_providerstatuses-@dghshrml4-(__LOCALHOST)---Table---Navicat-Premium.png', 'png', null, null, null, null, null, 'Yes', '1', '1', '2017-05-03 11:54:16', '2017-05-09 10:02:54', null, null);
INSERT INTO `spyr42_uploads` VALUES ('26', '0', 'cf8cf28e-81c7-453f-aced-d1e5840b9a44', '2017-05-03 17_38_55-dghshrml4_providerstatuses @dghshrml4 (__LOCALHOST) - Table - Navicat Premium.png', '/files/94vhh3_2017-05-03-17_38_55-dghshrml4_providerstatuses-@dghshrml4-(__LOCALHOST)---Table---Navicat-Premium.png', 'png', null, null, null, null, null, 'Yes', '1', '1', '2017-05-03 12:04:59', '2017-05-09 10:02:54', null, null);
INSERT INTO `spyr42_uploads` VALUES ('27', '18', '4543ab72-68a5-4284-8095-cd0ab27dd6eb', '2017-05-03 17_38_55-dghshrml4_providerstatuses @dghshrml4 (__LOCALHOST) - Table - Navicat Premium.png', '/files/18/gL7oRD_2017-05-03-17_38_55-dghshrml4_providerstatuses-@dghshrml4-(__LOCALHOST)---Table---Navicat-Premium.png', 'png', null, null, null, null, null, 'Yes', '38', '38', '2017-05-03 12:07:45', '2017-05-09 10:02:54', null, null);
INSERT INTO `spyr42_uploads` VALUES ('28', '18', 'be78984b-2731-4be6-908e-3b94d05f86a0', '2017-05-03 17_38_55-dghshrml4_providerstatuses @dghshrml4 (__LOCALHOST) - Table - Navicat Premium.png', '/files/18/I7RWOK_2017-05-03-17_38_55-dghshrml4_providerstatuses-@dghshrml4-(__LOCALHOST)---Table---Navicat-Premium.png', 'png', null, 'lorel ipsum', null, null, null, 'Yes', '38', '38', '2017-05-07 06:25:01', '2017-05-09 10:02:54', null, null);
INSERT INTO `spyr42_uploads` VALUES ('29', '18', '85ed30aa-a314-4298-85ca-3cc83803a63e', '2017-05-03 17_38_55-dghshrml4_providerstatuses @dghshrml4 (__LOCALHOST) - Table - Navicat Premium.png', '/files/18/9a4faa0c-9805-48fb-aadc-f5811c71ff37.png', 'png', null, null, '2', '40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', 'Yes', '38', '38', '2017-05-07 06:32:05', '2017-05-09 10:02:54', '2017-05-09 07:25:36', null);
INSERT INTO `spyr42_uploads` VALUES ('30', '18', '042b9fe9-4512-4168-89de-dad75e7d442f', '979Rd0_512287198 (1).jpg', '/files/18/bd52beaf-6546-4528-bce0-ffc12c6845f4.jpg', 'jpg', null, '', '2', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'Yes', '38', '38', '2017-05-09 07:57:39', '2017-05-09 10:05:52', null, null);
INSERT INTO `spyr42_uploads` VALUES ('31', '18', 'd0bf3951-f3ea-4e22-8b4b-d9823c77cc68', '979Rd0_512287198 (1).jpg', '/files/18/5abdcd9f-be26-4c84-9d36-aae1d8e1190c.jpg', 'jpg', null, 'lorel ipsum dolor sit amet', '2', '38', 'b0540092-b9e0-4517-8563-12b5ea6713af', 'Yes', '38', '38', '2017-05-09 08:01:02', '2017-05-09 10:56:17', null, null);

-- ----------------------------
-- Table structure for spyr42_userdetails
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_userdetails`;
CREATE TABLE `spyr42_userdetails` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_userdetails
-- ----------------------------

-- ----------------------------
-- Table structure for spyr42_users
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_users`;
CREATE TABLE `spyr42_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tenant_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci DEFAULT 'null',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `group_ids_csv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_titles_csv` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_editable_by_tenant` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_users
-- ----------------------------
INSERT INTO `spyr42_users` VALUES ('1', '8311d707-9dd3-4b38-aebd-e3675f079805', null, 'superuser', 'superuser1@email.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', '{\"superuser\":1}', null, null, '1', null, '2017-02-08 15:49:59', '2017-07-13 09:22:27', 'h2kG3Nc6elJWN1m3o5AsL3RG6hBSJ6dhLup2jPmzEc', null, null, 'Yes', '2017-02-07 13:58:59', '1', '2017-07-13 09:22:27', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('2', 'f03ee964-4343-40e2-be55-13321a1a4721', null, 'test', 'test@test.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', '{\"superuser\":1}', ',2,', ',Customer admin,', '1', null, null, '2017-02-28 09:57:56', 'HbSRdqQhSAarChEkr894w1PCvZTGiRdyyoyfCUrJFM', null, null, 'Yes', '2017-02-26 12:01:03', '1', '2017-04-30 06:45:35', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('3', 'f7051333-2018-4cb8-a369-0e7e7c0f2c10', null, 'anotheronesd', 'another@gmail.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, ',1,', ',superuser,', '1', null, null, null, null, null, null, 'Yes', '2017-02-27 07:27:20', '1', '2017-02-27 09:47:00', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('4', '06b122e9-7b28-4c3e-a38f-4301159d7632', null, 'test222', 'test222@gmail.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, ',1,', ',superuser,', '1', null, null, null, null, null, null, 'Yes', '2017-02-28 07:31:57', '1', '2017-03-06 09:13:37', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('5', '09ba9ff3-1b42-412e-b38d-f0410f1a47fa', null, 'another123', 'another123@gmail.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, ',2,', ',tenant-admin,', '1', null, null, null, null, null, null, 'Yes', '2017-02-28 07:32:50', '1', '2017-02-28 07:32:50', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('6', 'cab23d68-e0a3-42f8-a3dd-6f2e78c7f3a9', '18', 'uuid', 'uuid@gmail.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, ',2,', ',Customer admin,', '1', null, null, null, null, null, 'No', 'Yes', '2017-02-28 07:33:24', '1', '2017-04-30 10:13:10', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('7', 'ec058bbf-f71b-4349-8cdb-f64ed7b692ff', null, 'uuid2122', 'uuid2@gmail.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, ',2,', ',tenant-admin,', '1', null, null, null, null, null, null, 'Yes', '2017-02-28 07:35:31', '1', '2017-03-06 09:48:10', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('8', 'f9c950d9-6ed4-4316-95b2-c38a518703b7', null, 'testssdfchangelog', 'testchangelog@gmail.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, ',2,', ',tenant-admin,', '1', null, null, null, null, null, null, 'Yes', '2017-03-05 14:41:23', '1', '2017-03-06 10:05:53', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('22', '53b44b53-b6e0-4c26-960c-ed21d278851b', '9', 'tenant2', 'tenant2@gmail.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, ',2,', ',Customer admin,', '1', null, null, null, null, null, null, 'Yes', '2017-04-30 06:50:25', '1', '2017-04-30 06:50:25', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('23', '526fbb10-634b-4a90-9e3e-5939f5b5531e', '9', 'tenant-admin-9', 'support@activationltd.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, null, null, '1', null, null, null, null, null, 'No', 'Yes', '2017-04-30 06:50:25', '1', '2017-04-30 08:21:24', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('24', '15f6571e-3aa2-4543-9ecf-932fd56e9174', '10', 'tenant3', 'tenant3@gmail.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, ',2,', ',Customer admin,', '1', null, null, '2017-04-30 07:30:07', 'haIhdldoF8cJGQhCRkyTFCNTVPv4G7YMJ4nRcnYbLw', null, null, 'Yes', '2017-04-30 06:52:12', '1', '2017-04-30 08:09:55', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('25', '58576798-0220-431f-8449-d5218cfc0bd7', '10', 'tenant-admin-10', 'support@activationltd.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, null, null, '1', null, null, null, null, null, 'No', 'Yes', '2017-04-30 06:52:12', '1', '2017-04-30 06:52:12', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('26', 'a214220a-1f60-4345-a789-fd0dce2d451b', '11', 'tenant4', 'tenant4@gmail.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, '2', 'Customer admin', '1', null, null, null, null, null, null, 'Yes', '2017-04-30 07:26:33', '1', '2017-04-30 07:26:33', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('27', '12cceb99-48bb-4b86-8e9f-345a354ebbff', '11', 'tenant-admin-11', 'support@activationltd.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, '2', 'Customer admin', '1', null, null, null, null, null, 'No', 'Yes', '2017-04-30 07:26:33', '1', '2017-04-30 07:26:33', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('38', 'b0540092-b9e0-4517-8563-12b5ea6713af', '18', 'tenant6', 'tenant6@gmail.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, ',2,', ',Customer admin,', '1', null, null, '2017-06-11 11:29:39', 'zNIhJs9RwAb71VCLUZZCfK5ZW6p0i6B1afpgk4TSRN', null, '', 'Yes', '2017-04-30 08:41:25', '1', '2017-06-11 11:29:39', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('39', '5b01e62a-ba2b-4c5c-b2ed-de2af59888a0', '18', 'tenant-admin-18', 'support@activationltd.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, '2', 'Customer admin', '1', null, null, null, null, null, 'No', 'Yes', '2017-04-30 08:41:25', '1', '2017-04-30 08:41:25', '1', null, null);
INSERT INTO `spyr42_users` VALUES ('40', '3e152209-0cb8-4e0c-8e5b-56fa64154ec5', '18', 'tenant6-6', 'tenant6-6@gmail.com', '$2y$10$uWDxdBF5mSkosXDXildX0O9P2VuQ1IX7KL2KjQNx6e94ttmEqx2O.', null, ',2,', ',Customer admin,', '1', null, null, '2017-05-02 06:49:36', 'TRkcb1xrMA2T1OzVnXOwiQ73Q1uWzFFaKltOICaeTY', null, 'Yes', 'Yes', '2017-04-30 11:03:20', '38', '2017-05-09 11:56:13', '38', null, null);

-- ----------------------------
-- Table structure for spyr42_users_groups
-- ----------------------------
DROP TABLE IF EXISTS `spyr42_users_groups`;
CREATE TABLE `spyr42_users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spyr42_users_groups
-- ----------------------------
INSERT INTO `spyr42_users_groups` VALUES ('2', '2');
INSERT INTO `spyr42_users_groups` VALUES ('3', '1');
INSERT INTO `spyr42_users_groups` VALUES ('4', '1');
INSERT INTO `spyr42_users_groups` VALUES ('5', '2');
INSERT INTO `spyr42_users_groups` VALUES ('6', '2');
INSERT INTO `spyr42_users_groups` VALUES ('7', '2');
INSERT INTO `spyr42_users_groups` VALUES ('8', '2');
INSERT INTO `spyr42_users_groups` VALUES ('38', '2');
INSERT INTO `spyr42_users_groups` VALUES ('39', '2');
INSERT INTO `spyr42_users_groups` VALUES ('40', '2');
