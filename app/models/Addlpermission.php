<?php

/**
 * Addlpermission
 *
 * @property integer        $id
 * @property string         $uuid
 * @property string         $name
 * @property string         $title
 * @property string         $desc
 * @property integer        $parent_id
 * @property string         $type
 * @property integer        $moudule_id
 * @property integer        $moudulegroup_id
 * @property string         $route
 * @property string         $route_filters
 * @property string         $functions
 * @property string         $meta
 * @property string         $is_active
 * @property integer        $created_by
 * @property integer        $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer        $deleted_by
 * @property-read \User     $updater
 * @property-read \User     $creator
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereMouduleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereMoudulegroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereRoute($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereRouteFilters($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereFunctions($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereMeta($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereDeletedBy($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Upload[] $uploads
 * @property-read \Illuminate\Database\Eloquent\Collection|\Revision[] $revisions
 */
class Addlpermission extends Spyrmodule
{
    /**
     * Mass assignment fields (White-listed fields)
     *
     * @var array
     */
    protected $fillable = ['uuid', 'name', 'title', 'desc', 'parent_id', 'type', 'moudule_id', 'moudulegroup_id', 'route', 'route_filters', 'functions', 'meta', 'is_active', 'created_by', 'updated_by', 'deleted_by'];
    /**
     * Disallow from mass assignment. (Black-listed fields)
     *
     * @var array
     */
    // protected $guarded = [];

    /**
     * Date fields
     *
     * @var array
     */
    // protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Validation rules
     *
     * @param       $element
     * @param array $merge
     * @return array
     */
    public static function rules($element, $merge = [])
    {
        $rules = [
            'name'            => 'required|between:1,255|unique:addlpermissions,name' . (isset($element->id) ? ",$element->id" : ''),
            'title'           => 'required|between:1,255|unique:addlpermissions,title' . (isset($element->id) ? ",$element->id" : ''),
            'parent_id'       => 'exists:Addlpermission,id,is_active,"Yes"',
            'moudule_id'      => 'exists:Module,id,is_active,"Yes"',
            'moudulegroup_id' => 'exists:Modulegroup,id,is_active,"Yes"',
            'created_by'      => 'exists:users,id,is_active,"Yes"',
            'updated_by'      => 'exists:users,id,is_active,"Yes"',
            'is_active'       => 'required|in:Yes,No',
        ];
        return array_merge($rules, $merge);
    }

    /**
     * Custom validation messages.
     *
     * @var array
     */
    public static $custom_validation_messages = [
        //'name.required' => 'Custom message.',
    ];
    /**
     * Automatic eager load relation by default (can be expensive)
     *
     * @var array
     */
    // protected $with = ['relation1', 'relation2'];

    ############################################################################################
    # Model events
    ############################################################################################
    public static function boot()
    {
        /**
         * parent::boot() was previously used. However this invocation stops from the other classes
         * of other spyr modules(Models) to override the boot() method. Need to check more.
         * make the parent (Eloquent) boot method run.
         */
        parent::boot();
        Spyrmodule::registerObserver(get_class()); // register observer

        /************************************************************/
        // Following code block executes - when an element is in process
        // of creation for the first time but the creation has not
        // completed yet.
        /************************************************************/
        // static::creating(function (Addlpermission $element) { });

        /************************************************************/
        // Following code block executes - after an element is created
        // for the first time.
        /************************************************************/
        // static::created(function (Addlpermission $element) {});

        /************************************************************/
        // Following code block executes - when an already existing
        // element is in process of being updated but the update is
        // not yet complete.
        /************************************************************/
        // static::updating(function (Addlpermission $element) {});

        /************************************************************/
        // Following code block executes - after an element
        // is successfully updated
        /************************************************************/
        //static::updated(function (Addlpermission $element) {});

        /************************************************************/
        // Execute codes during saving (both creating and updating)
        /************************************************************/
        static::saving(function (Addlpermission $element) {
            $valid = true;
            /************************************************************/
            // Your validation goes here
            // if($valid) $valid = $element->isSomethingDoable(true)
            /************************************************************/
            // fill common fields, null-fill, trim blanks from input
            return $valid;
        });

        /************************************************************/
        // Execute codes after model is successfully saved
        /************************************************************/
        // static::saved(function (Addlpermission $element) { });

        /************************************************************/
        // Following code block executes - when some element is in
        // the process of being deleted. This is good place to
        // put validations for eligibility of deletion.
        /************************************************************/
        // static::deleting(function (Addlpermission $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully deleted.
        /************************************************************/
        // static::deleted(function (Addlpermission $element) {});

        /************************************************************/
        // Following code block executes - when an already deleted element
        // is in the process of being restored.
        /************************************************************/
        // static::restoring(function (Addlpermission $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully restored.
        /************************************************************/
        //static::restored(function (Addlpermission $element) {});
    }

    ############################################################################################
    # Validator functions
    ############################################################################################

    /**
     * @param bool|false $setMsgSession setting it false will not store the message in session
     * @return bool
     */
    //    public function isSomethingDoable($setMsgSession = false)
    //    {
    //        $valid = true;
    //        // Make invalid(Not request-able) if {something doesn't match with something}
    //        if ($valid && $this->id == null) {
    //            if ($setMsgSession) $valid = setError("Something is wrong. Id is Null!!"); // make valid flag false and set validation error message in session if message flag is true
    //            else $valid = false; // don't set any message only set validation as false.
    //        }
    //
    //        return $valid;
    //    }

    ############################################################################################
    # Helper functions
    ############################################################################################

    /**
     * Non static functions can be directly called $element->function();
     * Such functions are useful when an object(element) is already instantiated
     * and some processing is required for that
     */
    // public function someAction() { }

    /**
     * Static cuntions needs to be called using Model::function($id)
     * Inside static function you may need to query and get the element
     *
     * @param $id
     */
    // public static function someOtherAction($id) { }

    ############################################################################################
    # Permission functions
    ############################################################################################
    /*
     * This is a place holder to write the functions that resolve permission to a specific model.
     * In the parent class there are the follow functions that checks whether a user has
     * permission to perform the following on an element. Rewrite these functions
     * in case more customized access management is required.
     */
    ############################################################################################

    /**
     * Checks if the $addlpermission is viewable by current or any user passed as parameter.
     * spyrElementViewable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isViewable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementViewable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $addlpermission is editable by current or any user passed as parameter.
     * spyrElementEditable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isEditable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementEditable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $addlpermission is deletable by current or any user passed as parameter.
     * spyrElementDeletable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isDeletable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementDeletable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $addlpermission is restorable by current or any user passed as parameter.
     * spyrElementRestorable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isRestorable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementRestorable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    ############################################################################################
    # Query scopes
    ############################################################################################
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
    public function scopePopular($query) { return $query->where('votes', '>', 100); }
    public function scopeWomen($query) { return $query->whereGender('W'); }
    # Example of user
    $users = User::popular()->women()->orderBy('created_at')->get();
    */
    ############################################################################################

    // Write new query scopes here.

    ############################################################################################
    # Dynamic scopes
    ############################################################################################
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
    public function scopeOfType($query, $type) { return $query->whereType($type); }
    # Example of user
    $users = User::ofType('member')->get();
    */
    ############################################################################################

    // Write new dynamic query scopes here.

    ############################################################################################
    # Model relationships
    ############################################################################################
    /*
     * This is a place holder to write model relationships. In the parent class there are
     * In the parent class there are the follow two relations creator(), updater() are
     * already defined.
     */
    ############################################################################################

    # Default relationships already available in base Class 'Spyrmodule'
    //public function updater() { return $this->belongsTo('User', 'updated_by'); }
    //public function creator() { return $this->belongsTo('User', 'created_by'); }

    // Write new relationships below this line

    ############################################################################################
    # Accessors & Mutators
    ############################################################################################
    /*
     * Eloquent provides a convenient way to transform your model attributes when getting or setting them. Simply
     * define a getFooAttribute method on your model to declare an accessor. Keep in mind that the methods
     * should follow camel-casing, even though your database columns are snake-case:
     */
    // Example
    // public function getFirstNameAttribute($value) { return ucfirst($value); }
    // public function setFirstNameAttribute($value) { $this->attributes['first_name'] = strtolower($value); }

    ############################################################################################

    // Write accessors and mutators here.


}
