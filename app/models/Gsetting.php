<?php

/**
 * Gsetting
 *
 * @property integer        $id
 * @property string         $uuid
 * @property string         $name
 * @property string         $title
 * @property string         $type
 * @property string         $desc
 * @property string         $value
 * @property string         $allow_tenant_override
 * @property string         $is_active
 * @property integer        $created_by
 * @property integer        $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer        $deleted_by
 * @property-read \User     $updater
 * @property-read \User     $creator
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereAllowTenantOverride($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereDeletedBy($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Upload[] $uploads
 * @property-read \Illuminate\Database\Eloquent\Collection|\Revision[] $revisions
 */
class Gsetting extends Spyrmodule
{
    /**
     * Mass assignment fields (White-listed fields)
     *
     * @var array
     */
    protected $fillable = ['uuid', 'name', 'title', 'type', 'desc', 'value', 'allow_tenant_override', 'is_active', 'created_by', 'updated_by', 'deleted_by'];

    /**
     * Disallow from mass assignment. (Black-listed fields)
     *
     * @var array
     */
    // protected $guarded = [];

    /**
     * Date fields
     *
     * @var array
     */
    // protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Global setting can be stored in three different formats.Boolean, string and array.
     * Array value is stored as json
     *
     * @var array
     */
    public static $types = [
        'boolean' => 'Boolean',
        'string'  => 'String',
        'array'   => 'Array',
    ];

    /**
     * Validation rules. For regular expression validation use array instead of pipe
     * Example: 'name' => ['required', 'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/']
     *
     * @param       $element
     * @param array $merge
     * @return array
     */
    public static function rules($element, $merge = [])
    {
        $rules = [
            'name'                  => 'required|between:1,100|unique:gsettings,name' . (isset($element->id) ? ",$element->id" : ''),
            'title'                 => 'required|between:1,255|unique:gsettings,title' . (isset($element->id) ? ",$element->id" : ''),
            'type'                  => 'required', //Gsetting::$types
            'desc'                  => 'between:1,2048',
            'value'                 => 'between:1,2048',
            'allow_tenant_override' => 'required|in:Yes,No',
            'created_by'            => 'integer|exists:users,id,is_active,"Yes"',
            'updated_by'            => 'integer|exists:users,id,is_active,"Yes"',
            'is_active'             => 'required|in:Yes,No',
        ];
        return array_merge($rules, $merge);
    }

    public static $custom_validation_messages = [
        //'name.required' => 'Custom message.',
    ];
    # Automatic eager load relation by default (can be expensive)
    // protected $with = ['relation1', 'relation2'];

    ############################################################################################
    # Model events
    ############################################################################################
    public static function boot()
    {
        /**
         * parent::boot() was previously used. However this invocation stops from the other classes
         * of other spyr modules(Models) to override the boot() method. Need to check more.
         * make the parent (Eloquent) boot method run.
         */
        //parent::boot();
        Spyrmodule::registerObserver(get_class());
        //Gsetting::observe(new GsettingObserver); // register observer

        /************************************************************/
        // Following code block executes - when an element is in process
        // of creation for the first time but the creation has not
        // completed yet.
        /************************************************************/
        // static::creating(function (Gsetting $element) { });

        /************************************************************/
        // Following code block executes - after an element is created
        // for the first time.
        /************************************************************/
        // static::created(function (Gsetting $element) {});

        /************************************************************/
        // Following code block executes - when an already existing
        // element is in process of being updated but the update is
        // not yet complete.
        /************************************************************/
        // static::updating(function (Gsetting $element) {});

        /************************************************************/
        // Following code block executes - after an element
        // is successfully updated
        /************************************************************/
        //static::updated(function (Gsetting $element) {});

        /************************************************************/
        // Execute codes during saving (both creating and updating)
        /************************************************************/
        static::saving(function (Gsetting $element) {

            $valid = true;

            // type should be valid
            if ($valid && !in_array($element->type, keyAsArray(Gsetting::$types))) {
                $valid = setError("Type must be on the the following: " . csvFromArray(keyAsArray(Gsetting::$types)));
            }
            // for type=boolean only allow true/false as value
            if ($valid && $element->type == 'boolean' && !in_array($element->value, ['true', 'false'])) {
                $valid = setError("If boolean type is selected, value must be 'true' or 'false'");
            }
            // for type=array only allow JSON string as value
            if ($valid && $element->type == 'array' && !json_decode($element->value)) {
                $valid = setError("If array/json type is selected, value must be a valid json string");
            }

            return $valid;
        });

        /************************************************************/
        // Execute codes after model is successfully saved
        /************************************************************/
        static::saved(function (Gsetting $element) {
            // storeChangesFromSession("", $element, ""); // Take changes from session and store in changes table
        });

        /************************************************************/
        // Following code block executes - when some element is in
        // the process of being deleted. This is good place to
        // put validations for eligibility of deletion.
        /************************************************************/
        // static::deleting(function (Gsetting $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully deleted.
        /************************************************************/
        // static::deleted(function (Gsetting $element) {});

        /************************************************************/
        // Following code block executes - when an already deleted element
        // is in the process of being restored.
        /************************************************************/
        // static::restoring(function (Gsetting $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully restored.
        /************************************************************/
        //static::restored(function (Gsetting $element) {});
    }

    ############################################################################################
    # Validator functions
    ############################################################################################

    /**
     * @param bool|false $setMsgSession setting it false will not store the message in session
     * @return bool
     */
    //    public function isSomethingDoable($setMsgSession = false)
    //    {
    //        $valid = true;
    //        // Make invalid(Not request-able) if {something doesn't match with something}
    //        if ($valid && $this->id == null) {
    //            if ($setMsgSession) $valid = setError("Something is wrong. Id is Null!!"); // make valid flag false and set validation error message in session if message flag is true
    //            else $valid = false; // don't set any message only set validation as false.
    //        }
    //
    //        return $valid;
    //    }

    ############################################################################################
    # Helper functions
    ############################################################################################
    /**
     * Non static functions can be directly called $element->function();
     * Such functions are useful when an object(element) is already instantiated
     * and some processing is required for that
     */
    // public function someAction() { }

    /**
     * Static cuntions needs to be called using Model::function($id)
     * Inside static function you may need to query and get the element
     *
     * @param $id
     */
    // public static function someOtherAction($id) { }

    ############################################################################################
    # Permission functions
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write the functions that resolve permission to a specific model.
     * In the parent class there are the follow functions that checks whether a user has
     * permission to perform the following on an element. Rewrite these functions
     * in case more customized access management is required.
     */
    ############################################################################################

    /**
     * Checks if the $gsetting is viewable by current or any user passed as parameter.
     * spyrElementViewable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isViewable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementViewable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $gsetting is editable by current or any user passed as parameter.
     * spyrElementEditable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isEditable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementEditable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $gsetting is deletable by current or any user passed as parameter.
     * spyrElementDeletable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isDeletable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementDeletable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $gsetting is restorable by current or any user passed as parameter.
     * spyrElementRestorable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     *
     * @param null $user_id
     * @return bool
     */
    //    public function isRestorable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementRestorable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    ############################################################################################
    # Query scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
       public function scopePopular($query) { return $query->where('votes', '>', 100); }
       public function scopeWomen($query) { return $query->whereGender('W'); }
       # Example of user
       $users = User::popular()->women()->orderBy('created_at')->get();
    */
    ############################################################################################

    // Write new query scopes here.

    ############################################################################################
    # Dynamic scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
    public function scopeOfType($query, $type) { return $query->whereType($type); }
    # Example of user
    $users = User::ofType('member')->get();
    */
    ############################################################################################

    // Write new dynamic query scopes here.

    ############################################################################################
    # Model relationships
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write model relationships. In the parent class there are
     * In the parent class there are the follow two relations creator(), updater() are
     * already defined.
     */
    ############################################################################################

    # Default relationships already available in base Class 'Spyrmodule'
    //public function updater() { return $this->belongsTo('User', 'updated_by'); }
    //public function creator() { return $this->belongsTo('User', 'created_by'); }

    // Write new relationships below this line

    ############################################################################################
    # Accessors & Mutators
    # ---------------------------------------------------------------------------------------- #
    /*
     * Eloquent provides a convenient way to transform your model attributes when getting or setting them. Simply
     * define a getFooAttribute method on your model to declare an accessor. Keep in mind that the methods
     * should follow camel-casing, even though your database columns are snake-case:
     */
    // Example
    // public function getFirstNameAttribute($value) { return ucfirst($value); }
    // public function setFirstNameAttribute($value) { $this->attributes['first_name'] = strtolower($value); }
    ############################################################################################

    // Write accessors and mutators here.
}