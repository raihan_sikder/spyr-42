@if(tenantUser())
    <input name="tenant_id" type="hidden" value="{{userTenantId()}}"/>
@else
    @include("spyr.form.select-model",['var'=>['name'=>'tenant_id','label'=>'Tenant/Customer', 'table'=>'tenants', 'container_class'=>'col-sm-6']])
@endif