<div id="systemMessages">
    @if ($errors->any() || Session::has('error'))
        <div class="callout callout-danger" id="errorDiv">
            <p>
                {{ implode('<br/>', $errors->all()) }}
                @if (Session::has('error'))
                    @if (is_array(Session::get('error')))
                        {{ implode('<br/>', Session::get('error')) }}
                    @else
                        {{ Session::get('error') }}
                    @endif
                    <?php Session::forget('error'); ?>
                @endif
            </p>
        </div>
    @endif
    @if (Session::has('message'))
        <div class="callout callout-warning" id="messageDiv">
            <p>
                <?php
                if (is_array(Session::get('message'))) {
                    echo implode('<br/>', Session::get('message'));
                } else {
                    echo Session::get('message');
                }
                Session::forget('message');
                ?>
            </p>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="callout callout-info" id="successDiv">
            <p>
                <?php
                if (is_array(Session::get('success'))) {
                    echo implode('<br/>', Session::get('success'));
                    Session::forget('success');
                } else {
                    echo Session::get('success');
                    Session::forget('success');
                }
                ?>
            </p>
        </div>
    @endif
</div>