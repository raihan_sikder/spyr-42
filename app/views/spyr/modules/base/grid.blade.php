@extends('spyr.template.app-frame')

<?php
/**
 * Variables used in this view file.
 * @var $module_name string 'superheroes'
 * @var $mod Module
 * @var $uuid string '1709c091-8114-4ba4-8fd8-91f0ba0b63e8'
 */
?>

@section('sidebar-left')
    @include('spyr.modules.base.include.sidebar-left')
@stop

@section('title')
    {{$mod->title}}
    <a class="btn btn-xs" href="{{route("$module_name.create")}}" data-toggle="tooltip"
       title="Create a new {{lcfirst(str_singular($mod->title))}}"><i class="fa fa-plus"></i></a>
@stop

@section('content')
    @if(View::exists('spyr.modules.' . $module_name . '.grid'))
        @include('spyr.modules.' . $module_name . '.grid')
    @else
        @include('spyr.modules.base.include.datatable')
    @endif
@stop

