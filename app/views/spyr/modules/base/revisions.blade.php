@extends('spyr.template.app-frame')
<?php
/**
 * Variables used in this view file.
 * @var $module_name string 'superheroes'
 * @var $mod Module
 * @var $superhero Superhero Object that is being edited
 * @var $element string 'superhero'
 * @var $mod Module
 * @var $revisions \Illuminate\Database\Eloquent\Collection
 */
?>
@section('sidebar-left')
    @include('spyr.modules.base.include.sidebar-left')
@stop

@section('title')
    Revisions
@stop

@section('content')
    @foreach($revisions as $revision)
        {{$revision->uuid}}<br/>
    @endforeach
@stop

