<?php

/**
 * Class ModuleBaseObserver
 * This base observer class is responsible for handling additional common system features while a model is manipulated.
 * For example when a model is updated, besides it being updated and updated value saved in database, there
 * happens a few additional operations. First of all before updating(or you can say 'before saving')
 * a model is sanitized using the fillModel() function, then the changes in the model is stored
 * in the temporarily in session, and if finally the model is successfully saved then the
 * changes are fetched from session and stored in change log storage. In addition to
 * that common cascade updates are also handled.
 */
class ModuleBaseObserver
{

    /**
     * This function is executed during a model's saving() phase
     *
     * @param $element Eloquent|Spyrmodule
     */
    public function saving($element)
    {
        $element = fillModel($element); // This line should be placed just before return
        Revision::keepChangesInSession($element); // store change log
    }

    /**
     * This function is executed during a model's saved() phase
     *
     * @param $element Eloquent|Spyrmodule
     */
    public function saved($element)
    {
        Revision::storeChangesFromSession("", $element, ""); // Take changes from session and store in changes table
    }
}