<?php

/**
 * Class Spyrdatatable
 */
class Spyrdatatable extends Datatables
{
    /**
     * Spyrdatatable uses colum name with prefix so no need to add that again.
     * Returns current database prefix
     *
     * @return string
     */
    public function database_prefix() {
        // return Config::get('database.connections.'.Config::get('database.default').'.prefix', '');
        return '';
    }
}