<?php

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ModuleCommand extends Command
{
    /**
     * The console command name.
     * @var string
     */
    protected $name = 'module:make';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Create a new module';
    protected $template_module = 'superheroes';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $this->info('Creating New Module...');
        $this->askModuleName();
        $this->createMigration();
        $this->createViewFiles();
        $this->createModelFile();
        $this->createModelObserverFile();
        $this->createControllerFile();
//        $this->info('Created Module: ' . $this->userData['module_name'] . ' with module_name ' . $this->userData['module_sys_name'] . ' and human readable name: ' . $this->userData['module_human_name']);
        $this->maybeRunMigration();
    }

    /**
     * Creates the controller file for the new module.
     */
    public function createControllerFile()
    {
        // get template code
        $code = File::get("app/controllers/" . controller($this->template_module) . ".php");

        // replace maps
        $replaces = [
            controller($this->template_module) => controller($this->userData['module_name']),
            $this->template_module => $this->userData['module_name'],
        ];

        // run replace across the template code
        foreach ($replaces as $k => $v) {
            $code = str_replace($k, $v, $code);
        }

        // write on new file
        File::put('app/controllers/' . controller($this->userData['module_name']) . '.php', $code);

        // show message in console
        $this->info('Controller Created');
    }

    /**
     * replicate view files
     */
    public function createViewFiles()
    {
        $from = 'app/views/spyr/modules/' . $this->template_module;
        $to = 'app/views/spyr/modules/' . $this->userData['module_name'];
        if (!File::copyDirectory($from, $to)) {
            $this->error('Creating view was unsuccessful');
        } else {
            $this->info('View Files Created');
        }
    }

    /**
     * Create the model file and find-replace the placeholder words
     */
    public function createModelFile()
    {
        // get template code
        $code = File::get("app/models/" . model($this->template_module) . ".php");

        // replace maps
        $replaces = [
            model($this->template_module) => model($this->userData['module_name']),
            $this->template_module => $this->userData['module_name'],
            str_singular($this->template_module) => str_singular($this->userData['module_name']),
        ];

        // run replace across the template code
        foreach ($replaces as $k => $v) {
            $code = str_replace($k, $v, $code);
        }

        // write on new file
        File::put('app/models/' . model($this->userData['module_name']) . '.php', $code);

        // show message in console
        $this->info('Model Created');
    }

    /**
     * Create observer file
     */
    public function createModelObserverFile()
    {
        // get template code
        $code = File::get("app/observers/" . model($this->template_module) . "Observer.php");

        // replace maps
        $replaces = [
            model($this->template_module) => model($this->userData['module_name']), //Superhero to Newmodule
            $this->template_module => $this->userData['module_name'],
        ];

        // run replace across the template code
        foreach ($replaces as $k => $v) {
            $code = str_replace($k, $v, $code);
        }

        // write on new file
        File::put('app/observers/' . model($this->userData['module_name']) . 'Observer.php', $code);

        // show message in console
        $this->info('Observer Created');
    }

    /**
     * Run migration if required
     */
    public function maybeRunMigration()
    {
        $yn = strtolower(trim($this->ask('Do you want to run the created migration [y/n] default n:')));
        if ($yn == 'y' or $yn == 'yes') {
            $this->call('migrate');
            $this->call('cache:clear');
        } else {
            $this->info('Migration was not run. Run it later manually `php artisan migrate`. Don\'t forget to run `php artisan cache:clear` after that');
        }
    }

    public function askModuleName()
    {
        $invalid = true;
        while ($invalid) {
            $name = strtolower($this->ask('Enter Module Name (plural with lowercase):'));
            $title = ucfirst(str_singular($name));

            $validator = Validator::make(
                [
                    'name' => $name,
                    'title' => $title,
                ],
                [
                    'name' => 'required|alpha|unique:modules,name',
                    'title' => 'required|alpha',
                ]);
            if ($validator->fails()) {
                $this->error($validator->messages()->first());
            } else {
                $this->userData['module_name'] = $name;
                $this->userData['module_title'] = $title;
                $invalid = false;
            }
        }
    }

    /**
     * Create migration.
     */
    public function createMigration()
    {
        // get template code
        $code = File::get("app/database/2016_02_29_162403_create_superheros_table.php");

        // replace maps
        $replaces = [
            $this->template_module => $this->userData['module_name'],
            model($this->template_module) => model($this->userData['module_name']),
        ];

        // run replace across the template code
        foreach ($replaces as $k => $v) {
            $code = str_replace($k, $v, $code);
        }

        // write on new file
        $filename = 'create_' . $this->userData['module_name'] . '_table';
        $this->call('migrate:make', ['name' => $filename]);
        $migration_file = Collection::make(File::files('app/database/migrations'))->last();
        File::put($migration_file, $code);

        // show message in console
        $this->info('Migration Created');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['example', InputArgument::OPTIONAL, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}