<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Addl-permissions types field
	|--------------------------------------------------------------------------
	|
	| Additional permissions can be categories into different types. These type
	| segregation allows to present related permission options under relevant
	| parent items. i.e.
	|
	*/

	'types' => [
		'modulgroup-extended-permission',
		'module-extended-permission',
		'global-permission',
		'route-permission',
		'api-permission'
	]


];
