<?php

/**
 * Renders a multi-dimentional array of permissions in hiararchical order for assigning permission
 * The $tree can be generated from Modulegroup::tree()
 *
 * @param $tree : Modulegroup::tree()
 */
function renderModulePermissionTree($tree) {
    if (is_array($tree)) {
        echo "<ul>";
        foreach ($tree as $leaf) {

            $perm = 'perm-' . $leaf['type'] . '-' . $leaf['item']->name;
            $val = $perm;

            echo "<div class='clearfix'></div><li class='pull-left'>" .
                "<input name='permission[]' type='checkbox' v-model='permission' value='$val'
				v-on:click='clicked'/>" .
                "<label><b>" . $leaf['item']->title . "</b> - <small>" . $leaf['item']->desc . "</small></label> <div class='clearfix'></div>";

            if ($leaf['type'] == 'module') {
                $module_default_permissions_suffixes = [
                    'view-list'    => 'View grid',
                    'view-details' => 'View details',
                    'create'       => 'Create',
                    'edit'         => 'Edit',
                    'delete'       => 'Delete',
                    'restore'      => 'Restore',
                    'change-logs'  => 'Access change logs',
                ];

                echo "<ul class='pull-left module-permissions'>";
                foreach ($module_default_permissions_suffixes as $k => $v) {
                    $val = "$perm-$k";
                    echo "<li>" .
                        "<input name='permission[]' type='checkbox' v-model='permission'  value='$val'/>" .
                        "<label>" . $v . "</label>" .
                        "</li>";
                }
                echo "</ul>";

                if ($leaf['item']->has_uploads) {
                    $file_permission_suffixes = [
                        'files-view-list'    => 'View file list',
                        'files-view-details' => 'View file details',
                        'files-create'       => 'File upload',
                        'files-edit'         => 'File edit',
                        'files-delete'       => 'File delete',
                        'files-download'     => 'File download',
                    ];
                    echo "<ul class='pull-left '>";
                    foreach ($file_permission_suffixes as $k => $v) {
                        $val = "$perm-$k";
                        echo "<li>" .
                            "<input name='permission[]' type='checkbox' v-model='permission'  value='$val'/>" .
                            "<label>" . $v . "</label>" .
                            "</li>";
                    }
                    echo "</ul>";
                }

                if ($leaf['item']->has_messages) {
                    $message_permission_suffixes = [
                        'messages-view-list' => 'View message list',
                        'messages-create'    => 'View create',
                        'messages-edit'      => 'Message edit',
                        'messages-delete'    => 'Message delete',
                    ];
                    echo "<ul class='pull-left '>";
                    foreach ($message_permission_suffixes as $k => $v) {
                        $val = "$perm-$k";
                        echo "<li>" .
                            "<input name='permission[]' type='checkbox' v-model='permission'  value='$val'/>" .
                            "<label>" . $v . "</label>" .
                            "</li>";
                    }
                    echo "</ul>";
                }
            }

            if (isset($leaf['children']) && count($leaf['children'])) {
                renderModulePermissionTree($leaf['children']);
            }
            echo "</li>";
        }
        echo "</ul>";

        return;
    }
}

/**
 * returns sentry object of currently logged in user
 *
 * @param bool|false $user_id
 * @return bool|User|null|static
 */
function u($user_id = false) {
    $user = false;
    if ($user_id) {
        $user = User::remember(5)->find($user_id);
    } else if (Sentry::check()) { // for logged in user
        $user = Sentry::getUser();
    }
    //    // for API requests find the user based on the param/header values
    //    if(!$user && Input::has('user_id')){ // No logged user. get from user_id in url param or request header
    //        $user = User::find(Input::get('user_id'));
    //    }
    //    if(!$user && Input::has('client_id')){ // No logged user. get from user_id in url param or request header
    //        $user = User::find(Input::get('client_id'));
    //    }

    return $user;
}

/**
 * This is a similar function to sentry's hasAccess. checks if current user has access to a certain permission
 *
 * @param           $permission
 * @param bool|null $user_id
 * @return bool
 */
function hasAccess($permission, $user_id = false) {
    $allowed = false;
    $user = u($user_id);
    if (isset($user)) {
        if ($user->hasAccess($permission)) {
            $allowed = true;
            Session::push('permissions', "Allowed - user[" . $user->id . "] - '$permission'");
        } else {
            $allowed = false;
            // for the purpose of diagnosis store premission log in session.
            if (conf('app.debug') == true) {
                Session::push('permissions', "Not allowed - user[" . $user->id . "] - '$permission'");
            }
        }
    } else {
        Session::push('permissions', "Undefined user - '$permission'");
    }

    return $allowed;
}

/**
 * Same as has accses
 *
 * @param            $permission
 * @param bool|false $user_id
 * @return bool
 */
function hasPermission($permission, $user_id = false) {
    return hasAccess($permission, $user_id);
}

/**
 * Short hand function to check module specific permissions
 *
 * @param            $module_name
 * @param            $permission
 * @param bool|false $user_id
 * @return bool
 */
function hasModulePermission($module_name, $permission, $user_id = false) {
    return hasAccess("perm-module-$module_name-$permission", $user_id);
}

/**
 * Checks if an spyr element(model) is creatable by a user
 *
 * @param      $element
 * @param null $user_id
 * @param bool $set_msg
 * @return bool
 */
function spyrElementCreatable($element, $user_id = null, $set_msg = false) {
    $valid = true;
    $module_name = elementModule($element);
    $user = u($user_id); // get the currently logged in user

    // First check sentry permission
    if (!hasModulePermission($module_name, 'create', $user->id)) {
        $valid = setError("User[" . $user->name . "] does not have create permission on module: $module_name [" . $element->id . "]", $set_msg);
    }

    // Check for valid tenant context
    if ($valid && (inTenantContext($module_name) && !elementBelongsToSameTenant($element))) {
        $valid = setError("User[" . $user->name . "] does not create permission on module: $module_name [" . $element->id . "] because the element does not belong to same user", $set_msg);
    }

    return $valid;
}

/**
 * Checks if an spyr element(model) is viewable by a user
 *
 * @param      $element
 * @param null $user_id
 * @param bool $set_msg
 * @return bool
 */
function spyrElementViewable($element, $user_id = null, $set_msg = false) {
    $valid = true;
    $module_name = elementModule($element);
    $user = u($user_id); // get the currently logged in user

    // First check sentry permission
    if (!hasModulePermission($module_name, 'view-details', $user->id)) {
        $valid = setError("User[" . $user->name . "] does not have view permission on module: $module_name [" . $element->id . "]", $set_msg);
    }

    // Check for valid tenant context
    if ($valid && (inTenantContext($module_name) && !elementBelongsToSameTenant($element))) {
        $valid = setError("User[" . $user->name . "] does not have view permission on module: $module_name [" . $element->id . "] because the element does not belong to same user", $set_msg);
    }

    return $valid;
}

/**
 * Checks if an spyr element(model) is editable by a user
 *
 * @param      $element
 * @param null $user_id
 * @param bool $set_msg
 * @return bool
 */
function spyrElementEditable($element, $user_id = null, $set_msg = false) {
    $valid = true;
    $module_name = elementModule($element);
    $user = u($user_id); // get the currently logged in user

    // First check sentry permission
    if ($valid && !hasModulePermission($module_name, 'edit', $user->id)) {
        $valid = setError("User[" . $user->name . "] does not have edit permission on module: $module_name [" . $element->id . "]", $set_msg);
    }
    // Check for valid tenant context
    if ($valid) {
        $valid = editableInTenantContext($element, $user->id, $set_msg);
    }

    return $valid;
}

/**
 * Checks if an spyr element(model) is deletable by a user
 *
 * @param      $element
 * @param null $user_id
 * @param bool $set_msg
 * @return bool
 */
function spyrElementDeletable($element, $user_id = null, $set_msg = false) {
    $valid = true;
    $module_name = elementModule($element);
    $user = u($user_id); // get the currently logged in user

    // First check sentry permission
    if ($valid && !hasModulePermission($module_name, 'delete', $user->id)) {
        $valid = setError("User[" . $user->name . "] does not have delete permission on module: $module_name [" . $element->id . "]", $set_msg);
    }

    // Check for valid tenant context
    if ($valid) {
        $valid = editableInTenantContext($element, $user->id, $set_msg);
    }

    return $valid;
}

/**
 * Checks if an spyr element(model) is deletable by a user
 *
 * @param      $element
 * @param null $user_id
 * @param bool $set_msg
 * @return bool
 */
function spyrElementRestorable($element, $user_id = null, $set_msg = false) {
    $valid = true;
    $module_name = elementModule($element);
    $user = u($user_id); // get the currently logged in user

    // First check sentry permission
    if ($valid && !hasModulePermission($module_name, 'restore', $user->id)) {
        $valid = setError("User[" . $user->name . "] does not have restore permission on module: $module_name [" . $element->id . "]", $set_msg);
    }
    // Check for valid tenant context
    if ($valid) {
        $valid = editableInTenantContext($element, $user->id, $set_msg);
    }

    return $valid;
}

/**
 * Some elements are system enforced and not allowed to be editable by tenant.
 *
 * @param      $element
 * @param bool $set_msg
 * @return bool
 */
function editableByTenant($element, $set_msg = false) {
    $valid = true;
    if (isset($element->is_editable_by_tenant) && $element->is_editable_by_tenant == 'No') {
        $valid = setError("This is a system entry and not editable by tenant user ", $set_msg);
    }
    return $valid;
}

/**
 * This function checks if a user has correct tenant context of an element and the elemnet
 * is not a system reserved element.
 *
 * @param      $element
 * @param null $user_id
 * @param bool $set_msg
 * @return bool
 */
function editableInTenantContext($element, $user_id = null, $set_msg = false) {
    $user = u($user_id); // get the currently logged in user
    $module_name = elementModule($element);
    $valid = true;

    if (inTenantContext($module_name, $user->id)) {
        if (!elementBelongsToSameTenant($element, $user->id)) {
            $valid = setError("User[" . $user->name . "] can not edit : $module_name [" . $element->id . "] because it does not belong to this user", $set_msg);
        } else {
            $valid = editableByTenant($element, $set_msg);
        }
    }
    return $valid;
}