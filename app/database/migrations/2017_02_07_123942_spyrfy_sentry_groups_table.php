<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SpyrfySentryGroupsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function ($table) {
            $table->string('uuid', 36)->nullable()->default(null)->after('id');
            $table->integer('tenant_id')->unsigned()->nullable()->default(null)->after('uuid');
            $table->string('is_active', 3)->nullable()->default(null)->after('permissions');
            $table->integer('created_by')->unsigned()->nullable()->default(null)->after('created_at');
            $table->integer('updated_by')->unsigned()->nullable()->default(null)->after('updated_at');
            $table->softDeletes();
            $table->integer('deleted_by')->unsigned()->nullable()->default(null)->after('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function ($table) {
            $table->dropColumn(['uuid', 'tenant_id', 'is_active', 'created_by', 'updated_by', 'deleted_by', 'deleted_at']);
        });
    }

}
