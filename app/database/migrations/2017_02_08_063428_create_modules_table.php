<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function ($table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('uuid', 36)->nullable()->default(null);
            $table->string('name', 255)->nullable()->default(null);

            // custom fields starts
            $table->string('title', 100)->nullable()->default(null);
            $table->string('desc', 255)->nullable()->default(null);
            $table->integer('parent_module_id')->unsigned()->nullable()->default(null);
            $table->integer('level')->unsigned()->nullable()->default(null);
            $table->integer('order')->unsigned()->nullable()->default(null);
            $table->string('color_css', 128)->nullable()->default(null);
            $table->string('icon_css', 128)->nullable()->default(null);
            $table->string('route', 128)->nullable()->default(null);

            // custom fields ends
            $table->string('is_active', 3)->nullable()->default(null);
            $table->integer('created_by')->unsigned()->nullable()->default(null);
            $table->integer('updated_by')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('deleted_by')->unsigned()->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }

}
