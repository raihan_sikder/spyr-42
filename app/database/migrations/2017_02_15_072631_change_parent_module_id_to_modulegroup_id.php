<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeParentModuleIdToModulegroupId extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//DB::statement("ALTER TABLE `spyr42_modules` CHANGE `parent_module_id` `modulegroup_id` INT(10) UNSIGNED NULL DEFAULT NULL;");
		Schema::table('modules', function ($table) {
			$table->integer('modulegroup_id')->unsigned()->nullable()->default(null);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//DB::statement("ALTER TABLE `spyr42_modules` CHANGE `modulegroup_id` `parent_module_id` INT(10) UNSIGNED NULL DEFAULT NULL;");
		if (Schema::hasColumn('modules', 'modulegroup_id')) {
			Schema::table('modules', function ($table) {
				$table->dropColumn('modulegroup_id');
			});
		}
	}

}
