<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdInUserdetails extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('userdetails', function($table){
            $table->integer('user_id')->unsigned()->nullable()->default(null)->after('uuid');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('userdetails', function($table){
            $table->dropColumn('user_id');
        });
	}

}
