<?php

use Illuminate\Database\Migrations\Migration;
use Webpatser\Uuid\Uuid;

class CreatePermissionsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permissions', function ($table) {

			$table->engine = 'InnoDB';

			$table->bigIncrements('id');
			$table->string('uuid', 36)->nullable()->default(null);
			$table->string('name', 255)->nullable()->default(null);

			// custom fields starts
			$table->string('title', 255)->nullable()->default(null);
			$table->string('desc', 255)->nullable()->default(null);
			$table->integer('parent_permission_id')->unsigned()->nullable()->default(0);
			$table->integer('permissioncategory_id')->unsigned()->nullable()->default(null);
			$table->integer('moudule_id')->unsigned()->nullable()->default(null);
			$table->integer('moudulegroup_id')->unsigned()->nullable()->default(null);
			$table->string('route', 255)->nullable()->default(null);
			$table->string('route_filters', 512)->nullable()->default(null);
			$table->string('functions', 512)->nullable()->default(null);
			$table->string('meta', 512)->nullable()->default(null);

			// custom fields ends
			$table->string('is_active', 3)->nullable()->default(null);
			$table->integer('created_by')->unsigned()->nullable()->default(null);
			$table->integer('updated_by')->unsigned()->nullable()->default(null);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('deleted_by')->unsigned()->nullable()->default(null);
		});

		DB::table('modules')->insert(
			[
				'uuid'             => Uuid::generate(4),
				'name'             => 'permissions',
				'title'            => 'Permission',
				'desc'             => '',
				'parent_module_id' => '0',
				'level'            => 0,
				'order'            => 0,
				'color_css'        => 'aqua',
				'icon_css'         => 'fa fa-plus',
				'route'            => 'permissions.index',
				'is_active'        => 'Yes',
				'created_at'       => now(),
				'created_by'       => '1',
				'updated_at'       => now(),
				'updated_by'       => '1'
			]
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// drop the module table
		Schema::dropIfExists('permissions');
		// remove the module entry from modules table
		DB::table('modules')->where('name', 'permissions')->delete();
	}

}