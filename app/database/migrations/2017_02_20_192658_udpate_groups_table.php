<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UdpateGroupsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('groups', function ($table) {
			$table->dropColumn('tenant_id');
			$table->string('title', 100)->nullable()->default(null)->after('name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('groups', function ($table) {
			$table->integer('tenant_id')->unsigned()->nullable()->default(null)->after('uuid');
			$table->dropColumn('title');
		});
	}

}
