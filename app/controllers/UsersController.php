<?php

class UsersController extends SpyrmodulebaseController
{

    public function __construct()
    {

        $this->module_name = controllerModule(get_class());

        /*********************************************
         *  Query extender for grid
         *********************************************/
        // Grid datatable configurations
        $this->db_table = dbTable($this->module_name);
        // Join query
        $this->grid_query = DB::table(DB::raw($this->db_table))
            ->leftJoin('users as updater', DB::raw($this->db_table . '.updated_by'), ' = ', DB::raw('updater.id'))
            ->select(
                DB::raw($this->db_table . '.id as id'),
                DB::raw($this->db_table . '.name as name'),
                DB::raw($this->db_table . '.group_titles_csv as group_titles_csv'),
                DB::raw('updater.name as user_name'),
                DB::raw($this->db_table . '.updated_at as updated_at'),
                DB::raw($this->db_table . '.is_active as is_active')
            )->whereNull(DB::raw($this->db_table . '.deleted_at'));

        // Columns to show 'prefix_table.field','renamed_field','Grid_column_title'
        $this->grid_columns = ['Id', 'Name', 'Groups', 'Updater', 'Update time', 'Active'];
        /**********************************************/

        parent::__construct($this->module_name);
    }

    /**
     * Returns datatable json for the module index page
     * A route is automatically created for all modules to access this controller function
     *
     * @return mixed
     */
    public function grid()
    {
        // grid query builder
        $q = $this->grid_query->whereNull($this->module_name . '.deleted_at');

        // Generate datatable
        return Spyrdatatable::of($q)
            ->edit_column('name', '<a href="{{ route(\'' . $this->module_name . '.edit\', $id) }}">{{$name}}</a>')
            ->edit_column('group_titles_csv', '{{cleanCsv($group_titles_csv)}}')
            ->edit_column('id', '<a href="{{ route(\'' . $this->module_name . '.edit\', $id) }}">{{$id}}</a>')
            ->make();
    }

    /**
     * Update user
     *
     * @param $id
     * @return $this|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */

    /*
    public function updateR($id)
    {

        // init local variables
        $module_name = $this->module_name;
        $Model = model($this->module_name);
        $element = str_singular($module_name);

        $ret = ret(); // load default return values

        if ($$element = $Model::find($id)) {
            // validate
            // Do we want to update the user password?
            $password_rules = [];
            $password = Input::get('password');

            $validator = Validator::make(Input::all(), $Model::rules($$element, $password_rules), $Model::$custom_validation_messages);
            if ($validator->fails()) {
                $ret = ret('fail', "Validation error(s) on updating $Model.", ['validation_errors' => json_decode($validator->messages(), true)]);
            } else {
                $$element->fill(Input::except('password', 'password_confirm', 'groups'));
                //$$element->fill(Input::all('password', 'password_confirm', 'groups'));
                if ($password) {
                    $$element->password = $password;
                }
                if ($$element->save()) {
                    setSuccess("$Model has been updated");
                    $ret = ret('success', "$Model has been updated", ['data' => $$element]);
                } else {
                    setError("$Model update failed.");
                    $ret = ret('fail', "$Model update failed.");
                }
            }
        } else {
            $ret = ret('fail', "$Model could not be found. The element is either unavailable or deleted.");
        }

        if (Input::get('ret') == 'json') {
            $ret = fillRet($ret); // fill with session values(messages, errors, success etc) and redirect
            return Response::json($ret);
        } else {
            if ($ret['status'] == 'fail') {
                return Redirect::to(Input::get('redirect_fail'))->withErrors($validator)->withInput();
            } else {
                return Redirect::to(Input::get('redirect_success'))->with('success', "$Model has been updated.");
            }
        }
    }
    */

    /**
     * @param $id
     * @return $this|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        // init local variables
        $module_name = $this->module_name;
        $Model = model($this->module_name);
        $element = str_singular($module_name);
        $ret = ret(); // load default return values

        # --------------------------------------------------------
        # Process update
        # --------------------------------------------------------
        if ($$element = $Model::find($id)) { // Check if element exists.
            if ($$element->isEditable()) { // Check if the element is editable.
                // validate
                // Do we want to update the user password?
                $password_rules = [];
                $password = Input::get('password');

                $validator = Validator::make(Input::all(), $Model::rules($$element, $password_rules), $Model::$custom_validation_messages);

                if ($validator->fails()) { // Handle validation fail. set return values.
                    $ret = ret('fail', "Validation error(s) on updating $Model.", ['validation_errors' => json_decode($validator->messages(), true)]);
                } else { // Validation passes
                    $$element->fill(Input::except('password'));
                    //$$element->fill(Input::all('password', 'password_confirm', 'groups'));
                    if ($password) {
                        $$element->password = $password;
                    }
                    if ($$element->save()) { // Attempt to update/save.
                        $ret = ret('success', "$Model has been updated", ['data' => $$element]);
                    } else { // attempt to update/save failed. Set error message and return values.
                        $ret = ret('fail', "$Model update failed.");
                    }
                }
            } else { // Element is not editable. Set message and return values.
                $ret = ret('fail', "$Model is not editable by user.");
            }
        } else { // element does not exist(or possibly deleted). Set error message and return values
            $ret = ret('fail', "$Model could not be found. The element is either unavailable or deleted.");
        }

        # --------------------------------------------------------
        # Process return/redirect
        # --------------------------------------------------------
        if (Input::get('ret') == 'json') {
            $ret = fillRet($ret); // fill with session values(messages, errors, success etc) and redirect
            return Response::json($ret);
        } else {
            if ($ret['status'] == 'fail') { // Update failed. Redirect to fail path(url)
                $redirect = Redirect::to(Input::get('redirect_fail'))->withInput();
                if (isset($validator)) {
                    $redirect = $redirect->withErrors($validator);
                }
            } else { // Update successful. Redirect to success path(url)
                $redirect = Redirect::to(Input::get('redirect_success'));
            }
            return $redirect;
        }
    }

}
