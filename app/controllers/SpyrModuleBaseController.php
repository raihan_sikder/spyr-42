<?php

/**
 * Class SpyrmodulebaseController
 */
class SpyrmodulebaseController extends BaseController
{

    protected $module_name;         // Stores module name with lowercase and plural i.e. 'superheros'.
    protected $grid_query;          // Stores default DB query to create the grid. Used in grid() function.
    protected $grid_columns;        // Columns to show, this array is set form modules individual controller.
    protected $db_table;            // database table name with prefix

    /**
     * Constructor for this class is very important as it boots up necessary features of
     * Spyr module. First of all, it load module related meta information, then based
     * on context check(tenant context) it loads the tenant id. The it constructs the default
     * grid query and also add tenant context to grid query if applicable. Finally it
     * globally shares a couple of variables $module_name, $mod to all views rendered
     * from this controller
     *
     * @param $module_name
     */
    public function __construct($module_name)
    {

        $this->module_name = $module_name;
        $this->db_table = dbTable($module_name);

        # load default grid query
        if (!isset($this->grid_query)) {
            $this->grid_query = DB::table($this->module_name)
                ->leftJoin('users as updater', DB::raw($this->db_table . '.updated_by'), ' = ', DB::raw('updater.id'))
                ->select(
                    DB::raw($this->db_table . '.id as id'),
                    DB::raw($this->db_table . '.name as name'),
                    DB::raw('updater.name as user_name'),
                    DB::raw($this->db_table . '.updated_at as updated_at'),
                    DB::raw($this->db_table . '.is_active as is_active')
                );
        }
        # Inject tenant context in grid query
        if ($tenant_id = inTenantContext($module_name)) {
            $this->grid_query = injectTenantIdInModelQuery($module_name, $this->grid_query);
            Input::merge([tenantIdField() => $tenant_id]); // Set tenant_id in request header
        }

        # Load default grid columns
        if (!isset($this->grid_columns)) {
            $this->grid_columns = ['Id', 'Name', 'Updater', 'Update time', 'Active'];
        }
        // Share the variables across all views accessed by this controller
        View::share('module_name', $this->module_name); // module sys_name now shared across all views
        View::share('mod', Module::whereName($this->module_name)->remember(cacheTime('module-list'))->first()); // module sys_name now shared across all views
    }

    /**
     * Index/List page to show grid
     * This controller method is responsible for rendering the view that has the default
     * spyr module grid.
     *
     * @return $this
     */
    public function index()
    {
        if (hasModulePermission($this->module_name, 'view-list')) {
            return View::make('spyr.modules.base.grid')->with('grid_columns', $this->grid_columns);
        } else {
            showPermissionErrorPage("No group permission for " . $this->module_name);
        }
    }

    /**
     * Returns datatable json for the module index page
     * A route is automatically created for all modules to access this controller function
     *
     * @return mixed
     */
    public function grid()
    {
        // Grid query builder
        $q = $this->grid_query->whereNull($this->module_name . '.deleted_at');

        // Make datatable
        /** @var Datatables $dt */
        $dt = Spyrdatatable::of($q); // $dt refers to data table.
        $dt = $dt->edit_column('name', '<a href="{{ route(\'' . $this->module_name . '.edit\', $id) }}">{{$name}}</a>');
        $dt = $dt->edit_column('id', '<a href="{{ route(\'' . $this->module_name . '.edit\', $id) }}">{{$id}}</a>');

        return $dt->make();
    }

    /**
     * Shows an element create form.
     *
     * @return view
     */
    public function create()
    {
        if (hasModulePermission($this->module_name, 'create')) { // check for create permission
            $uuid = (Input::old('uuid')) ? Input::old('uuid') : uuid(); // Set uuid for the new element to be created
            return View::make('spyr.modules.base.form')->with('uuid', $uuid)->with('spyr_element_editable', true);
        } else {
            return showPermissionErrorPage('User do not have create permission');
        }
    }

    /**
     * Store an spyr element. Returns json response if ret=json is sent as url parameter. Otherwise redirects
     * based on the url set in redirect_success|redirect_fail
     *
     * @return $this|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        // init local variables
        $module_name = $this->module_name;
        /** @var Spyrmodule $Model */
        $Model = model($this->module_name);
        //$element_name = str_singular($module_name);
        $ret = ret();

        # --------------------------------------------------------
        # Process store while creation
        # --------------------------------------------------------
        if (hasModulePermission($this->module_name, 'create')) { // check module permission
            /** @var Spyrmodule $element */
            $element = new $Model(Input::all());
            // validate
            $validator = Validator::make(Input::all(), $Model::rules($element), $Model::$custom_validation_messages);
            if ($validator->fails()) {
                $ret = ret('fail', "Validation error(s) on creating $Model.", ['validation_errors' => json_decode($validator->messages(), true)]);
            } else {
                if ($element->isCreatable()) {
                    if ($element->save()) {
                        $ret = ret('success', "$Model id::" . $element->id . " has been created", ['data' => $Model::find($element->id)]);
                        Upload::linkTemporaryUploads($element->id, $element->uuid);
                    } else {
                        $ret = ret('fail', "$Model create failed.");
                    }
                } else {
                    $ret = ret('fail', "$Model could not be saved. (error: isCreatable())");
                }
            }

        } else {
            $ret = ret('fail', "User does not have create permission for module: $Model ");
        }
        # --------------------------------------------------------
        # Process return/redirect
        # --------------------------------------------------------
        if (Input::get('ret') == 'json') {
            $ret = fillRet($ret); // fill with session values(messages, errors, success etc) and redirect
            if ($ret['status'] == 'success' && $ret['redirect'] == '#new') {
                $ret['redirect'] = route("$module_name.edit", $element->id);
            }
            return Response::json($ret);
        } else {
            if ($ret['status'] == 'fail') {
                $redirect = Redirect::to(Input::get('redirect_fail'))->withInput();
                if (isset($validator)) {
                    $redirect = $redirect->withErrors($validator);
                }
            } else {
                if (Input::get('redirect_success') == '#new') {
                    $redirect = Redirect::route("$module_name.edit", $element->id);
                } else {
                    $redirect = Redirect::to(Input::get('redirect_success'));
                }
            }
            return $redirect;
        }
    }

    /**
     * Shows an spyr element. Store an spyr element. Returns json response if ret=json is sent as url parameter.
     * Otherwise redirects to edit page where details is visible as filled up edit form.
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        $module_name = $this->module_name;
        /** @var Spyrmodule $Model */
        $Model = model($this->module_name);
        $element_name = str_singular($module_name);
        $ret = ret(); // load default return values

        # --------------------------------------------------------
        # Process show
        # --------------------------------------------------------
        /** @var Spyrmodule $element */
        if ($element = $Model::find($id)) { // Check if the element exists
            if ($element->isViewable()) { // Check if the element is viewable
                $ret = ret('success', "$Model id::" . $element->id . " found", ['data' => $element]);
            } else { // not viewable
                $ret = ret('fail', "$Model is not viewable.");
            }
        } else { // The element was not found or has been deleted.
            $ret = ret('fail', "$Model could not be found. The element is either unavailable or deleted.");
        }
        # --------------------------------------------------------
        # Process return/redirect
        # --------------------------------------------------------
        if (Input::get('ret') == 'json') {
            $ret = fillRet($ret); // fill with session values(messages, errors, success etc) and redirect
            return Response::json($ret);
        } else {
            if ($ret['status'] == 'fail') { // Show failed. Redirect to fail path(url)
                return Redirect::route('home');
            } else { // Redirect to edit path
                return Redirect::route("$module_name.edit", $id);
            }
        }
    }

    /**
     * Show spyr element edit form
     *
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        // init local variables
        $module_name = $this->module_name;
        /** @var Spyrmodule $Model */
        $Model = model($this->module_name);
        $element_name = str_singular($module_name);
        # --------------------------------------------------------
        # Process return/redirect
        # --------------------------------------------------------
        /** @var Spyrmodule $element */
        if ($element = $Model::find($id)) { // Check if the element you are trying to edit exists
            if ($element->isViewable()) { // Check if the element is viewable
                return View::make('spyr.modules.base.form')
                    ->with('element', $element_name)//loads the singular module name in variable called $element = 'user'
                    ->with($element_name, $element)//loads the object into a variable with module name $user = (user object)
                    ->with('spyr_element_editable', $element->isEditable());
            } else { // Not viewable by the user. Set error message and return value.
                return showPermissionErrorPage("The element is not view-able by current user.");
            }
        } else { // The element does not exist. Set error and return values
            return showGenericErrorPage("The item that you are trying to access does not exist or has been deleted");
        }
    }

    /**
     * Update handler for spyr element.
     *
     * @param $id
     * @return $this|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        // init local variables
        //$module_name = $this->module_name;
        /** @var Spyrmodule $Model */
        $Model = model($this->module_name);
        // $element_name = str_singular($module_name);
        $ret = ret(); // load default return values
        # --------------------------------------------------------
        # Process update
        # --------------------------------------------------------
        /** @var Spyrmodule $element */
        if ($element = $Model::find($id)) { // Check if element exists.
            if ($element->isEditable()) { // Check if the element is editable.
                // validate
                $validator = Validator::make(Input::all(), $Model::rules($element), $Model::$custom_validation_messages);
                if ($validator->fails()) { // Handle validation fail. set return values.
                    $ret = ret('fail', "Validation error(s) on updating $Model.", ['validation_errors' => json_decode($validator->messages(), true)]);
                } else { // Validation passes
                    if ($element->fill(Input::all())->save()) { // Attempt to update/save.
                        $ret = ret('success', "$Model has been updated", ['data' => $element]);
                    } else { // attempt to update/save failed. Set error message and return values.
                        $ret = ret('fail', "$Model update failed.");
                    }
                }
            } else { // Element is not editable. Set message and return values.
                $ret = ret('fail', "$Model is not editable by user.");
            }
        } else { // element does not exist(or possibly deleted). Set error message and return values
            $ret = ret('fail', "$Model could not be found. The element is either unavailable or deleted.");
        }

        # --------------------------------------------------------
        # Process return/redirect
        # --------------------------------------------------------
        if (Input::get('ret') == 'json') {
            $ret = fillRet($ret); // fill with session values(messages, errors, success etc) and redirect
            return Response::json($ret);
        } else {
            if ($ret['status'] == 'fail') { // Update failed. Redirect to fail path(url)
                $redirect = Redirect::to(Input::get('redirect_fail'))->withInput();
                if (isset($validator)) {
                    $redirect = $redirect->withErrors($validator);
                }
            } else { // Update successful. Redirect to success path(url)
                $redirect = Redirect::to(Input::get('redirect_success'));
            }
            return $redirect;
        }
    }

    /**
     * Delete spyr element.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        // init local variables
        $module_name = $this->module_name;
        /** @var Spyrmodule $Model */
        $Model = model($this->module_name);
        $element_name = str_singular($module_name);
        $ret = ret(); // load default return values

        # --------------------------------------------------------
        # Process delete
        # --------------------------------------------------------
        /** @var Spyrmodule $element */
        if ($element = $Model::find($id)) { // check if the element exists
            if ($element->isDeletable()) { // check if the element is editable
                if ($element->delete()) { // attempt delete and set success message return values
                    $ret = ret('success', "$Model has been deleted");
                } else { // handle delete failure and set error message and return values
                    $ret = ret('fail', "$Model delete failed.");
                }
            } else { // element is not editable(which also means not deletable)
                $ret = ret('fail', "$Model could not be deleted.");
            }
        } else { // the element was not fonud. Set error message and return value
            $ret = ret('fail', "$Model could not be found. The element is either unavailable or deleted.");
        }

        # --------------------------------------------------------
        # Process return/redirect
        # --------------------------------------------------------
        if (Input::get('ret') == 'json') {
            $ret = fillRet($ret); // fill with session values(messages, errors, success etc) and redirect
            return Response::json($ret);
        } else {
            if ($ret['status'] == 'fail') { // Delete failed. Redirect to fail path(url)
                return Redirect::to(Input::get('redirect_fail'));
            } else { // Delete successful. Redirect to success path(url)
                return Redirect::to(Input::get('redirect_success'));
            }
        }
    }

    /**
     * Restore a soft-deleted.
     *
     * @param null $id
     * @return $this
     */
    public function restore($id = null)
    {
        return showGenericErrorPage("Restore feature is disabled");
    }

    /**
     * Show all the revisions/change logs of an item
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse|SpyrmodulebaseController
     */
    public function revisions($id)
    {
        // init local variables
        $module_name = $this->module_name;
        /** @var Spyrmodule $Model */
        $Model = model($this->module_name);
        $element_name = str_singular($module_name);
        $ret = ret(); // load default return values
        # --------------------------------------------------------
        # Process return/redirect
        # --------------------------------------------------------
        /** @var $element Spyrmodule */
        if ($element = $Model::find($id)) { // Check if the element you are trying to edit exists
            if ($element->isViewable()) { // Check if the element is viewable
                $revisions = $element->revisions;
                $ret = ret('success', "", ['data' => $revisions]);

            } else { // Not viewable by the user. Set error message and return value.
                $ret = ret('fail', "The element is not view-able by current user.");
                //return showPermissionErrorPage("The element is not view-able by current user.");
            }
        } else { // The element does not exist. Set error and return values
            $ret = ret('fail', "The item that you are trying to access does not exist or has been deleted");
            //return showGenericErrorPage("The item that you are trying to access does not exist or has been deleted");
        }

        # --------------------------------------------------------
        # Process return/redirect
        # --------------------------------------------------------
        if (Input::get('ret') == 'json') {
            $ret = fillRet($ret); // fill with session values(messages, errors, success etc) and redirect
            return Response::json($ret);
        } else {
            if ($ret['status'] == 'fail') { // Update failed. Redirect to fail path(url)
                return showGenericErrorPage($ret['message']);
            } else { // Update successful. Redirect to success path(url)
                /** @var array $revisions */
                return View::make('spyr.modules.base.revisions')
                    ->with('element', $element_name)
                    ->with($element_name, $element)
                    ->with('revisions', $revisions);
            }
        }
    }
}
