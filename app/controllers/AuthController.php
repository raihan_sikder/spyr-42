<?php

/**
 * Class AuthController
 */
class AuthController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default authentication controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authentication and registration related functionalities
    |
    */

    /**
     * Shows the sign up page for a new tenant/customer
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getSignup()
    {
        if (Sentry::check()) {
            return Redirect::route('account');
        }

        return View::make('spyr.auth-register.signup');
    }

    /**
     * Post sign up form handler
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postSignup()
    {

        // Declare the rules for the form validation.
        $rules = [
            'tenant_name'      => 'required|between:3,255| unique:tenants,name',
            'email'            => 'required|email', // email is no longer required to be unique
            'email_confirm'    => 'required|email|same:email',
            'name'             => 'required|alpha_num|between:3,32| unique:users,name',
            'password'         => 'required|between:6,32',
            'password_confirm' => 'required|same:password',
        ];
        // Custom validation error message for specific fields.
        $custom_validation_messages = [
            'tenant_name.required' => 'The company name field is required.',
        ];

        // validate
        $validator = Validator::make(Input::all(), $rules, $custom_validation_messages);
        if ($validator->fails()) {
            $ret = ret('fail', "Validation error(s)", ['validation_errors' => json_decode($validator->messages(), true)]);
            //return Redirect::back()->withInput()->withErrors($validator);
        } else {
            try {
                // Register the user
                $user = Sentry::register([
                    'name'     => Input::get('name'),
                    'email'    => Input::get('email'),
                    'password' => Input::get('password'),
                ]);

                // Create Tenant
                $tenant = Tenant::create([
                    'name'      => Input::get('tenant_name'),
                    'user_id'   => $user->id,
                    'is_active' => 'Yes',
                ]);

                // set the user as tenant admin of the newly created tenant
                DB::table('users')->where('id', $user->id)->update([
                    'tenant_id'        => $tenant->id,
                    'is_active'        => 'Yes',
                    'group_ids_csv'    => "," . conf('var.tenant-admin.group-id') . ",",
                    'group_titles_csv' => "," . conf('var.tenant-admin.group-title') . ",",
                ]);

                $group = Sentry::getGroupProvider()->findById(conf('var.tenant-admin.group-id'));
                User::find($user->id)->addGroup($group);

                // create the default tenant admin
                $default_tenant_admin_user = Sentry::createUser([
                    'email'     => conf('var.system-tenant-admin-email'),
                    'name'      => 'tenant-admin-' . $tenant->id,
                    'tenant_id' => $tenant->id,
                    'password'  => randomString(),
                    'activated' => true,
                ]);
                // Add this user to group and populate group fields in users table
                User::find($default_tenant_admin_user->id)->addGroup($group);
                DB::table('users')->where('id', $default_tenant_admin_user->id)->update([
                    'group_ids_csv'         => "," . conf('var.tenant-admin.group-id') . ",",
                    'group_titles_csv'      => "," . conf('var.tenant-admin.group-title') . ",",
                    'is_editable_by_tenant' => 'No',
                ]);

                // Data to be used on the email
                $data = [
                    'user'          => $user,
                    'activationUrl' => URL::route('activate', $user->getActivationCode()),
                ];

                // Send the activation code through email
                Mail::send('spyr.repository.emails.auth.register-activate', $data, function ($m) use ($user) {
                    $m->to($user->email, $user->name);
                    $m->subject('Welcome ' . $user->name);
                    $m->subject('Verify your account ' . $user->name);
                });

                // Redirect to the register page
                $ret = ret('success', Lang::get('auth/message.signup.success', ['data' => User::find($user->id)]));

            } catch (Cartalyst\Sentry\Users\UserExistsException $e) {
                $ret = ret('fail', Lang::get('auth/message.account_already_exists'));
            }
        }

        # --------------------------------------------------------
        # Process return/redirect
        # --------------------------------------------------------
        if (Input::get('ret') == 'json') {
            return Response::json(fillRet($ret));
        } else {
            if ($ret['status'] == 'fail') {
                $redirect = Redirect::back()->withInput();
                if (isset($validator)) {
                    $redirect = $redirect->withErrors($validator);
                }
            } else {
                $redirect = Redirect::back()->with('success', Lang::get('auth/message.signup.success'));
            }
            return $redirect;
        }
    }

    /**
     * Account sign in.
     *
     * @return View
     */
    public function getSignin()
    {
        // Is the user logged in?
        if (Sentry::check()) {
            return Redirect::route('home');
        }

        // Show the page
        return View::make('spyr.auth-register.signin');
    }

    /**
     * Account sign in form processing.
     *
     * @return Redirect
     */
    public function postSignin()
    {
        $ret = ret(); // load default return values

        // Declare the rules for the form validation
        $rules = [
            'name'     => 'required',
            'password' => 'required',
        ];

        // Create a new validator instance from our validation rules
        $validator = Validator::make(Input::all(), $rules);
        // Get the page we were before
        $redirect = Session::get('loginRedirect', route('home'));

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            // return Redirect::back()->withInput()->withErrors($validator);
            $ret = ret('fail', "Validation error(s).", ['validation_errors' => json_decode($validator->messages(), true)]);
        } else {

            try {
                // Try to log the user in
                Sentry::authenticate(Input::only('name', 'password'), Input::get('remember-me', 0));
                // Unset the page we were before from the session
                Session::forget('loginRedirect');
                $user = Sentry::getUser();
                $ret = ret('success', "User successfully logged in", ['data' => $user]);
            } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
                $ret = ret('fail', 'Login field is required.');
            } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
                $ret = ret('fail', 'Password field is required.');
            } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
                $ret = ret('fail', 'Wrong password, try again.');
            } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
                $ret = ret('fail', 'User was not found.');
            } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
                $ret = ret('fail', 'Uses is not activated');
            } // The following is only required if the throttling is enabled
            catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
                $ret = ret('fail', 'User is suspended.');
            } catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
                $ret = ret('fail', 'User is banned.');
            }
        }
        // Ooops.. something went wrong
        # --------------------------------------------------------
        # Process return/redirect
        # --------------------------------------------------------
        if (Input::get('ret') == 'json') {
            $ret = fillRet($ret); // fill with session values(messages, errors, success etc) and redirect
            return Response::json($ret);
        } else {
            if ($ret['status'] == 'fail') { // Delete failed. Redirect to fail path(url)
                return Redirect::back()->withInput()->withErrors($this->messageBag);
            } else { // Delete successful. Redirect to success path(url)
                return Redirect::to($redirect)->with('success', Lang::get('auth/message.signin.success'));
            }
        }
    }

    /**
     * Logout page.
     *
     * @return Redirect
     */
    public function getLogout()
    {
        // Log the user out
        Sentry::logout();
        if (Input::get('ret') == 'json') {
            return Response::json(['status' => 'success']);
        } else {
            // Redirect to the users page
            return Redirect::route('home')->with('success', 'You have successfully logged out!');
        }
    }

    /**
     * User account activation page.
     *
     * @param null $activationCode
     * @return \Illuminate\Http\RedirectResponse
     * @internal param string $actvationCode
     */
    public function getActivate($activationCode = null)
    {
        // Is the user logged in?
        if (Sentry::check()) {
            return Redirect::route('account');
        }

        try {
            // Get the user we are trying to activate
            $user = Sentry::getUserProvider()->findByActivationCode($activationCode);

            // Try to activate this user account
            if ($user->attemptActivation($activationCode)) {
                // Redirect to the login page
                return Redirect::route('signin')->with('success', Lang::get('auth/message.activate.success'));
            }

            // The activation failed.
            $error = Lang::get('auth/message.activate.error');
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            $error = Lang::get('auth/message.activate.error');
        }

        // Ooops.. something went wrong
        return Redirect::route('signin')->with('error', $error);
    }

    /**
     * Forgot password page.
     *
     * @return View
     */
    public function getForgotPassword()
    {
        // Show the page
        return View::make('auth.forgot-password');
    }

    /**
     * Forgot password form processing page.
     *
     * @return Redirect
     */
    public function postForgotPassword()
    {
        // Declare the rules for the validator
        $rules = [
            'email' => 'required|email',
        ];

        // Create a new validator instance from our dynamic rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::route('forgot-password')->withInput()->withErrors($validator);
        }

        try {
            // Get the user password recovery code
            $user = Sentry::getUserProvider()->findByLogin(Input::get('email'));

            // Data to be used on the email view
            $data = [
                'user'              => $user,
                'forgotPasswordUrl' => URL::route('forgot-password-confirm', $user->getResetPasswordCode()),
            ];

            // Send the activation code through email
            Mail::send('emails.forgot-password', $data, function ($m) use ($user) {
                if (isset($user->provider_id)) {
                    if ($provider_email = Provider::find($user->provider_id)->email) {
                        $m->to($provider_email);
                    }
                }
                $m->to($user->email, $user->first_name . ' ' . $user->last_name);
                $m->subject('Account Password Recovery');
            });
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            // Even though the email was not found, we will pretend
            // we have sent the password reset code through email,
            // this is a security measure against hackers.
        }

        //  Redirect to the forgot password
        return Redirect::route('forgot-password')->with('success', Lang::get('auth/message.forgot-password.success'));
    }

    /**
     * Forgot Password Confirmation page.
     *
     * @param  string $passwordResetCode
     * @return View
     */
    public function getForgotPasswordConfirm($passwordResetCode = null)
    {
        try {
            // Find the user using the password reset code
            $user = Sentry::getUserProvider()->findByResetPasswordCode($passwordResetCode);
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            // Redirect to the forgot password page
            return Redirect::route('forgot-password')->with('error', Lang::get('auth/message.account_not_found'));
        }

        // Show the page
        return View::make('auth.forgot-password-confirm');
    }

    /**
     * Forgot Password Confirmation form processing page.
     *
     * @param  string $passwordResetCode
     * @return Redirect
     */
    public function postForgotPasswordConfirm($passwordResetCode = null)
    {
        // Declare the rules for the form validation
        $rules = [
            'password'         => 'required',
            'password_confirm' => 'required|same:password',
        ];

        // Create a new validator instance from our dynamic rules
        $validator = Validator::make(Input::all(), $rules);

        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return Redirect::route('forgot-password-confirm', $passwordResetCode)->withInput()->withErrors($validator);
        }

        try {
            // Find the user using the password reset code
            $user = Sentry::getUserProvider()->findByResetPasswordCode($passwordResetCode);

            // Attempt to reset the user password
            if ($user->attemptResetPassword($passwordResetCode, Input::get('password'))) {
                // Password successfully reseted
                return Redirect::route('signin')->with('success', Lang::get('auth/message.forgot-password-confirm.success'));
            } else {
                // Ooops.. something went wrong
                return Redirect::route('signin')->with('error', Lang::get('auth/message.forgot-password-confirm.error'));
            }
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            // Redirect to the forgot password page
            return Redirect::route('forgot-password')->with('error', Lang::get('auth/message.account_not_found'));
        }
    }

    /**
     * create a superuser with given name and password
     */
    public function createSuperUser()
    {
        // create first user
        $user = Sentry::createUser(['name' => 'superuser', 'password' => 'superuser', 'activated' => 1,]);
        // create first group : superuser
        $group = Sentry::createGroup(['name' => 'Superuser', 'permissions' => ['superuser' => 1,]]);
        // add this newly created user to superuser group
        $user->addGroup($group);
        dd($group->getName() . " created<br/>");
    }
}
