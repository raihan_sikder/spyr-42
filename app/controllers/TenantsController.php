<?php

class TenantsController extends SpyrmodulebaseController
{
    /*
     * constructor
     */

    public function __construct()
    {
        $this->module_name = controllerModule(get_class());

        /*********************************************
         *  Query extender for grid
         *********************************************/
        // Grid datatable configurations
        $this->db_table = dbTable($this->module_name);
        // Join query
        $this->grid_query = DB::table(DB::raw($this->db_table))
            ->leftJoin('users as updater', DB::raw($this->db_table . '.updated_by'), ' = ', DB::raw('updater.id'))
            ->select(
                DB::raw($this->db_table . '.id as id'),
                DB::raw($this->db_table . '.name as name'),
                DB::raw('updater.name as user_name'),
                DB::raw($this->db_table . '.updated_at as updated_at'),
                DB::raw($this->db_table . '.is_active as is_active')
            )->whereNull(DB::raw($this->db_table . '.deleted_at'));

        // Columns to show 'prefix_table.field','renamed_field','Grid_column_title'
        $this->grid_columns = ['Id', 'Name', 'Updater', 'Update time', 'Active'];
        /**********************************************/

        parent::__construct($this->module_name);
    }

}
