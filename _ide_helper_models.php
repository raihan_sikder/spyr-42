<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace {
/**
 * Addlpermission
 *
 * @property integer $id 
 * @property string $uuid 
 * @property string $name 
 * @property string $title 
 * @property string $desc 
 * @property integer $parent_id 
 * @property string $type 
 * @property integer $moudule_id 
 * @property integer $moudulegroup_id 
 * @property string $route 
 * @property string $route_filters 
 * @property string $functions 
 * @property string $meta 
 * @property string $is_active 
 * @property integer $created_by 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property \Carbon\Carbon $deleted_at 
 * @property integer $deleted_by 
 * @property-read \User $updater 
 * @property-read \User $creator 
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereMouduleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereMoudulegroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereRoute($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereRouteFilters($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereFunctions($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereMeta($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Addlpermission whereDeletedBy($value)
 */
	class Addlpermission {}
}

namespace {
/**
 * Group
 *
 * @property integer $id 
 * @property string $uuid 
 * @property string $name 
 * @property string $title 
 * @property string $permissions 
 * @property string $is_active 
 * @property \Carbon\Carbon $created_at 
 * @property integer $created_by 
 * @property \Carbon\Carbon $updated_at 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $deleted_at 
 * @property integer $deleted_by 
 * @property-read \User $updater 
 * @property-read \User $creator 
 * @property-read \Illuminate\Database\Eloquent\Collection|\static::$userModel[] $users 
 * @method static \Illuminate\Database\Query\Builder|\Group whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Group whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Group whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Group whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Group wherePermissions($value)
 * @method static \Illuminate\Database\Query\Builder|\Group whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Group whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Group whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Group whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Group whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Group whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Group whereDeletedBy($value)
 */
	class Group {}
}

namespace {
/**
 * Gsetting
 *
 * @property integer $id 
 * @property string $uuid 
 * @property string $name 
 * @property string $title 
 * @property string $type 
 * @property string $desc 
 * @property string $value 
 * @property string $allow_tenant_override 
 * @property string $is_active 
 * @property integer $created_by 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property \Carbon\Carbon $deleted_at 
 * @property integer $deleted_by 
 * @property-read \User $updater 
 * @property-read \User $creator 
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereAllowTenantOverride($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Gsetting whereDeletedBy($value)
 */
	class Gsetting {}
}

namespace {
/**
 * Module
 *
 * @property integer $id 
 * @property string $uuid 
 * @property string $name 
 * @property string $title 
 * @property string $desc 
 * @property integer $parent_id 
 * @property integer $modulegroup_id 
 * @property integer $level 
 * @property integer $order 
 * @property string $color_css 
 * @property string $icon_css 
 * @property string $route 
 * @property string $has_uploads 
 * @property string $has_messages 
 * @property string $is_active 
 * @property integer $created_by 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property \Carbon\Carbon $deleted_at 
 * @property integer $deleted_by 
 * @property-read \User $updater 
 * @property-read \User $creator 
 * @method static \Illuminate\Database\Query\Builder|\Module whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereModulegroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereColorCss($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereIconCss($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereRoute($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereHasUploads($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereHasMessages($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereDeletedBy($value)
 */
	class Module {}
}

namespace {
/**
 * Modulegroup
 *
 * @property integer $id 
 * @property string $uuid 
 * @property string $name 
 * @property string $title 
 * @property string $desc 
 * @property integer $parent_id 
 * @property integer $level 
 * @property integer $order 
 * @property string $color_css 
 * @property string $icon_css 
 * @property string $route 
 * @property string $is_active 
 * @property integer $created_by 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property \Carbon\Carbon $deleted_at 
 * @property integer $deleted_by 
 * @property-read \User $updater 
 * @property-read \User $creator 
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereColorCss($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereIconCss($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereRoute($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modulegroup whereDeletedBy($value)
 */
	class Modulegroup {}
}

namespace {
/**
 * Permissioncategory
 *
 * @property integer $id 
 * @property string $uuid 
 * @property string $name 
 * @property integer $parent_id 
 * @property string $is_active 
 * @property integer $created_by 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property \Carbon\Carbon $deleted_at 
 * @property integer $deleted_by 
 * @property-read \User $updater 
 * @property-read \User $creator 
 * @method static \Illuminate\Database\Query\Builder|\Permissioncategory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Permissioncategory whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Permissioncategory whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Permissioncategory whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Permissioncategory whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Permissioncategory whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Permissioncategory whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Permissioncategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Permissioncategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Permissioncategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Permissioncategory whereDeletedBy($value)
 */
	class Permissioncategory {}
}

namespace {
/**
 * Revision
 *
 * @property integer $id 
 * @property integer $tenant_id 
 * @property string $uuid 
 * @property string $changeset 
 * @property string $name 
 * @property integer $module_id 
 * @property string $module_name 
 * @property integer $element_id 
 * @property string $element_uuid 
 * @property string $field 
 * @property string $old 
 * @property string $new 
 * @property string $desc 
 * @property string $is_active 
 * @property integer $created_by 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property \Carbon\Carbon $deleted_at 
 * @property integer $deleted_by 
 * @property-read \User $updater 
 * @property-read \User $creator 
 * @method static \Illuminate\Database\Query\Builder|\Revision whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereTenantId($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereChangeset($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereModuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereModuleName($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereElementId($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereElementUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereField($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereOld($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereNew($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Revision whereDeletedBy($value)
 */
	class Revision {}
}

namespace {
/**
 * Spyrmodule
 *
 * @property-read \User $updater 
 * @property-read \User $creator 
 */
	class Spyrmodule {}
}

namespace {
/**
 * Tenant
 *
 * @property integer $id 
 * @property string $uuid 
 * @property string $name 
 * @property integer $user_id 
 * @property string $is_active 
 * @property integer $created_by 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property \Carbon\Carbon $deleted_at 
 * @property integer $deleted_by 
 * @property-read \User $updater 
 * @property-read \User $creator 
 * @method static \Illuminate\Database\Query\Builder|\Tenant whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Tenant whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Tenant whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Tenant whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Tenant whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Tenant whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Tenant whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Tenant whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Tenant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Tenant whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Tenant whereDeletedBy($value)
 */
	class Tenant {}
}

namespace {
/**
 * Upload
 *
 * @property integer $id 
 * @property integer $tenant_id 
 * @property string $uuid 
 * @property string $name 
 * @property string $path 
 * @property string $tags 
 * @property string $desc 
 * @property integer $module_id 
 * @property integer $element_id 
 * @property string $element_uuid 
 * @property string $is_active 
 * @property integer $created_by 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property \Carbon\Carbon $deleted_at 
 * @property integer $deleted_by 
 * @property-read \User $updater 
 * @property-read \User $creator 
 * @method static \Illuminate\Database\Query\Builder|\Upload whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereTenantId($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereTags($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereModuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereElementId($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereElementUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Upload whereDeletedBy($value)
 */
	class Upload {}
}

namespace {
/**
 * User
 *
 * @property integer $id 
 * @property string $uuid 
 * @property integer $tenant_id 
 * @property string $name 
 * @property string $email 
 * @property string $password 
 * @property string $permissions 
 * @property string $group_ids_csv 
 * @property string $group_titles_csv 
 * @property boolean $activated 
 * @property string $activation_code 
 * @property \Carbon\Carbon $activated_at 
 * @property \Carbon\Carbon $last_login 
 * @property string $persist_code 
 * @property string $reset_password_code 
 * @property string $is_system_user 
 * @property string $is_active 
 * @property \Carbon\Carbon $created_at 
 * @property integer $created_by 
 * @property \Carbon\Carbon $updated_at 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $deleted_at 
 * @property integer $deleted_by 
 * @property-read \User $updater 
 * @property-read \User $creator 
 * @property-read \Illuminate\Database\Eloquent\Collection|\static::$groupModel[] $groups 
 * @method static \Illuminate\Database\Query\Builder|\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereTenantId($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\User wherePermissions($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereGroupIdsCsv($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereGroupTitlesCsv($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereActivated($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereActivationCode($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereActivatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereLastLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\User wherePersistCode($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereResetPasswordCode($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereIsSystemUser($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\User whereDeletedBy($value)
 */
	class User {}
}

namespace {
/**
 * Userdetail
 *
 * @property integer $id 
 * @property string $uuid 
 * @property integer $user_id 
 * @property string $name 
 * @property string $first_name 
 * @property string $last_name 
 * @property string $is_active 
 * @property integer $created_by 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property \Carbon\Carbon $deleted_at 
 * @property integer $deleted_by 
 * @property-read \User $updater 
 * @property-read \User $creator 
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Userdetail whereDeletedBy($value)
 */
	class Userdetail {}
}

